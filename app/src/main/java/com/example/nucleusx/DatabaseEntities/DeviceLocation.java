package com.example.nucleusx.DatabaseEntities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity (tableName = "device_location_table")
public class DeviceLocation {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private Date created;

    public DeviceLocation(String name) {
        this.name = name;
        this.created = new Date(System.currentTimeMillis());
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(" DatabaseEntities:DeviceLocation");
        sb.append(" - Id: " + this.id);
        sb.append(" - Name: " + this.name);
        sb.append(" - Created: " + this.created);
        return sb.toString();
    }
}
