package com.example.nucleusx.DatabaseEntities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.example.nucleusx.TypeConverters.TimestampConverter;

import java.util.Date;

@Entity(tableName = "device_table")
@TypeConverters(TimestampConverter.class)
public class Device {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int deviceId;
    private int deviceConfig;
    private int state;
    private int nodeId;
    private long uts;
    private String location;
    private Boolean quickAccessMember;
    private Date processedByRepositoryTimeStamp;

    public Device(String name, int deviceId, int deviceConfig, int state, int nodeId, long uts, String location, Boolean quickAccessMember) {
        this.name = name;
        this.deviceId = deviceId;
        this.deviceConfig = deviceConfig;
        this.state = state;
        this.nodeId = nodeId;
        this.uts = uts;
        this.location = location;
        this.quickAccessMember = quickAccessMember;
        this.processedByRepositoryTimeStamp = new Date(0);
    }

    public Device(Device device){
        this.name = device.name;
        this.deviceId = device.deviceId;
        this.deviceConfig = device.deviceConfig;
        this.state = device.state;
        this.nodeId = device.nodeId;
        this.uts = device.uts;
        this.location = device.location;
        this.quickAccessMember = device.quickAccessMember;
        this.processedByRepositoryTimeStamp = device.processedByRepositoryTimeStamp;
        //
        this.id = device.getId();
    }

    //Setters
    public void setState(int state) {
        this.state = state;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    public void setQuickAccessMember(Boolean quickAccessMember) {
        this.quickAccessMember = quickAccessMember;
    }

    public void setProcessedByRepositoryTimeStamp(Date processedByRepositoryTimeStamp) {
        this.processedByRepositoryTimeStamp = processedByRepositoryTimeStamp;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getState() {
        return state;
    }

    public long getUts() {
        return uts;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public int getDeviceConfig() {
        return deviceConfig;
    }

    public int getNodeId() {
        return nodeId;
    }

    public String getLocation() {
        return location;
    }

    public Boolean getQuickAccessMember() {
        return quickAccessMember;
    }

    public Date getProcessedByRepositoryTimeStamp() {
        return processedByRepositoryTimeStamp;
    }

    //Other methods
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("\nDevice:");
        sb.append("\nid: " + id);
        sb.append("\nName: " + name);
        sb.append("\ndeviceId: " + deviceId);
        sb.append("\ndeviceConfig: " + deviceConfig);
        sb.append("\nstate: " + state);
        sb.append("\nnodeId: " + nodeId);
        sb.append("\nuts: " + uts);
        sb.append("\nlocation: " + location);
        sb.append("\nquickAccessMember: " + quickAccessMember);
        sb.append("\nprocessedByRepositoryTimeStamp: " + processedByRepositoryTimeStamp);

        return sb.toString();
    }
}
