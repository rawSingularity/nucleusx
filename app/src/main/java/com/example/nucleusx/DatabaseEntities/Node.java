package com.example.nucleusx.DatabaseEntities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import com.example.nucleusx.TypeConverters.TimestampConverter;

import java.util.Date;

@Entity(tableName = "node_table")
@TypeConverters(TimestampConverter.class)
public class Node {

    @PrimaryKey (autoGenerate = true)
    private int id;

    private int nodeId;
    private String ip;
    private String serviceName;
    private Boolean registered;
    private String registeredTo;
    private Date registeredOn;
    private String firmwareBuildDate;
    private Date lastUpdated;


    public Node(int nodeId, String ip, Boolean registered, String registeredTo, Date registeredOn, String firmwareBuildDate, Date lastUpdated) {
        this.nodeId = nodeId;
        this.ip = ip;
        this.registered = registered;
        this.registeredTo = registeredTo;
        this.registeredOn = registeredOn;
        this.firmwareBuildDate = firmwareBuildDate;
        this.lastUpdated = lastUpdated;
        this.serviceName = "";
    }

    @Ignore
    public Node(String serviceName, int nodeId, String ip) {
        this.serviceName = serviceName;
        this.nodeId = nodeId;
        this.ip = ip;

        this.registered = true;
        this.registeredTo = "";
        this.registeredOn = new Date(0);
        this.firmwareBuildDate = "";
        this.lastUpdated = new Date(System.currentTimeMillis());
    }

    @Ignore
    public Node(String serviceName, int nodeId){
        this.serviceName = serviceName;
        this.nodeId = nodeId;

        this.ip = "";
        this.registered = false;
        this.registeredTo = "";
        this.registeredOn = new Date(0);
        this.firmwareBuildDate = "";
        this.lastUpdated = new Date(System.currentTimeMillis());
    }


    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getServiceName() {
        return serviceName;
    }

    public int getId() {
        return id;
    }

    public String getRegisteredTo() {
        return registeredTo;
    }

    public Date getRegisteredOn() {
        return registeredOn;
    }

    public String getFirmwareBuildDate() {
        return firmwareBuildDate;
    }

    public String getIp() {
        return ip;
    }

    public int getNodeId() {
        return nodeId;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }
}
