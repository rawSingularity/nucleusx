package com.example.nucleusx;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.nucleusx.Adapters.CalendarEventAdapter;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeApis.CalendarEventsApi;
import com.example.nucleusx.NodeApis.DeleteApi;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjCalendarEvents;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ManageEventsActivity extends AppCompatActivity {

    private static final String TAG = "ManageEventsActivity";
    HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();

    ControlItem incomingParcel;

    private ArrayList<CalendarEvent> mCalendarEventsList = new ArrayList<CalendarEvent>();
    private RecyclerView mRecyclerView;
    private CalendarEventAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_events);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.dsScheduleEventButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSchedulerActivity(incomingParcel);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        // To get data from Node Control Activity
        Intent intent = getIntent();
        incomingParcel = intent.getParcelableExtra("Device Info Parcel");
        if (incomingParcel != null) {
            setTitle("Manage Events for " + incomingParcel.getDeviceName());
            fetchAndPopulateEventsWithLatest(incomingParcel.getDeviceInfo().getD());
        }

        mRecyclerView = findViewById(R.id.ads2);
        //mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new CalendarEventAdapter(mCalendarEventsList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new CalendarEventAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(ManageEventsActivity.this, "Card at position " + position + " clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDeleteClick(int position) {
                showEventDeletionSnackbarForPosition(position);
            }

            @Override
            public void onRepeatSunClick(int position) {
                initiateRepeatMaskUpdate(Calendar.SUNDAY, position);
            }

            @Override
            public void onRepeatMonClick(int position) {
                initiateRepeatMaskUpdate(Calendar.MONDAY, position);
            }

            @Override
            public void onRepeatTueClick(int position) {
                initiateRepeatMaskUpdate(Calendar.TUESDAY, position);
            }

            @Override
            public void onRepeatWedClick(int position) {
                initiateRepeatMaskUpdate(Calendar.WEDNESDAY, position);
            }

            @Override
            public void onRepeatThuClick(int position) {
                initiateRepeatMaskUpdate(Calendar.THURSDAY, position);
            }

            @Override
            public void onRepeatFriClick(int position) {
                initiateRepeatMaskUpdate(Calendar.FRIDAY, position);
            }

            @Override
            public void onRepeatSatClick(int position) {
                initiateRepeatMaskUpdate(Calendar.SATURDAY, position);
            }
        });
    }

    public void fetchAndPopulateEventsWithLatest(int deviceId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + incomingParcel.getDeviceInfo().getIp() + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CalendarEventsApi calendarEventsApi = retrofit.create(CalendarEventsApi.class);
        Call<NodeResponseObjCalendarEvents> call = calendarEventsApi.getNodeResponseObjCalendarEventsForDevice(deviceId);
        call.enqueue(new Callback<NodeResponseObjCalendarEvents>() {
            @Override
            public void onResponse(Call<NodeResponseObjCalendarEvents> call, Response<NodeResponseObjCalendarEvents> response) {
                if (!response.isSuccessful()) {
                    Log.d("call Failure", "response failure. code: " + response.code());
                    return;
                }
                //Replace calendar events when they arrive
                NodeResponseObjCalendarEvents nodeResponseObjCalendarEvents = response.body();
                if (nodeResponseObjCalendarEvents.getCalendarEvents() != null) {
                    //first clear existing
                    if (!mCalendarEventsList.isEmpty()) {
                        mCalendarEventsList.clear();
                        mAdapter.notifyDataSetChanged();

                    }

                    if(nodeResponseObjCalendarEvents.getCalendarEvents() == null || nodeResponseObjCalendarEvents.getCalendarEvents().size() == 0){
                        findViewById(R.id.eventsDefaultBanner).setVisibility(View.VISIBLE);
                    }
                    else {
                        findViewById(R.id.eventsDefaultBanner).setVisibility(View.GONE);
                    }

                    for (CalendarEvent c : nodeResponseObjCalendarEvents.getCalendarEvents()) {
                        mCalendarEventsList.add(c);
                        Log.d(TAG, "device of entry inserted: d=" + c.getD());
                        //Notify the adapter of insertion
                        mAdapter.notifyItemInserted(mCalendarEventsList.indexOf(c));
                    }

                    sortmCalendarEventsList();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<NodeResponseObjCalendarEvents> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (incomingParcel != null) {
            fetchAndPopulateEventsWithLatest(incomingParcel.getDeviceInfo().getD());
        }
    }

    private void openSchedulerActivity(ControlItem parcel) {
        //Toast.makeText(DeviceSettingsActivity.this, "Preparing next flow for D" + parcel.getDeviceInfo().getD(), Toast.LENGTH_SHORT).show();
        // Flow into next Activity
        if (parcel == null) {
            //Snackbar.make(NodeControlActivity.this, "Data not yet ready.\nCannot flow into Controls", Snackbar.LENGTH_LONG).show();
            Log.d(TAG, "Error: unable to open scheduler - parcel is empty");
            return;
        }
        Intent intent = new Intent(ManageEventsActivity.this, ScheduleEventActivity.class);
        intent.putExtra("Device Info Parcel", parcel);
        startActivity(intent);
    }

    public void showEventDeletionSnackbarForPosition(final int position) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.manangeEventsCoordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "This will delete this event.\nThis action is undoable.", Snackbar.LENGTH_LONG)
                .setAction("DELETE!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteEventAtPosition(position);
                    }
                });
        snackbar.show();
    }

    public void initiateRepeatMaskUpdate(int calendarWeekDay, int position) {
        int currentRepeatMask = mCalendarEventsList.get(position).getR();
        int updatedRepeatMask = hardwareDescriptors.toggleDayForRepeatMask(currentRepeatMask, calendarWeekDay);
        Log.d(TAG, "currentRepeatMask: " + currentRepeatMask + "  updatedRepeatMask: " + updatedRepeatMask);

        //Here, we need to send the request to ESP to update the repeat mask

        int s = mCalendarEventsList.get(position).getS();
        int uts = mCalendarEventsList.get(position).getT();

        if (updatedRepeatMask == 0) {
            showEventDeletionSnackbarForPosition(position);
            if (incomingParcel != null) {
                fetchAndPopulateEventsWithLatest(incomingParcel.getDeviceInfo().getD());
            }
        } else {
            performSetEventRequest(s, uts, updatedRepeatMask);
        }
    }

    public void performSetEventRequest(final int s, final long t, final int r) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + incomingParcel.getDeviceInfo().getIp() + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjPostCalendarEventAddition(
                incomingParcel.getDeviceInfo().getD(),
                s,
                t,
                r,
                incomingParcel.getDeviceInfo().getNode()
        );
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }

                List<CalendarEvent> updatedEvents = response.body().getCalendarEvents();
                updateUiWithUpdatedEvents(updatedEvents);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void updateUiWithUpdatedEvents(List<CalendarEvent> updatedEvents) {
        if(mCalendarEventsList.size() == 0){
            findViewById(R.id.eventsDefaultBanner).setVisibility(View.VISIBLE);
        }
        else {
            findViewById(R.id.eventsDefaultBanner).setVisibility(View.GONE);
        }

        for (CalendarEvent currentEvent : mCalendarEventsList) {
            for (CalendarEvent latestEvent : updatedEvents) {
                if (currentEvent.getT() == latestEvent.getT() &&
                        currentEvent.getS() == latestEvent.getS()) {
                    if (currentEvent.getR() == latestEvent.getR()) {
                        Log.d(TAG, "latest event at idx: " + latestEvent.getJ() + " is same as current at idx:" + currentEvent.getJ());
                        break;
                    } else {
                        Log.d(TAG, "latest event at idx: " + latestEvent.getJ() + " is different in repeatMask as current at idx:" + currentEvent.getJ());
                        currentEvent.setR(latestEvent.getR());
                        mAdapter.notifyItemChanged(mCalendarEventsList.indexOf(currentEvent));
                        break;
                    }
                }
            }
        }
    }


    public void sortmCalendarEventsList() {
        if (mCalendarEventsList == null) {
            return;
        }
        if (mCalendarEventsList.size() <= 1) {
            return;
        }

        Collections.sort(mCalendarEventsList, new Comparator<CalendarEvent>() {
            @Override
            public int compare(CalendarEvent o1, CalendarEvent o2) {
                return (o2.getT() > o1.getT() ? -1 : (o1.getT() == o2.getT() ? 0 : 1));
            }
        });
    }

    public void deleteEventAtPosition(int position) {
        performDeleteEventRequest(mCalendarEventsList.get(position).getJ(), position);
    }

    public void performDeleteEventRequest(final int j, final int position) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + incomingParcel.getDeviceInfo().getIp() + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DeleteApi deleteApi = retrofit.create(DeleteApi.class);
        Call<NodeResponseObjCalendarEvents> call = deleteApi.getNodeResponseObjCalendarEventsPostEventDeletion(
                j,
                incomingParcel.getDeviceInfo().getNode()
        );
        call.enqueue(new Callback<NodeResponseObjCalendarEvents>() {
            @Override
            public void onResponse(Call<NodeResponseObjCalendarEvents> call, Response<NodeResponseObjCalendarEvents> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }
                //Replace calendar events when they arrive
                NodeResponseObjCalendarEvents nodeResponseObjCalendarEvents = response.body();
                if (nodeResponseObjCalendarEvents.getCalendarEvents() != null) {
                    //first clear existing
                    mCalendarEventsList.clear();
                    for (CalendarEvent c : nodeResponseObjCalendarEvents.getCalendarEvents()) {
                        mCalendarEventsList.add(c);
                    }

                    sortmCalendarEventsList();

                }

                removeItemAtPosition(position);
                Toast.makeText(ManageEventsActivity.this, "Event Successfully deleted!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<NodeResponseObjCalendarEvents> call, Throwable t) {

            }
        });
    }

    public void removeItemAtPosition(int position) {
        mCalendarEventsList.remove(position);
        mAdapter.notifyItemRemoved(position);
    }
}
