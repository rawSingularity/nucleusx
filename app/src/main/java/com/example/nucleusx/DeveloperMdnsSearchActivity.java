package com.example.nucleusx;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.nucleusx.ActivityParcels.MdnsCache;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class DeveloperMdnsSearchActivity extends AppCompatActivity {

    private static final String TAG = "mDNS";
    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;
    private NsdManager.ResolveListener resolveListener;
    private NsdServiceInfo nsdServiceInfo;

    private Boolean firstServiceSubmitted;
    private Boolean noPendingServicesLeftToResolve;

    private List<MdnsCache> mdnsCacheList = new ArrayList<>();
    ;

    private TextView vtvMockup;

    //private static final String SERVICE_TYPE = "_googlecast._tcp.";
    private static final String SERVICE_TYPE = "_nucleus._tcp.";
    // for all services
    //private static final String SERVICE_TYPE = "_services._dns-sd._udp";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_control_mock_up);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        vtvMockup = findViewById(R.id.tvMockup);
        firstServiceSubmitted = false;
        noPendingServicesLeftToResolve = false;

        nsdManager = (NsdManager) (getApplicationContext().getSystemService(Context.NSD_SERVICE));
        //initializeResolveListener();
        initializeDiscoveryListener();
        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }

    private void initializeDiscoveryListener() {
        discoveryListener = new NsdManager.DiscoveryListener() {
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                //nsdManager.stopServiceDiscovery(this);
                Log.d(TAG, "onStartDiscoveryFailed: errorCode:" + errorCode);
                vtvMockup.append("\nonStartDiscoveryFailed: errorCode:" + errorCode);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                nsdManager.stopServiceDiscovery(this);
                Log.d(TAG, "onStopDiscoveryFailed: errorCode:" + errorCode);
                vtvMockup.append("\nonStopDiscoveryFailed: errorCode:" + errorCode);
            }

            @Override
            public void onDiscoveryStarted(String serviceType) {
                Log.d(TAG, "onDiscoveryStarted");
                vtvMockup.append("\nonDiscoveryStarted");
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.d(TAG, "onDiscoverStopped");
                vtvMockup.append("\nonDiscoverStopped");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                Log.d(TAG, "onServiceFound");
                // A service was found!  Do something with it.
                String name = service.getServiceName();
                String type = service.getServiceType();
                vtvMockup.append("\nServiceFound: name:" + name + " type:" + type);
                Log.d(TAG, "\nService Found: " + name);

                mdnsCacheList.add(new MdnsCache(service));

                if (!firstServiceSubmitted) {
                    Log.d(TAG, "This is the first service to be resolved!!");
                    vtvMockup.append("\nThis is the first service to be resolved!");
                    nsdManager.resolveService(service, new MyResolveListener());
                    firstServiceSubmitted = true;
                }
                if(noPendingServicesLeftToResolve){
                    Log.d(TAG, "New service found to resolve");
                    vtvMockup.append("\n\nNew service found to resolve");
                    nsdManager.resolveService(service, new MyResolveListener());
                    noPendingServicesLeftToResolve = false;
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo serviceInfo) {

            }
        };
    }

    private class MyResolveListener implements NsdManager.ResolveListener {
        @Override
        public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            //your code
            Log.d(TAG, "onResolveFailed: errorCode:" + errorCode);
            vtvMockup.append("\n\nonResolveFailed: errorCode:" + errorCode);

            //Retry
            //Log.d(TAG, "Retrying resolving  " + serviceInfo.getServiceName());
            //nsdManager.resolveService(serviceInfo, new MyResolveListener());
            nsdManager.resolveService(getServiceToResolve(indexOfService(serviceInfo)), new MyResolveListener());
        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {

            nsdServiceInfo = serviceInfo;

            // Port is being returned as 9. Not needed.
            //int port = mServiceInfo.getPort();

            InetAddress host = nsdServiceInfo.getHost();
            String ipAddress = host.getHostAddress();
            Log.d(TAG, "Resolved address = " + ipAddress);
            vtvMockup.append("\nResolved address = " + ipAddress + " for " + serviceInfo.getServiceName());

            //Mark it as resolved
            markServiceAsResolved(serviceInfo, ipAddress);

            //Resolve new one
            //NsdServiceInfo newService = nowResolveThisService();
            NsdServiceInfo newService = getServiceToResolve(indexOfService(serviceInfo));
            if(newService != null){
                nsdManager.resolveService(nowResolveThisService(), new MyResolveListener());
            }
            else {
                Log.d(TAG, "ALL SERVICES RESOLVED !!!!");
                vtvMockup.append("\nALL SERVICES RESOLVED!!!!");
                noPendingServicesLeftToResolve = true;
            }

        }
    }

    private NsdServiceInfo nowResolveThisService() {
        for(MdnsCache c: mdnsCacheList){
            if(!c.getResolved()){
                Log.d(TAG, "Now resolving " + c.getServiceInfo().getServiceName());
                vtvMockup.append("\n\nNow resolving " + c.getServiceInfo().getServiceName());
                return c.getServiceInfo();
            }
        }
        return null;
    }

    private NsdServiceInfo getServiceToResolve(int lastProcessedIndex){
        NsdServiceInfo candidateService = null;

        for(MdnsCache c : mdnsCacheList){
            if(mdnsCacheList.indexOf(c) <= lastProcessedIndex){
                continue;
            }
            if(!c.getResolved()){
                candidateService = c.getServiceInfo();
                break;
            }
        }

        if(candidateService != null){
            return candidateService;
        }

        for(MdnsCache c : mdnsCacheList){
            if(!c.getResolved()){
                candidateService = c.getServiceInfo();
                break;
            }
        }

        return candidateService;
    }

    private int indexOfService(NsdServiceInfo service){
        for(MdnsCache c : mdnsCacheList){
            if(c.getServiceInfo().getServiceName().equalsIgnoreCase(service.getServiceName())){
                return mdnsCacheList.indexOf(c);
            }
        }
        Log.d(TAG, "Error! Service not found in MDNS Cache");
        vtvMockup.append("\nError! Service not found in MDNS Cache");
        return -1;
    }

    private void markServiceAsResolved(NsdServiceInfo serviceInfo, String ipAddress) {
        for(MdnsCache c : mdnsCacheList){
            if(c.getServiceInfo().getServiceName().equalsIgnoreCase(serviceInfo.getServiceName())){
                c.setResolved(true);
                Log.d(TAG, "Marked " + ipAddress + " as resolved");
                vtvMockup.append("\nMarked " + ipAddress + " as resolved for " + serviceInfo.getServiceName());
            }
        }
    }
}
