package com.example.nucleusx.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.Repositories.NodeRepository;

import java.util.Date;
import java.util.List;

public class NodeViewModel extends AndroidViewModel {

    private static final String TAG = "NodeViewModel";

    private NodeRepository repository;
    private LiveData<List<Node>> allNodes;
    private MutableLiveData<List<Node>> searchResults;

    //private LiveData<List<Devices>> quickAccessDevices;

    public NodeViewModel(@NonNull Application application) {
        super(application);

        repository = new NodeRepository(application);
        allNodes = repository.getAllNodes();
        searchResults = repository.getSearchResults();
    }

    public void insert (Node node){
        repository.insert(node);
    }

    public void update (Node node){
        repository.update(node);
    }

    public void updateIpForNodeId(Node node){
        repository.updateIpForNodeId(node);
    }

    public void delete (Node node){
        repository.delete(node);
    }

    public void deleteNodeForNodeId(int nodeId){
        repository.deleteNodeForNodeId(nodeId);
    }

    public void deleteUnregisteredNodes(){
        repository.deleteUnregisteredNodes();
    }

    public void deleteAllNodes (){
        repository.deleteAllNodes();
    }



    public LiveData<List<Node>> getAllNodes() {
        return allNodes;
    }

    public MutableLiveData<List<Node>> getSearchResults(){
        return searchResults;
    }

    public void findDataForNodeId(int nodeId){
        Log.d(TAG, "findDataForNodeId: " + nodeId);
        repository.getDataForNodeId(nodeId);
    }

    public void resetNode(int nodeId, String ip){
        repository.resetNode(nodeId, ip);
    }

    public void rebootNode(int nodeId, String ip){
        repository.rebootNode(nodeId, ip);
    }

    public void requestBeacon(int nodeId, String ip){
        repository.requestBeacon(nodeId, ip);
    }
}
