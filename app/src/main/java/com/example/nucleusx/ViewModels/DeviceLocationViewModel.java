package com.example.nucleusx.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.nucleusx.DatabaseEntities.DeviceLocation;
import com.example.nucleusx.Repositories.DeviceLocationRepository;

import java.util.List;

public class DeviceLocationViewModel extends AndroidViewModel {

    private DeviceLocationRepository repository;
    private LiveData<List<DeviceLocation>> allDeviceLocations;

    public DeviceLocationViewModel(@NonNull Application application) {
        super(application);

        repository = new DeviceLocationRepository(application);
        allDeviceLocations = repository.getAllDeviceLocations();
    }

    public void insert(DeviceLocation deviceLocation){
        repository.insert(deviceLocation);
    }

    public void update(DeviceLocation deviceLocation){
        repository.update(deviceLocation);
    }

    public void delete(DeviceLocation deviceLocation){
        repository.delete(deviceLocation);
    }

    public void deleteAllDeviceLocations(){
        repository.deleteAllDeviceLocations();
    }

    public LiveData<List<DeviceLocation>> getAllDeviceLocations() {
        return allDeviceLocations;
    }
}
