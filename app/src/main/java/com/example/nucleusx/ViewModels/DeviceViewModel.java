package com.example.nucleusx.ViewModels;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.Repositories.DeviceRepository;

import java.util.List;

public class DeviceViewModel extends AndroidViewModel {

    private DeviceRepository repository;
    private LiveData<List<Device>> allDevices;
    private LiveData<List<Device>> allQuickAccessDevices;
    private MutableLiveData<List<Device>> customDevices;

    public DeviceViewModel(@NonNull Application application) {
        super(application);

        repository = new DeviceRepository(application);
        allDevices = repository.getAllDevices();
        allQuickAccessDevices = repository.getAllQuickAccessDevices();
    }

    public void prepareCustomDevices(int nodeId, int deviceId){

    }



    public void insert(Device device){
        repository.insert(device);
    }

    public void update(Device device){
        repository.update(device);
    }

    public void delete(Device device){
        repository.delete(device);
    }

    public void setDeviceQuickAccessPreference(Device device){
        repository.setDeviceQuickAccessPreference(device);
    }

    public void refetchDeviceStateFromDevice(Device device, String ip){
        repository.refetchDeviceStateFromDevice(device, ip);
    }

    public void setDeviceLocation(Device device){
        repository.setDeviceLocation(device);
    }

    public void removeDevicesFromLocation(String location){
        repository.removeDevicesFromLocation(location);
    }

    public void removeDevicesForNode(int nodeId){
        repository.removeDevicesForNode(nodeId);
    }

    public void updateAllDeviceLocationIn(String oldLocation, String newLocation){
        repository.updateAllDeviceLocationIn(oldLocation, newLocation);
    }



    public void setDeviceState(Device device, String ip){
        repository.setDeviceState(device, ip);
    }

    public void deleteAll(){
        repository.deleteAllDevices();
    }

    public LiveData<List<Device>> getAllDevices(){
        return allDevices;
    }

    public MutableLiveData<List<Device>> getCustomDevices(){
        return customDevices;
    }

    public LiveData<List<Device>> getAllQuickAccessDevices() {
        return allQuickAccessDevices;
    }

    public LiveData<List<Device>> getDeviceInfo(int nodeId, int deviceId){
        return repository.getDeviceInfo(nodeId, deviceId);
    }

    public void refetchDeviceStatesViaMediator(List<Device> devices, List<Node> nodes){
        repository.refetchDeviceStatesViaMediator(devices, nodes);
    }
}
