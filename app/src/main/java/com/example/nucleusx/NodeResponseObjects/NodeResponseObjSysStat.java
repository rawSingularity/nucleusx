package com.example.nucleusx.NodeResponseObjects;

public class NodeResponseObjSysStat extends NodeBaseResponse {

    private int timezone;
    private int pcdUpdates;
    private int scheduledEvents;
    private int calendarEvents;
    private Session session;

    public int getTimezone() {
        return timezone;
    }

    public int getPcdUpdates() {
        return pcdUpdates;
    }

    public int getScheduledEvents() {
        return scheduledEvents;
    }

    public int getCalendarEvents() {
        return calendarEvents;
    }

    public Session getSession() {
        return session;
    }
}
