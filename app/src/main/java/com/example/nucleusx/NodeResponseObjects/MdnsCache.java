package com.example.nucleusx.NodeResponseObjects;

public class MdnsCache {
    private String node;
    private String ip;

    public String getNode() {
        return node;
    }
    public String getIp() {
        return ip;
    }
}
