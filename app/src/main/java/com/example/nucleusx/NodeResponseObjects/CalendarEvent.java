package com.example.nucleusx.NodeResponseObjects;

import android.os.Parcel;
import android.os.Parcelable;

public class CalendarEvent implements Parcelable {
    private int j;
    private int d;
    private int t;
    private String time;
    private int s;
    private int r;

    protected CalendarEvent(Parcel in) {
        j = in.readInt();
        d = in.readInt();
        t = in.readInt();
        time = in.readString();
        s = in.readInt();
        r = in.readInt();
    }

    public static final Creator<CalendarEvent> CREATOR = new Creator<CalendarEvent>() {
        @Override
        public CalendarEvent createFromParcel(Parcel in) {
            return new CalendarEvent(in);
        }

        @Override
        public CalendarEvent[] newArray(int size) {
            return new CalendarEvent[size];
        }
    };

    public int getJ() {
        return j;
    }
    public int getD() {
        return d;
    }
    public int getT() {
        return t;
    }
    public String getTime() {
        return time;
    }
    public int getS() {
        return s;
    }
    public int getR() {
        return r;
    }

    public CalendarEvent(int j, int d, int t, String time, int s, int r) {
        this.j = j;
        this.d = d;
        this.t = t;
        this.time = time;
        this.s = s;
        this.r = r;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(j);
        dest.writeInt(d);
        dest.writeInt(t);
        dest.writeString(time);
        dest.writeInt(s);
        dest.writeInt(r);
    }

    public void setR(int r) {
        this.r = r;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder("\nCalendarEvent obj:");
        sb.append("\nj:" + this.getJ());
        sb.append("\nd:" + this.getD());
        sb.append("\nt:" + this.getT());
        sb.append("\ntime:" + this.getTime());
        sb.append("\ns:" + this.getS());
        sb.append("\nr:" + this.getR());
        sb.append("\n");

        return sb.toString();
    }
}
