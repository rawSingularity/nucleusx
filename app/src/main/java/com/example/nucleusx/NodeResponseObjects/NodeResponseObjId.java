package com.example.nucleusx.NodeResponseObjects;

public class NodeResponseObjId extends NodeBaseResponse {

    private String mac;
    private FirmwareVersion current;
    private FirmwareVersion previous;

    public String getMac() {
        return mac;
    }
    public FirmwareVersion getCurrent() {
        return current;
    }
    public FirmwareVersion getPrevious() {
        return previous;
    }

}
