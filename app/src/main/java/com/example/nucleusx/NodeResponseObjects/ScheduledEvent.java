package com.example.nucleusx.NodeResponseObjects;

public class ScheduledEvent {
    private int j;
    private int d;
    private int t;
    private String time;
    private int s;

    public int getJ() {
        return j;
    }
    public int getD() {
        return d;
    }
    public int getT() {
        return t;
    }
    public String getTime() {
        return time;
    }
    public int getS() {
        return s;
    }
}
