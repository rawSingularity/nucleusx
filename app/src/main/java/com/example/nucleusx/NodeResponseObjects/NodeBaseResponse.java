package com.example.nucleusx.NodeResponseObjects;

import com.google.gson.annotations.SerializedName;

public abstract class NodeBaseResponse {
    private String ip;
    private int node;
    private int UTS;
    private String time;
    @SerializedName("request processing time (ms)")
    private int reqProcessingTime;

    /*public NodeBaseResponse(String ip, int node, int UTS, String time) {
        this.ip = ip;
        this.node = node;
        this.UTS = UTS;
        this.time = time;
    }*/

    public int getReqProcessingTime() {
        return reqProcessingTime;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setNode(int node) {
        this.node = node;
    }

    public void setUTS(int UTS) {
        this.UTS = UTS;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIp() {
        return ip;
    }

    public int getNode() {
        return node;
    }

    public int getUTS() {
        return UTS;
    }

    public String getTime() {
        return time;
    }
}
