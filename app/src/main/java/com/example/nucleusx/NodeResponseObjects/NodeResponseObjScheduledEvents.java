package com.example.nucleusx.NodeResponseObjects;

import java.util.List;

public class NodeResponseObjScheduledEvents extends NodeBaseResponse{

    private List<ScheduledEvent> scheduledEvents;

    public List<ScheduledEvent> getScheduledEvents() {
        return scheduledEvents;
    }
}
