package com.example.nucleusx.NodeResponseObjects;

import java.util.List;

public class NodeResponseObjDeviceList extends NodeBaseResponse{

    private List<DeviceElement> deviceList;

    public List<DeviceElement> getDeviceList() {
        return deviceList;
    }
}
