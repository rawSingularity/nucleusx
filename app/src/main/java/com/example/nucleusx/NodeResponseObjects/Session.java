package com.example.nucleusx.NodeResponseObjects;

import com.google.gson.annotations.SerializedName;

public class Session {

    private String uptime;
    private int heap;
    private int requests;
    private int invalidRequests;
    private int clientTo;
    @SerializedName("WiFi signal strength (dBm)")
    private int wifiSignalStrength;
    private boolean timeSynced;
    private int wifiDisconnects;

    public String getUptime() {
        return uptime;
    }
    public int getHeap() {
        return heap;
    }
    public int getRequests() {
        return requests;
    }
    public int getInvalidRequests() {
        return invalidRequests;
    }
    public int getClientTo() {
        return clientTo;
    }
    public boolean isTimeSynced() {
        return timeSynced;
    }
    public int getWifiSignalStrength() {
        return wifiSignalStrength;
    }
    public int getWifiDisconnects() {
        return wifiDisconnects;
    }

}
