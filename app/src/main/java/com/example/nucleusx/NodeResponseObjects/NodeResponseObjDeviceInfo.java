package com.example.nucleusx.NodeResponseObjects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NodeResponseObjDeviceInfo extends NodeBaseResponse {
    private int d;
    private int c;
    private String n;
    private String m;
    private int s;
    private int g;
    private List<CalendarEvent> calendarEvents;

    public NodeResponseObjDeviceInfo(int d) {
        this.d = d;
    }

    public String getN() {
        return n;
    }

    public int getD() {
        return d;
    }

    public int getC() {
        return c;
    }

    public String getM() {
        return m;
    }

    public int getS() {
        return s;
    }

    public int getG() {
        return g;
    }

    public List<CalendarEvent> getCalendarEvents() {
        return calendarEvents;
    }

    //Setters

    public void setS(int s) {
        this.s = s;
    }

    public void setD(int d) {
        this.d = d;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void setN(String n) {
        this.n = n;
    }

    public void setM(String m) {
        this.m = m;
    }

    public void setG(int g) {
        this.g = g;
    }

    public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
        this.calendarEvents = calendarEvents;
    }

}
