package com.example.nucleusx.NodeResponseObjects;

import java.util.List;

public class NodeResponseObjMdnsCache extends NodeBaseResponse {

    private List<MdnsCache> mdnsCache;

    public List<MdnsCache> getMdnsCache() {
        return mdnsCache;
    }
}
