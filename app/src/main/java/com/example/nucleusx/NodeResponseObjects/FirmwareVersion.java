package com.example.nucleusx.NodeResponseObjects;

public class FirmwareVersion {

    private String version;
    private String build;
    private String fBoot;

    public String getVersion() {
        return version;
    }
    public String getBuild() {
        return build;
    }
    public String getfBoot() {
        return fBoot;
    }
}
