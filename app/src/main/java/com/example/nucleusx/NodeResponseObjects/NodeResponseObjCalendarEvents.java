package com.example.nucleusx.NodeResponseObjects;

import java.util.List;

public class NodeResponseObjCalendarEvents extends NodeBaseResponse {

    private List<CalendarEvent> calendarEvents;

    public List<CalendarEvent> getCalendarEvents() {
        return calendarEvents;
    }

}
