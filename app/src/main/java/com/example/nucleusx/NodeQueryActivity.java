package com.example.nucleusx;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeApis.CalendarEventsApi;
import com.example.nucleusx.NodeApis.IdApi;
import com.example.nucleusx.NodeApis.MdnsCacheApi;
import com.example.nucleusx.NodeApis.ScheduledEventsApi;
import com.example.nucleusx.NodeApis.SysStatApi;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;
import com.example.nucleusx.NodeResponseObjects.MdnsCache;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjCalendarEvents;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjId;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjMdnsCache;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjScheduledEvents;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjSysStat;
import com.example.nucleusx.NodeResponseObjects.ScheduledEvent;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NodeQueryActivity extends AppCompatActivity {

    public static final String TAG = "NodeQueryActivity";

    //Input
    private EditText ipInput;
    private Button vbutton_fetch_sys_stat;
    //sysStat
    private TextView vtvNodeId;
    private TextView vtvTime;
    private TextView vtvUts;
    private TextView vtvTzValue;
    private TextView vtvTzRegion;
    private TextView vtvSignalStrength;
    private TextView vtvUptime;
    private TextView vtvHeap;
    private TextView vtvValidRequests;
    private TextView vtvInValidRequests;
    private TextView vtvClientTo;
    private TextView vtvPcdUpdates;
    private TextView vtvTimSync;
    private TextView vtvReqTimeSysStat;
    private TextView vtvWifiDisconnects;
    //id
    private TextView vtvMacAddress;
    private TextView vtvCurrentFirmware;
    private TextView vtvPreviousFirmware;
    private TextView vtvCurrentFirmwareBuild;
    private TextView vtvPreviousFirmwareBuild;
    private TextView vtvReqTimId;
    // calendar events
    private TextView vtvReqCalEvents;
    private TextView vtitleCalendarEvent;
    private TextView vtvCalendarEvents;
    // scheduled events
    private TextView vtvReqSchEvents;
    private TextView vtitleUpcomingEvent;
    private TextView vtvUpcomingEvents;

    // mdns cache
    private TextView vtvReqMdnsCache;
    private TextView vtvMdnsCache;

    //Email body
    private String emailBodyString;

    private CardView vcardNodeControls;

    private ScannedNodeItem parcelForNextView = null;

    private HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_query);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Node Query Tool");

        FloatingActionButton fab = findViewById(R.id.fab);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ipInput = findViewById(R.id.edit_text_input_ip);
        vbutton_fetch_sys_stat = findViewById(R.id.button_fetch_sys_stat);
        vtvNodeId = findViewById(R.id.tvNodeId);
        vtvTime = findViewById(R.id.tvTime);
        vtvUts = findViewById(R.id.tvUts);
        vtvTzValue = findViewById(R.id.tvTzValue);
        vtvTzRegion = findViewById(R.id.tvTzRegion);
        vtvSignalStrength = findViewById(R.id.tvSignalStrength);
        vtvUptime = findViewById(R.id.tvUptime);
        vtvHeap = findViewById(R.id.tvHeap);
        vtvValidRequests = findViewById(R.id.tvValidRequests);
        vtvInValidRequests = findViewById(R.id.tvInValidRequests);
        vtvClientTo = findViewById(R.id.tvClientTo);
        vtvPcdUpdates = findViewById(R.id.tvPcdUpdates);
        vtvTimSync = findViewById(R.id.tvTimSync);
        vtvReqTimeSysStat = findViewById(R.id.tvReqTimeSysStat);
        vtvWifiDisconnects = findViewById(R.id.tvWifiDisconnects);

        //
        vtvMacAddress = findViewById(R.id.tvMacAddress);
        vtvCurrentFirmware = findViewById(R.id.tvCurrentFirmware);
        vtvPreviousFirmware = findViewById(R.id.tvPreviousFirmware);
        vtvCurrentFirmwareBuild = findViewById(R.id.tvCurrentFirmwareBuild);
        vtvPreviousFirmwareBuild = findViewById(R.id.tvPreviousFirmwareBuild);
        vtvReqTimId = findViewById(R.id.tvReqTimId);

        //
        vtvReqCalEvents = findViewById(R.id.tvReqCalEvents);
        vtitleCalendarEvent = findViewById(R.id.titleCalendarEvent);
        vtvCalendarEvents = findViewById(R.id.tvCalendarEvents);
        //
        vtvReqSchEvents = findViewById(R.id.tvReqSchEvents);
        vtitleUpcomingEvent = findViewById(R.id.titleUpcomingEvent);
        vtvUpcomingEvents = findViewById(R.id.tvUpcomingEvents);
        //
        vtvReqMdnsCache = findViewById(R.id.tvReqMdnsCache);
        vtvMdnsCache = findViewById(R.id.tvMdnsCache);

        vcardNodeControls = findViewById(R.id.cardNodeControls);

        // To get data from Node Scanner Activity
        Intent intent = getIntent();
        ScannedNodeItem nodeItem = intent.getParcelableExtra("Node Item");
        if(nodeItem != null){
            ipInput.setText(nodeItem.getNodeIp());
            //Simulate click
            vbutton_fetch_sys_stat.performClick();
            //Hide components
            ipInput.setVisibility(View.GONE);
            vbutton_fetch_sys_stat.setVisibility(View.GONE);
        }

        //for next flow
        vcardNodeControls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(NodeQueryActivity.this, "Preparing next flow", Toast.LENGTH_SHORT).show();
                // Flow into next Activity
                if(parcelForNextView == null){
                    Snackbar.make(v, "Data not yet ready.\nCannot flow into Controls", Snackbar.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(NodeQueryActivity.this, NodeControlActivity.class);
                intent.putExtra("Node Item", parcelForNextView);
                startActivity(intent);
            }
        });


        //Testing
        Calendar c = Calendar.getInstance();
        c.get(Calendar.ZONE_OFFSET);
        Log.d(TAG, "Calendar.Zone_offset=" + c.get(Calendar.ZONE_OFFSET));


    }

    public void onEmailRunner(View view) {

        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).show();

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","pp423+nucleus@cornell.edu", null));

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Nucleus Node Diagnostic Data");
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailBodyString);

        startActivity(Intent.createChooser(emailIntent, "Send nucleus node diagnostics via email"));
    }

    public void onFetchRunner(final View view) {

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        // Get input IP
        String input = ipInput.getText().toString();


        if (!hardwareDescriptors.validIp(input)) {
            Toast.makeText(this, "Provided input is invalid: " + input + "\nProvide a valid node IP Address", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Requesting data from Nucleus Node. Please wait...", Toast.LENGTH_LONG).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + input + ":80/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // (1) System Status API
        SysStatApi sysStatApi = retrofit.create(SysStatApi.class);
        Call<NodeResponseObjSysStat> call = sysStatApi.getNodeResponseObjSysStat();
        call.enqueue(new Callback<NodeResponseObjSysStat>() {
            @Override
            public void onResponse(Call<NodeResponseObjSysStat> call, Response<NodeResponseObjSysStat> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeQueryActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Toast.makeText(NodeQueryActivity.this, "Received: sysStat", Toast.LENGTH_SHORT).show();
                Snackbar.make(view, "Received: System Status", Snackbar.LENGTH_LONG).show();
                NodeResponseObjSysStat r = response.body();

                //Get the parcel for next view ready
                parcelForNextView = new ScannedNodeItem(
                        r.getNode(),
                        r.getIp(),"");

                // Add to Email body
                emailBodyString += "\n\nRequest: /get/sysStat\nResponse: \n";
                emailBodyString += response.toString();

                setTextView(vtvNodeId, "Node Id: " + r.getNode());
                setTextView(vtvTime, r.getTime().toString());
                setTextView(vtvUts, "Unix TS: " + r.getUTS());
                setTextView(vtvTzValue, "tz minutes: " + r.getTimezone());
                setTextView(vtvTzRegion, "Time-zone: GMT" + hardwareDescriptors.getFormattedTimeZoneFromMinutes(r.getTimezone()));
                setTextView(vtvSignalStrength,r.getSession().getWifiSignalStrength() + " dBm");
                setTextView(vtvUptime, r.getSession().getUptime().toString().trim());
                setTextView(vtvHeap, r.getSession().getHeap() + " B");
                setTextView(vtvValidRequests, String.valueOf(r.getSession().getRequests()));
                setTextView(vtvInValidRequests, String.valueOf(r.getSession().getInvalidRequests()));
                setTextView(vtvClientTo, String.valueOf(r.getSession().getClientTo()));
                setTextView(vtvPcdUpdates, String.valueOf(r.getPcdUpdates()));
                setTextView(vtvReqTimeSysStat, r.getReqProcessingTime() + " ms");
                if(r.getSession().isTimeSynced()){
                    setTextView(vtvTimSync, "Successful");
                }
                else {
                    setTextView(vtvTimSync, "Unsuccessful");
                }
                setTextView(vtvWifiDisconnects, r.getSession().getWifiDisconnects() + " times");
            }

            @Override
            public void onFailure(Call<NodeResponseObjSysStat> call, Throwable t) {
                Toast.makeText(NodeQueryActivity.this, "Failure " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("sysCall Failure", t.getMessage());
            }
        });

        // (2) ID
        IdApi idApi = retrofit.create(IdApi.class);
        Call<NodeResponseObjId> callId = idApi.getNodeResponseObjId();
        callId.enqueue(new Callback<NodeResponseObjId>() {
            @Override
            public void onResponse(Call<NodeResponseObjId> call, Response<NodeResponseObjId> response) {
                if (!response.isSuccessful()) {
                    Log.d("idCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeQueryActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Toast.makeText(NodeQueryActivity.this, "Received: id", Toast.LENGTH_SHORT).show();
                Snackbar.make(view, "Received: System Identity", Snackbar.LENGTH_LONG).show();
                NodeResponseObjId r = response.body();

                // Add to Email body
                emailBodyString += "\n\nRequest: /get/id\nResponse: \n";
                emailBodyString += response.toString();

                setTextView(vtvMacAddress, "MAC: " + r.getMac().toUpperCase());
                setTextView(vtvCurrentFirmware, "Current: " + r.getCurrent().getVersion().trim().substring(r.getCurrent().getVersion().toString().length() - 6));
                setTextView(vtvCurrentFirmwareBuild, "built: " + r.getCurrent().getBuild().trim());
                setTextView(vtvPreviousFirmware, "Prev.: " + r.getPrevious().getVersion().trim().substring(r.getPrevious().getVersion().toString().length() - 6));
                setTextView(vtvPreviousFirmwareBuild, "built: " + r.getPrevious().getBuild().trim());
                setTextView(vtvReqTimId, r.getReqProcessingTime() + " ms");
                //common
                setTextView(vtvTime, r.getTime().toString());
                setTextView(vtvUts, "Unix TS: " + r.getUTS());
            }

            @Override
            public void onFailure(Call<NodeResponseObjId> call, Throwable t) {

            }
        });


        // (4) Scheduled Events
        ScheduledEventsApi scheduledEventsApi = retrofit.create(ScheduledEventsApi.class);
        Call<NodeResponseObjScheduledEvents> callSchEvents = scheduledEventsApi.getNodeResponseObjScheduledEvents();
        callSchEvents.enqueue(new Callback<NodeResponseObjScheduledEvents>() {
            @Override
            public void onResponse(Call<NodeResponseObjScheduledEvents> call, Response<NodeResponseObjScheduledEvents> response) {
                if (!response.isSuccessful()) {
                    Log.d("schEventsCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeQueryActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Toast.makeText(NodeQueryActivity.this, "Received: id", Toast.LENGTH_SHORT).show();
                Snackbar.make(view, "Received: Scheduled Events", Snackbar.LENGTH_LONG).show();

                NodeResponseObjScheduledEvents r = response.body();

                // Add to Email body
                emailBodyString += "\n\nRequest: /get/id\nResponse: \n";
                emailBodyString += response.toString();

                setTextView(vtvReqSchEvents, r.getReqProcessingTime() + " ms");
                Log.d("calendarEvents size", r.getScheduledEvents().size() + " events");

                StringBuilder sb = new StringBuilder();
                if(r.getScheduledEvents().size() == 0){
                    setTextView(vtitleUpcomingEvent, "No Upcoming Event");

                }
                else {
                    setTextView(vtitleUpcomingEvent, r.getScheduledEvents().size() + " Upcoming Event");
                    if(r.getScheduledEvents().size() > 1){
                        vtitleUpcomingEvent.append("s");
                    }

                    for(ScheduledEvent se : r.getScheduledEvents()){
                        sb.append("\nDevice at D" + se.getD()+ " will be set to " + se.getS()+"\n");
                        sb.append("at " + se.getTime() + "\n");
                    }
                }
                setTextView(vtvUpcomingEvents, sb.toString());

                //common
                setTextView(vtvTime, r.getTime().toString());
                setTextView(vtvUts, "Unix TS: " + r.getUTS());

            }

            @Override
            public void onFailure(Call<NodeResponseObjScheduledEvents> call, Throwable t) {

            }

        });


        // (3) Calendar Events
        CalendarEventsApi calendarEventsApi = retrofit.create(CalendarEventsApi.class);
        Call<NodeResponseObjCalendarEvents> callCalendarEvents = calendarEventsApi.getNodeResponseObjCalendarEvents();
        callCalendarEvents.enqueue(new Callback<NodeResponseObjCalendarEvents>() {
            @Override
            public void onResponse(Call<NodeResponseObjCalendarEvents> call, Response<NodeResponseObjCalendarEvents> response) {
                if (!response.isSuccessful()) {
                    Log.d("calEventsCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeQueryActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Toast.makeText(NodeQueryActivity.this, "Received: id", Toast.LENGTH_SHORT).show();
                Snackbar.make(view, "Received: Calendar Events", Snackbar.LENGTH_LONG).show();
                NodeResponseObjCalendarEvents r = response.body();

                // Add to Email body
                emailBodyString += "\n\nRequest: /get/id\nResponse: \n";
                emailBodyString += response.toString();

                setTextView(vtvReqCalEvents, r.getReqProcessingTime() + " ms");
                Log.d("calendarEvents size", r.getCalendarEvents().size() + " events");

                StringBuilder sb = new StringBuilder();
                if(r.getCalendarEvents().size() == 0){
                    setTextView(vtitleCalendarEvent, "No Calendar Events");

                }
                else {
                    setTextView(vtitleCalendarEvent, r.getCalendarEvents().size() + " Calendar Event");
                    if(r.getCalendarEvents().size() > 1){
                        vtitleCalendarEvent.append("s");
                    }

                    for(CalendarEvent ce : r.getCalendarEvents()){
                        sb.append("\nDevice at to D" + ce.getD()+ " will be set to " + ce.getS()+"\n");
                        if(ce.getR() == 0){
                            // no repeat mask
                            sb.append("at " + ce.getTime() + "\n");
                        }
                        else {
                            sb.append("at " + ce.getTime().substring(ce.getTime().length()-14) + " every\n");
                            sb.append(hardwareDescriptors.getDaysStringFromRepeatMask(ce.getR()) + "\n");
                        }
                    }
                }
                setTextView(vtvCalendarEvents, sb.toString());

                //common
                setTextView(vtvTime, r.getTime().toString());
                setTextView(vtvUts, "Unix TS: " + r.getUTS());
            }

            @Override
            public void onFailure(Call<NodeResponseObjCalendarEvents> call, Throwable t) {

            }
        });


        // (5) MDNS Cache
        MdnsCacheApi mdnsCacheApi = retrofit.create(MdnsCacheApi.class);
        Call<NodeResponseObjMdnsCache> callMdns = mdnsCacheApi.getNodeResponseObjMdnsCache();
        callMdns.enqueue(new Callback<NodeResponseObjMdnsCache>() {
            @Override
            public void onResponse(Call<NodeResponseObjMdnsCache> call, Response<NodeResponseObjMdnsCache> response) {
                if (!response.isSuccessful()) {
                    Log.d("schEventsCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeQueryActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                //Toast.makeText(NodeQueryActivity.this, "Received: id", Toast.LENGTH_SHORT).show();
                Snackbar.make(view, "Received: mDns Cache", Snackbar.LENGTH_LONG).show();
                NodeResponseObjMdnsCache r = response.body();

                // Add to Email body
                emailBodyString += "\n\nRequest: /get/id\nResponse: \n";
                emailBodyString += response.toString();

                setTextView(vtvReqMdnsCache, r.getReqProcessingTime() + " ms");
                Log.d("mdnsCache size", r.getMdnsCache().size() + " events");

                StringBuilder sb = new StringBuilder();
                if(r.getMdnsCache().size() == 0){
                    sb.append("No mDNS Entries");
                }

                for(MdnsCache mdnsCache : r.getMdnsCache()){
                    sb.append("\nNode ID: " + mdnsCache.getNode() + "\n");
                    sb.append("IP Address: " + mdnsCache.getIp() + "\n");
                }
                setTextView(vtvMdnsCache, sb.toString());

                //common
                setTextView(vtvTime, r.getTime().toString());
                setTextView(vtvUts, "Unix TS: " + r.getUTS());
            }

            @Override
            public void onFailure(Call<NodeResponseObjMdnsCache> call, Throwable t) {

            }
        });
    }

    private void setTextView(TextView tv, String s) {
        tv.setText(s);
        tv.setTextColor(Color.BLACK);
        tv.setTypeface(null, Typeface.BOLD);
    }
}
