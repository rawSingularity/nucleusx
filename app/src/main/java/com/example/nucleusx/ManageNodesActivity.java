package com.example.nucleusx;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.Adapters.NodeAdapter;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.Utilities.MdnsServiceQuery;
import com.example.nucleusx.Utilities.WiFiScannerService;
import com.example.nucleusx.Utilities.WifiConnectionService;
import com.example.nucleusx.ViewModels.DeviceViewModel;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.util.List;

import static com.example.nucleusx.Utilities.WiFiScannerService.WIFI_PERMISSION_IDENTIFIER;

public class ManageNodesActivity extends AppCompatActivity {

    private static final String TAG = "ManageNodesActivity";
    private NodeViewModel nodeViewModel;
    private DeviceViewModel deviceViewModel;
    //private List<Node> dbNodeList;
    private NodeAdapter mAdapter;

    private WiFiScannerService mWifiScanner;
    MdnsServiceQuery mdnsServiceQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_nodes);
        setTitle("Manage Nodes");

        //Setup Wifi network scanner service
        mWifiScanner = new WiFiScannerService(this);
        if(!mWifiScanner.hasSelfPermission()){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, WIFI_PERMISSION_IDENTIFIER);
        }
        else {
            mWifiScanner.initiateScan();
        }

        //Setup Mdns query/scanner service
        mdnsServiceQuery = MdnsServiceQuery.getInstance(this);
        mdnsServiceQuery.searchForServices();

        //Setup Views
        final RecyclerView recyclerView = findViewById(R.id.manageNodesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        mAdapter = new NodeAdapter();
        recyclerView.setAdapter(mAdapter);

        //Setup NodeViewModel
        //Setup live data observer from database
        nodeViewModel = ViewModelProviders.of(this).get(NodeViewModel.class);
        nodeViewModel.getAllNodes().observe(this, new Observer<List<Node>>() {
            @Override
            public void onChanged(@Nullable List<Node> nodes) {
                Log.d(TAG, "getAllNodes: onChanged: size:" + nodes.size());

                for(Node n : nodes){
                    Log.d(TAG, "onChanged: Node:" + n.getNodeId() + " reg:" + n.getRegistered());
                }

                //update recyclerView
                //dbNodeList = nodes;
                //mAdapter.setmNodes(nodes);
                mAdapter.submitList(nodes);
                recyclerView.smoothScrollToPosition(0);
            }
        });

        //Setup DeviceViewModel
        deviceViewModel = ViewModelProviders.of(this).get(DeviceViewModel.class);

        //Setup Adapter listeners
        mAdapter.setOnItemClickListener(new NodeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Node node) {
                //if(!dbNodeList.get(position).getRegistered()){
                if(!node.getRegistered()){
                    alertUserOfNetworkInterruption(node.getServiceName());
                }
            }

            @Override
            public void onLocateClick(Node node) {
                requestBeacon(node.getNodeId(), node.getIp());
            }

            @Override
            public void onConfigureClick(Node node) {
                ScannedNodeItem parcel = new ScannedNodeItem(node.getNodeId(), node.getIp(), "");

                Intent intent = new Intent(ManageNodesActivity.this, NodeControlActivity.class);
                intent.putExtra("Node Item", parcel);
                startActivity(intent);
            }

            @Override
            public void onMoreInfoClick(Node node) {

                ScannedNodeItem parcel = new ScannedNodeItem(
                        node.getNodeId(),
                        node.getIp(),
                        "");

                Intent intent = new Intent(ManageNodesActivity.this, NodeQueryActivity.class);
                intent.putExtra("Node Item", parcel);
                startActivity(intent);
            }

            @Override
            public void onRebootClick(Node node) {
                int nodeId = node.getNodeId();
                String nodeIp = node.getIp();
                showNodeRebootSnackbarForPosition(nodeId, nodeIp);
            }

            @Override
            public void onRemoveSettingsClick(View view, Node node) {
                // inflate menu
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.node_more_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new NodeMoreMenuItemClickListener(node));
                popup.show();
            }

            /*
            @Override
            public void onDeleteClick(int position) {
                Toast.makeText(ManageNodesActivity.this, "Initiate factory-reset and node-purge activities", Toast.LENGTH_SHORT).show();
            }*/

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (requestCode == WIFI_PERMISSION_IDENTIFIER) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            mWifiScanner.initiateScan();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mWifiScanner, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        scanForNodes();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mWifiScanner);
    }

    class NodeMoreMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private Node node;

        public NodeMoreMenuItemClickListener(Node node) {
            this.node = node;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            switch (menuItem.getItemId()) {

                case R.id.mn_action_perform_factory_reset:
                    showNodeResetSnackbarForPosition(node);
                    return true;

                /*case R.id.mn_action_add_to_local_group:
                    Toast.makeText(ManageNodesActivity.this, "TODO: add to local group", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.mn_action_remove_from_local_group:
                    Toast.makeText(ManageNodesActivity.this, "TODO: remove from local group", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.mn_action_wifi_settings:
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    return true;*/

                default:
                    return false;
            }
        }
    }

    private void showNodeResetSnackbarForPosition(final Node node) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.manageNodesCoordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Are you sure you want to factory-reset this node? This action is undoable", Snackbar.LENGTH_LONG)
                .setAction("FACTORY\nRESET!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertUserAndPerformNodeValidation(node);
                    }
                });
        snackbar.show();
    }

    private void alertUserOfNetworkInterruption(final String ssid){

        //Parent layout
        Context context = getApplicationContext();
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        int padding_in_dp = 16;  // 16 dps
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);

        layout.setPadding(padding_in_px, padding_in_px, padding_in_px, padding_in_px);

        //Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Node Network Setup");

        //Info
        StringBuilder sb = new StringBuilder();
        sb.append("Connect to the Node, select your Wifi network & provide Wifi credentials.\n\n");
        sb.append("Upon succesful connection of the node to your Wifi network, the Node network will disappear.\n\n");
        sb.append("This device will temporarily disconnect from your WiFi network during this process & reconnect back thereafter.");
        final TextView info = new TextView(this);
        info.setText(sb.toString());
        layout.addView(info);

        //Set layout to dialog
        builder.setView(layout);

        builder.setPositiveButton("OK; PROCEED.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WifiConnectionService wifiConnectionService = new WifiConnectionService(getApplicationContext());
                wifiConnectionService.initiateConnection(ssid);

                // go to settings
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ManageNodesActivity.this, "Node setup cancelled by user", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

    private void alertUserAndPerformNodeValidation(final Node node) {

        //Parent layout
        Context context = getApplicationContext();
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        int padding_in_dp = 16;  // 16 dps
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);

        layout.setPadding(padding_in_px, padding_in_px, padding_in_px, padding_in_px);

        //Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Heads Up!");

        //Info
        StringBuilder sb = new StringBuilder();
        sb.append("You are initiating a factory-reset on the node");
        sb.append("\nYou understand that:\n\n");
        sb.append("1. All the associated devices will be removed\n");
        sb.append("2. The Node will be taken off your local WiFi network\n");
        sb.append("3. This action is undoable\n\n");
        sb.append("If you still wish to proceed, enter the Serial Id of the node that you are trying to reset:");
        final TextView info = new TextView(this);
        info.setText(sb.toString());
        layout.addView(info);

        //Input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setMaxLines(1);
        input.setHint("Node Id");
        layout.addView(input);

        //Set layout to dialog
        builder.setView(layout);

        builder.setPositiveButton("PERFORM FACTORY RESET", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String inputNodeId = input.getText().toString();
                if(inputNodeId == null){
                    inputNodeId = "";
                }

                int nodeId = node.getNodeId();
                String ip = node.getIp();
                Log.d(TAG, " nodeId:" + nodeId + " ip:" + ip);

                Log.d(TAG, "Input: " + inputNodeId + " nodeId:" + nodeId);

                if(nodeId == Integer.parseInt(inputNodeId)){
                    Toast.makeText(ManageNodesActivity.this, "Initiating factory reset of Node " + nodeId, Toast.LENGTH_LONG).show();
                    performNodeReset(nodeId, ip);
                }
                else{
                    Toast.makeText(ManageNodesActivity.this, "Incorrect Node Id. Factory-reset operation aborted.", Toast.LENGTH_LONG).show();
                }
            }
        });

        builder.setNegativeButton("CANCEL & EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(ManageNodesActivity.this, "Factory-reset operation cancelled by user", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

    private void performNodeReset(int nodeId, String ip){
        //1. Send reset request to Node
        nodeViewModel.resetNode(nodeId, ip);

        //2. Remove all devices associated with this node
        deviceViewModel.removeDevicesForNode(nodeId);

        //3. Delete Node from database
        nodeViewModel.deleteNodeForNodeId(nodeId);
    }

    private void showNodeRebootSnackbarForPosition(final int nodeId, final String ip) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.manageNodesCoordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "This will reboot all the associated Node devices. Are you sure?", Snackbar.LENGTH_LONG)
                .setAction("REBOOT!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nodeViewModel.rebootNode(nodeId, ip);
                        CoordinatorLayout coordinatorLayout = findViewById(R.id.manageNodesCoordinatorLayout);
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Reboot initiated. It can take upto 30 seconds for devices to come online.", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
        snackbar.show();
    }

    private void requestBeacon(int nodeId, String ip) {
        nodeViewModel.requestBeacon(nodeId, ip);
        CoordinatorLayout coordinatorLayout = findViewById(R.id.manageNodesCoordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Beacon turned on. Look for flashing light on device. It will turn off in 20 seconds", Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.node_scan_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.scan_and_refetch_nodes:
                scanForNodes();
                return true;

            /*case R.id.refresh_nodes:
                refetchNodeIps();
                return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    void scanForNodes() {
        //Toast.makeText(ManageNodesActivity.this, "For now, search-for-nodes is only available from home screen", Toast.LENGTH_SHORT).show();
        mdnsServiceQuery.searchForServices();
        mWifiScanner.initiateScan();
    }

    void refetchNodeIps() {
        mdnsServiceQuery.refetchIpsForExistingNodes();
    }
}
