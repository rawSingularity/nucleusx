package com.example.nucleusx.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.nucleusx.DatabaseEntities.Node;

import java.util.Date;
import java.util.List;

/*
General rule: 1 DAO per Entity
 */

@Dao
public interface NodeDao {

    @Insert
    void insert(Node node);

    @Update
    void update(Node node);

    @Delete
    void delete(Node node);

    @Query("DELETE FROM node_table")
    void deleteAllNodes();

    @Query("DELETE FROM node_table where nodeId = :nodeId")
    void deleteNodeForNodeId(int nodeId);

    @Query("DELETE FROM node_table where registered = 0")
    void deleteUnregisteredNodes();

    @Query("SELECT * FROM node_table ORDER BY registered ASC, lastUpdated ASC")
    LiveData<List<Node>> getAllNodes();

    @Query("SELECT * FROM node_table WHERE nodeId = :nodeId")
    //@Query("SELECT * FROM node_table WHERE nodeId != :nodeId")
    List<Node> getDataForNodeId(int nodeId);

    @Query("UPDATE node_table SET ip = :ip, lastUpdated = :lastUpdated WHERE nodeId = :nodeId")
    void updateIpForNodeId(String ip, Date lastUpdated, int nodeId);

    @Query("SELECT ip FROM node_table WHERE nodeId = :nodeId")
    String getIpForNodeId(int nodeId);
}