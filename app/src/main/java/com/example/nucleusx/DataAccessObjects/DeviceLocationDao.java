package com.example.nucleusx.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.nucleusx.DatabaseEntities.DeviceLocation;

import java.util.List;

@Dao
public interface DeviceLocationDao {

    @Insert
    void insert(DeviceLocation deviceLocation);

    @Update
    void update(DeviceLocation deviceLocation);

    @Delete
    void delete(DeviceLocation deviceLocation);

    @Query("DELETE FROM device_location_table")
    void deleteAllDeviceLocations();

    @Query("SELECT * FROM device_location_table ORDER BY created DESC")
    LiveData<List<DeviceLocation>> getAllDeviceLocations();
}
