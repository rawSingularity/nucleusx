package com.example.nucleusx.DataAccessObjects;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.nucleusx.DatabaseEntities.Device;

import java.util.Date;
import java.util.List;

@Dao
public interface DeviceDao {

    @Insert
    void insert(Device device);

    @Update
    void update(Device device);

    @Delete
    void delete(Device device);

    @Query("DELETE FROM device_table")
    void deleteAllDevices();

    @Query("UPDATE device_table SET quickAccessMember = :quickAccess WHERE nodeId = :nodeId AND deviceId = :deviceId")
    void setDeviceQuickAccessPreference(int deviceId, int nodeId, Boolean quickAccess);

    @Query("UPDATE device_table SET location = :location WHERE nodeId = :nodeId AND deviceId = :deviceId")
    void setDeviceLocation(int deviceId, int nodeId, String location);

    @Query("UPDATE device_table SET location = 'Does not belong to any room' WHERE location = :location")
    void removeDevicesFromLocation(String location);

    @Query("DELETE FROM device_table WHERE nodeId = :nodeId")
    void removeDevicesForNode(int nodeId);

    @Query("UPDATE device_table SET location = :newLocation WHERE location = :oldLocation")
    void updateAllDeviceLocationIn(String oldLocation, String newLocation);

    @Query("UPDATE device_table " +
            "SET name = :name , " +
            "deviceConfig = :deviceConfig , " +
            "state = :state , " +
            "uts = :uts , " +
            "processedByRepositoryTimeStamp = :processedByRepositoryTimeStamp " +
            "WHERE id = :id AND deviceId = :deviceId AND nodeId = :nodeId")
    void updateDeviceRemoteInfo(String name, int deviceConfig, int state, long uts, Date processedByRepositoryTimeStamp, int id, int deviceId, int nodeId);

    @Query("SELECT * FROM device_table WHERE quickAccessMember")
    LiveData<List<Device>> getAllQuickAccessDevices();

    @Query("SELECT * FROM device_table WHERE nodeId = :nodeId AND deviceId = :deviceId")
    LiveData<List<Device>> getDeviceInfo(int nodeId, int deviceId);

    @Query("SELECT * FROM device_table ORDER BY nodeId")
    LiveData<List<Device>> getAllDevices();

    @Query(("SELECT * FROM device_table"))
    List<Device> getAllDevicesOnDemand();
}
