package com.example.nucleusx;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
/*
public class IpProbe extends AsyncTask<String, Integer, String> {

    static final String TAG = "ipProbe";

    public interface IpProbeResponse {
        void processIpProbeResponse(String result);
    }

    public IpProbeResponse delegate = null;
    public IpProbe(IpProbeResponse delegate){
        this.delegate = delegate;
    }


    @Override
    public void onPreExecute()
    {
    }

    @Override
    public String doInBackground(String... params) {

        String testIp = params[0];
        Log.d(TAG, "Probing: " + testIp);

        InetAddress address = null;
        boolean reachable = false;

        try {
            address = InetAddress.getByName(testIp);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            reachable = address.isReachable(500);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String hostName = address.getCanonicalHostName();

        String result = "";
        if (reachable) {
            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");
            //getMacFromArpCache(String.valueOf(testIp));
            result = testIp+" reachable\n";
        }
        else {
            Log.d(TAG, "Host: " + testIp + " is NOT reachable");
            //result = testIp+" unreachable\n";
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result)
    {
        super.onPostExecute(result);
        delegate.processIpProbeResponse(result);
    }

    @Override
    public void onProgressUpdate(Integer... params)
    {
        // show in spinner, access UI elements
    }
}
*/