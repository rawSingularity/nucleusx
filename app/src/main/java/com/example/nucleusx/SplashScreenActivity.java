package com.example.nucleusx;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EasySplashScreen config = new EasySplashScreen(SplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(5000)
                .withBackgroundColor(Color.parseColor("#009ab5"))
                .withHeaderText("NucleusX")
                .withFooterText("Owner: Pranjal Senjaliya\nDeveloper: Pratik Panchal")
                .withBeforeLogoText("Home Automation")
                .withAfterLogoText("Node Management Application")
                .withLogo(R.mipmap.ic_launcher_foreground);

        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);
        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);
        config.getHeaderTextView().setTextColor(Color.WHITE);

        View easySplashScreen  = config.create();

        //getSupportActionBar().hide();

        setContentView(easySplashScreen);
    }
}
