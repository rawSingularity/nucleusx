package com.example.nucleusx.Database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.nucleusx.DataAccessObjects.DeviceDao;
import com.example.nucleusx.DataAccessObjects.DeviceLocationDao;
import com.example.nucleusx.DataAccessObjects.NodeDao;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.DeviceLocation;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.TypeConverters.TimestampConverter;

import java.sql.Date;

/*
TODO: Add new entities here in @Database annotation
TODO: Create abstract methods for new DAOs corresponding to these entities
 */
@Database(entities = {Node.class, Device.class, DeviceLocation.class}, version = 1)
@TypeConverters({TimestampConverter.class})
public abstract class PrimaryDatabase extends RoomDatabase {

    private static PrimaryDatabase instance;

    public abstract NodeDao nodeDao();
    public abstract DeviceDao deviceDao();
    public abstract DeviceLocationDao deviceLocationDao();

    public static synchronized PrimaryDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(),
                    PrimaryDatabase.class,
                    "primary_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            //new PopulateNodeTableDbAsyncTask(instance).execute();
            //new PopulateDeviceTableDbAsyncTask(instance).execute();
            //new PopulateDeviceLocationTableDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateNodeTableDbAsyncTask extends AsyncTask<Void, Void, Void>{

        private NodeDao nodeDao;
        private PopulateNodeTableDbAsyncTask(PrimaryDatabase db){
            nodeDao = db.nodeDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // node_table
            nodeDao.insert(new Node(2358654, "192.168.1.55", true, "Home Group", new Date(System.currentTimeMillis()),"201901151836", new Date(System.currentTimeMillis())));
            nodeDao.insert(new Node(1548501, "192.168.1.57", true, "Home Group", new Date(System.currentTimeMillis()),"201901151836", new Date(System.currentTimeMillis())));
            nodeDao.insert(new Node(2930304, "192.168.1.60", true, "Home Group", new Date(System.currentTimeMillis()),"201901151836", new Date(System.currentTimeMillis())));
            nodeDao.insert(new Node(589023, "192.168.1.61", true, "Home Group", new Date(System.currentTimeMillis()),"201901151836", new Date(System.currentTimeMillis())));

            return null;
        }
    }

    private static class PopulateDeviceTableDbAsyncTask extends AsyncTask<Void, Void, Void>{

        private DeviceDao deviceDao;
        private PopulateDeviceTableDbAsyncTask(PrimaryDatabase db){
            deviceDao = db.deviceDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // device_table
            deviceDao.insert(new Device("Light 1",1, 1, 0, 2358654,0, "Living Location", true));
            deviceDao.insert(new Device("Light 1",2, 2, 0,1548501, 0, "Drawing Location", true));
            deviceDao.insert(new Device("Light 1",3, 3, 0,2358654, 0, "Bed Location", true));
            deviceDao.insert(new Device("Light 1",4, 4, 0, 1548501, 0, "Play Location", true));
            deviceDao.insert(new Device("Light 1",5, 1, 0, 2358654, 0,"Kitchen", true));
            deviceDao.insert(new Device("Light 1",6, 2, 0, 1548501, 0, "Patio", true));
            deviceDao.insert(new Device("Light 1",7, 3, 0, 2358654, 0, "Living Location", true));
            deviceDao.insert(new Device("Light 1",8, 4, 0, 1548501, 0, "Living Location", false));

            return null;
        }
    }

    private static class PopulateDeviceLocationTableDbAsyncTask extends AsyncTask<Void, Void, Void>{

        private DeviceLocationDao deviceLocationDao;
        private PopulateDeviceLocationTableDbAsyncTask(PrimaryDatabase db){
            deviceLocationDao = db.deviceLocationDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // device_table
            deviceLocationDao.insert(new DeviceLocation("Living Room"));
            deviceLocationDao.insert(new DeviceLocation("Kitchen"));
            deviceLocationDao.insert(new DeviceLocation("Bed Room"));
            deviceLocationDao.insert(new DeviceLocation("Porch"));

            return null;
        }
    }
}