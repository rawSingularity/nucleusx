package com.example.nucleusx.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class NodeAdapter extends ListAdapter<Node, NodeAdapter.NodeHolder> {

    private static final String TAG = "NodeAdapter";

    //private List<Node> mNodes = new ArrayList<Node>();
    private OnItemClickListener mListener;

    public NodeAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Node> DIFF_CALLBACK = new DiffUtil.ItemCallback<Node>() {
        @Override
        public boolean areItemsTheSame(@NonNull Node oldItem, @NonNull Node newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Node oldItem, @NonNull Node newItem) {
            return oldItem.getServiceName().equalsIgnoreCase(newItem.getServiceName()) &&
                   oldItem.getRegistered() == newItem.getRegistered() &&
                   oldItem.getNodeId() == newItem.getNodeId() &&
                   oldItem.getIp().equalsIgnoreCase(newItem.getIp()) &&
                   oldItem.getFirmwareBuildDate().equalsIgnoreCase(newItem.getFirmwareBuildDate()) &&
                   oldItem.getLastUpdated().getTime() == newItem.getLastUpdated().getTime() &&
                   oldItem.getRegisteredOn().getTime() == newItem.getRegisteredOn().getTime() &&
                   oldItem.getRegisteredTo().equalsIgnoreCase(newItem.getRegisteredTo());
        }
    };

    //For Click capture
    public interface OnItemClickListener {
        void onItemClick(Node node);
        void onLocateClick(Node node);
        void onConfigureClick(Node node);
        void onMoreInfoClick(Node node);
        void onRebootClick(Node node);
        //void onDeleteClick(int position);
        void onRemoveSettingsClick(View view, Node node);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public NodeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_node_item, viewGroup, false);
        NodeHolder nodeHolder = new NodeHolder(view, mListener);
        return nodeHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NodeHolder nodeHolder, int position) {
        //Node currentNode = mNodes.get(position);
        Node currentNode = getItem(position);

        // Header
        nodeHolder.mPrimaryHeader.setText("Node Id " + String.valueOf(currentNode.getNodeId()));

        //Registration & card color
        if(currentNode.getRegistered()){
            nodeHolder.mCardView.setCardBackgroundColor(Color.parseColor("#bbe8e3"));
            nodeHolder.mSecondaryHeader.setText("Network member");
            //Component visibility
            nodeHolder.mDeleteButton.setVisibility(View.VISIBLE);
            nodeHolder.mTertiaryHeader.setVisibility(View.VISIBLE);
            nodeHolder.mTimezoneHeader.setVisibility(View.VISIBLE);
            nodeHolder.mButtonContainer.setVisibility(View.VISIBLE);
            nodeHolder.mButtonTitleContainer.setVisibility(View.VISIBLE);
        }
        else{
            nodeHolder.mCardView.setCardBackgroundColor(Color.parseColor("#ffaaaa"));
            nodeHolder.mSecondaryHeader.setText("Not on network - Add now!");
            //Component visibility
            nodeHolder.mDeleteButton.setVisibility(View.GONE);
            nodeHolder.mTertiaryHeader.setVisibility(View.GONE);
            nodeHolder.mTimezoneHeader.setVisibility(View.GONE);
            nodeHolder.mButtonContainer.setVisibility(View.GONE);
            nodeHolder.mButtonTitleContainer.setVisibility(View.GONE);
        }

        // IP
        nodeHolder.mIpField.setText(currentNode.getIp());

        //Last updated - date
        StringBuilder sb = new StringBuilder();
        sb.append("Seen ");
        DateFormat dateFormat;
        if(System.currentTimeMillis() - currentNode.getLastUpdated().getTime() < 30000){
            sb.append("few seconds ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else if(System.currentTimeMillis() - currentNode.getLastUpdated().getTime() < 60000){
            sb.append("less than a minute ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else if(System.currentTimeMillis() - currentNode.getLastUpdated().getTime() < 5*60*1000){
            sb.append("few minutes ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else {
            dateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
        }
        sb.append(dateFormat.format(currentNode.getLastUpdated()));
        nodeHolder.mAdditionalInfoField.setText(sb.toString());
    }

    public Node getNodeAt(int position){
        return getItem(position);
    }

//    @Override
//    public int getItemCount() {
//        return mNodes.size();
//    }

//    public void setmNodes(List<Node> nodes){
//        //this.mNodes = nodes;
//        //notifyDataSetChanged();
//
//        for(Node node : nodes){
//            if(nodeExistsInScannedNodeItemList(node)){
//                int index = indexOfNodeWithNodeId(node.getNodeId());
//
//                //Update
//                //mNodes.get(index).setIp(node.getIp());
//                //mNodes.get(index).setLastUpdated(node.getLastUpdated());
//
//                mNodes.set(index, node);
//
//                Log.d(TAG, "UPDATE: nodeExist in mNodes: node:" + mNodes.get(index).getNodeId() +
//                        " ip:" + mNodes.get(index).getIp() +
//                        " registered:" + mNodes.get(index).getRegistered() +
//                        " last updated:" + mNodes.get(index).getLastUpdated());
//
//                //Notify adapter
//                notifyItemChanged(index);
//            }
//            else {
//                //Add
//                mNodes.add(node);
//                int index = mNodes.indexOf(node);
//
//                Log.d(TAG, "ADD: nodeExistsInScannedNodeItemList: node:"+ mNodes.get(index).getNodeId() +
//                        " ip:" + mNodes.get(index).getIp() +
//                        " registered:" + mNodes.get(index).getRegistered() +
//                        " last updated:" + mNodes.get(index).getLastUpdated());
//
//                notifyItemInserted(index);
//            }
//        }
//    }

//    private boolean nodeExistsInScannedNodeItemList(Node n){
//        for(Node node : mNodes){
//            if(node.getNodeId() == n.getNodeId()){
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private int indexOfNodeWithNodeId(int nodeId){
//        int i = -1;
//        for(Node node : mNodes){
//            if(node.getNodeId() == nodeId){
//                i = mNodes.indexOf(node);
//            }
//        }
//        return i;
//    }

    class NodeHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private LinearLayout mButtonContainer;
        private LinearLayout mButtonTitleContainer;

        private ImageView mConfigureImage;
        private ImageView mLocateImage;
        private ImageView mMoreInfoImage;
        private ImageView mRebootImage;
        private Button mDeleteButton;

        private TextView mPrimaryHeader;
        private ImageView mStatusIcon;
        private TextView mSecondaryHeader;
        private TextView mAdditionalInfoField;
        private TextView mIpField;
        private TextView mTertiaryHeader;
        private TextView mTimezoneHeader;

        public NodeHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.sniNodeCard);
            mButtonContainer = itemView.findViewById(R.id.sniButtonContainer);
            mButtonTitleContainer = itemView.findViewById(R.id.sniButtonTitleContainer);

            mLocateImage = itemView.findViewById(R.id.sniLocateImage);
            mConfigureImage = itemView.findViewById(R.id.sniConfigureImage);
            mMoreInfoImage = itemView.findViewById(R.id.sniMoreInfoImage);
            mRebootImage = itemView.findViewById(R.id.sniPowerCycleImage);
            mDeleteButton = itemView.findViewById(R.id.sniDeleteNode);
            mPrimaryHeader = itemView.findViewById(R.id.sniPrimaryHeader);
            mStatusIcon = itemView.findViewById(R.id.sniStatusIcon);
            mSecondaryHeader = itemView.findViewById(R.id.sniSecondaryHeader);
            mAdditionalInfoField = itemView.findViewById(R.id.sniAdditionalInfoField);
            mIpField = itemView.findViewById(R.id.sniIpField);
            mTertiaryHeader = itemView.findViewById(R.id.sniTertiaryHeader);
            mTimezoneHeader = itemView.findViewById(R.id.sniTimezoneHeader);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(getItem(position));
                        }
                    }
                }
            });

            mLocateImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onLocateClick(getItem(position));
                        }
                    }
                }
            });

            mConfigureImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onConfigureClick(getItem(position));
                        }
                    }
                }
            });

            mMoreInfoImage.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onMoreInfoClick(getItem(position));
                        }
                    }
                }
            });

            mRebootImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onRebootClick(getItem(position));
                        }
                    }
                }
            });

            mDeleteButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            //listener.onDeleteClick(position);
                            listener.onRemoveSettingsClick(mDeleteButton, getItem(position));
                        }
                    }
                }
            });
        }
    }

}
