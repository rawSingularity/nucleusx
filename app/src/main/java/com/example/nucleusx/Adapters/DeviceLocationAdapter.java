package com.example.nucleusx.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.DeviceLocation;
import com.example.nucleusx.R;
import com.example.nucleusx.Utilities.HardwareDescriptors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DeviceLocationAdapter extends ListAdapter<DeviceLocation, DeviceLocationAdapter.DeviceLocationHolder> {

    //private List<DeviceLocation> deviceLocationList = new ArrayList<DeviceLocation>();
    private OnItemClickListener mListener;
    private HardwareDescriptors hd;

    public DeviceLocationAdapter(Activity activity) {
        super(DIFF_CALLBACK);
        hd = new HardwareDescriptors(activity);
    }

    private static final DiffUtil.ItemCallback<DeviceLocation> DIFF_CALLBACK = new DiffUtil.ItemCallback<DeviceLocation>() {
        @Override
        public boolean areItemsTheSame(@NonNull DeviceLocation oldItem, @NonNull DeviceLocation newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull DeviceLocation oldItem, @NonNull DeviceLocation newItem) {

            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getCreated().getTime() == newItem.getCreated().getTime();
        }
    };

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onDeleteClick(int position);
        void onNameClick(int position, String name);
        void onNameEdit();
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    class DeviceLocationHolder extends RecyclerView.ViewHolder{
        private CardView mCardView;
        private EditText mEditTextTitle;
        private TextView mTextViewInfo;
        private TextView mTextViewDevices;
        private Button mDeleteButton;

        public DeviceLocationHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.crDeviceLocationCard);
            mEditTextTitle = itemView.findViewById(R.id.crRoomName);
            mTextViewDevices = itemView.findViewById(R.id.crNumberOfDevices);
            mTextViewInfo = itemView.findViewById(R.id.crLastUpdated);
            mDeleteButton = itemView.findViewById(R.id.crDeleteRoomButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mEditTextTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            //Hide cursor
                            mEditTextTitle.setFocusable(false);

                            listener.onNameClick(position, mEditTextTitle.getText().toString());
                        }
                    }
                }
            });

            mEditTextTitle.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mEditTextTitle.setFocusableInTouchMode(true);
                    //mEditTextTitle.requestFocus();
                    listener.onNameEdit();
                    return false;
                }
            });

            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

        }
    }

    @NonNull
    @Override
    public DeviceLocationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_room, viewGroup, false);
        return new DeviceLocationHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceLocationHolder deviceLocationHolder, int position) {
        DeviceLocation currentDeviceLocation = getItem(position);

        deviceLocationHolder.mEditTextTitle.setText(currentDeviceLocation.getName());
        deviceLocationHolder.mTextViewDevices.setText(getDeviceCountStringForRoom(currentDeviceLocation.getName()));

        //Card Background
        int deviceCount = hd.getNumberOfDevicesInRoom(currentDeviceLocation.getName());
        if(deviceCount == 0){
            deviceLocationHolder.mCardView.setBackgroundColor(Color.parseColor("#efbd9b"));
        }
        else{
            deviceLocationHolder.mCardView.setBackgroundColor(Color.parseColor("#a6ef9b"));
        }

        DateFormat dateFormat = new SimpleDateFormat("hh:mm, EEE, MMM dd, yyyy");
        deviceLocationHolder.mTextViewInfo.setText("Last Updated " + dateFormat.format(currentDeviceLocation.getCreated()));
    }


    private String getDeviceCountStringForRoom(String roomName){
        int i = hd.getNumberOfDevicesInRoom(roomName.trim());

        StringBuilder sb = new StringBuilder();

        if(i==0){
            sb.append("No Devices");
        }
        else if (i==1){
            sb.append("1 Device");
        }
        else {
            sb.append(i);
            sb.append(" Devices");
        }

        return sb.toString();
    }

    /*@Override
    public int getItemCount() {
        return deviceLocationList.size();
    }

    public void setDeviceLocationList(List<DeviceLocation> deviceLocationList){
        this.deviceLocationList = deviceLocationList;
        notifyDataSetChanged();
    }*/

    public DeviceLocation getDeviceLocationAtPosition(int position) {
        return getItem(position);
    }
}
