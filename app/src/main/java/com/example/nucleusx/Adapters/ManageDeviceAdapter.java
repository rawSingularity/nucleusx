package com.example.nucleusx.Adapters;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.R;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;

import java.util.ArrayList;
import java.util.List;

public class ManageDeviceAdapter extends ListAdapter<Device, ManageDeviceAdapter.ControlItemViewHolder> {

    static final String TAG = "ManageDeviceAdapter";

    //private List<Device> mDeviceList = new ArrayList<Device>();
    //
    private OnItemClickListener mItemListener;

    private static SparseBooleanArray mSwitchStateArray = new SparseBooleanArray();
    private static SparseArray mSeekbarStateArray = new SparseArray();

    private HardwareDescriptors hd = new HardwareDescriptors();

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onSettingsClick(int position);
        void onSwitchChange(int position, boolean isChecked);
        void onSeekbarChange(int position, int progress, boolean fromUser);
        void onManageEventsClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemListener = listener;
    }

    public class ControlItemViewHolder extends RecyclerView.ViewHolder {

        private Button mSettings;
        private TextView mDeviceName;
        private ImageView mStatusIcon;
        private TextView mDeviceConfiguration;
        private Switch mSwitch;
        private SeekBar mSeekBar;
        private TextView mSeekbarValue;
        private TextView mEvents;
        private Button mManageEventsButton;

        public ControlItemViewHolder(@NonNull View itemView, final OnItemClickListener itemClickListener) {
            super(itemView);
            mSettings = itemView.findViewById(R.id.ceSettings);
            mDeviceName = itemView.findViewById(R.id.ceDeviceName);
            mStatusIcon = itemView.findViewById(R.id.ceStatusIcon);
            mDeviceConfiguration = itemView.findViewById(R.id.ceDeviceConfiguration);
            mSwitch = itemView.findViewById(R.id.ceSwitch);
            mSeekbarValue = itemView.findViewById(R.id.ceSeekbarValue);
            mSeekBar = itemView.findViewById(R.id.ceSeekBar);
            mEvents = itemView.findViewById(R.id.ceEvents);
            mManageEventsButton = itemView.findViewById(R.id.ceManageEvents);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            itemClickListener.onItemClick(position);
                            mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        }
                    }
                }
            });

            mSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            itemClickListener.onSettingsClick(position);
                        }
                    }
                }
            });

            mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (itemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {

                            Log.d(TAG, "onCheckedChanged: position:" + position + " state:" + isChecked);
                            Log.d(TAG, "Checking for position: " + position + " (indexOfKey=" + mSwitchStateArray.indexOfKey(position) + ")");

                            Device device = getItem(position);
                            int key = hd.getCombinedNodeDeviceId(device.getNodeId(), device.getDeviceId());

                            if(mSwitchStateArray.indexOfKey(key) < 0){
                                //This key does not exist in the array yet
                                mSwitchStateArray.put(key, isChecked);
                            }

                            if(mSwitchStateArray.get(key) == isChecked){
                                // the switch state is same as last recorded - so this might be a phantom trigger.
                            }
                            else {
                                itemClickListener.onSwitchChange(position, isChecked);
                                mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                            }
                        }
                    }
                }
            });

            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (itemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {

                            Device device = getItem(position);
                            int key = hd.getCombinedNodeDeviceId(device.getNodeId(), device.getDeviceId());
                            if(mSeekbarStateArray.indexOfKey(key)<0){
                                //This key does not exist in the array yet
                                Log.d(TAG, "key " + key + " does not exist in sparseARray");
                                mSeekbarStateArray.put(key, device.getState());
                            }

                            //Send request only if the difference is more than preset
                            int difference = Math.abs(progress - (int)mSeekbarStateArray.get(key));
                            Log.d(TAG, "for key: " + key + "  state: " + device.getState() + " progress: " + mSeekbarStateArray.get(key) + " difference:" + difference);
                            if(difference > 75){
                                Log.d(TAG, "difference is more than 10");
                                itemClickListener.onSeekbarChange(position, progress, fromUser);

                                //The response will set it to the correct value - but to avoid flicker on the UI element, set it manually here as well.
                                mSeekbarStateArray.put(key, progress);
                            }

                            //But update UI elements immediately
                            mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                            mSeekbarValue.setText(hd.getStatePercentValue(progress)+"%");
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int position = getAdapterPosition();
                    if (itemClickListener != null && position != RecyclerView.NO_POSITION) {

                        itemClickListener.onSeekbarChange(position, seekBar.getProgress(), true);

                        //update UI elements immediately
                        mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        mSeekbarValue.setText(hd.getStatePercentValue(seekBar.getProgress())+"%");
                    }
                }
            });

            mManageEventsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            itemClickListener.onManageEventsClick(position);
                        }
                    }
                }
            });

        }
    }

    /*
    public ManageDeviceAdapter(List<Device> controlItemList) {

        mDeviceList = controlItemList;
    }

    public ManageDeviceAdapter() {

    }
    */

    public ManageDeviceAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Device> DIFF_CALLBACK = new DiffUtil.ItemCallback<Device>() {
        @Override
        public boolean areItemsTheSame(@NonNull Device oldDevice, @NonNull Device newDevice) {
            return oldDevice.getId() == newDevice.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Device oldDevice, @NonNull Device newDevice) {
            return  oldDevice.getNodeId() == newDevice.getNodeId() &&
                    oldDevice.getDeviceId() == newDevice.getDeviceId() &&
                    oldDevice.getDeviceConfig() == newDevice.getDeviceConfig() &&
                    oldDevice.getQuickAccessMember() == newDevice.getQuickAccessMember() &&
                    oldDevice.getLocation().trim().equalsIgnoreCase(newDevice.getLocation().trim()) &&
                    oldDevice.getState() == newDevice.getState() &&
                    oldDevice.getUts() == newDevice.getUts() &&
                  //REVALUATE - WHY DO WE NEED THIS?
                  //oldDevice.getProcessedByRepositoryTimeStamp() == newDevice.getProcessedByRepositoryTimeStamp() &&
                    (System.currentTimeMillis()-newDevice.getProcessedByRepositoryTimeStamp().getTime() < 15000) &&
                    oldDevice.getName().trim().equalsIgnoreCase(newDevice.getName().trim());
        }
    };

    @NonNull
    @Override
    public ControlItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_control_item, viewGroup, false);
        ControlItemViewHolder cvh = new ControlItemViewHolder(v, mItemListener);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ControlItemViewHolder controlItemViewHolder, int position) {

        Device currentDevice = getItem(position);// mDeviceList.get(position);
        StringBuilder sb = new StringBuilder("Configuration: ");
        int statusIcon;

        Log.d(TAG, "onBindViewHolder: position: " + position + " uts: " + currentDevice.getUts());

        //Device Name
        controlItemViewHolder.mDeviceName.setText(hd.sanitzeDeviceNameForDevice(currentDevice.getName().trim(), currentDevice.getDeviceId()));

        //Iconography
        if (currentDevice.getUts() == 0) {
            statusIcon = R.drawable.ic_in_progress_orange_24dp;
        } else if (hd.getCurrentUts() - currentDevice.getUts() > 15) {
            statusIcon = R.drawable.ic_check_circle_green_005822_24dp;
        } else {
            statusIcon = R.drawable.ic_check_circle_green_008221_24dp;
        }

        //SparseArray swtich state
        int sparseKey = hd.getCombinedNodeDeviceId  (currentDevice.getNodeId(), currentDevice.getDeviceId());

        // Components Visibility & State Configuration
        if (hd.isDigitalOutput(currentDevice.getDeviceConfig())) {
            // Digital Switch

            mSwitchStateArray.put(sparseKey, hd.getBooleanFromDigitalState(currentDevice.getState()));

            // State
            //1. Disable the listener to avoid phantom-changes
            //quickAccessDeviceHolder.mSwitch.setOnCheckedChangeListener(null);
            //2. Change the switch state
            Log.d(TAG, "received current state: " + currentDevice.getState() + " name:" + hd.sanitzeDeviceNameForDevice(currentDevice.getName(), currentDevice.getDeviceId()));
            controlItemViewHolder.mSwitch.setChecked(
                    hd.getBooleanFromDigitalState(currentDevice.getState())
            );
            //3. Reenable it back
            //quickAccessDeviceHolder.mSwitch.setOnCheckedChangeListener(switchChangeListener);

            //Visibility
            controlItemViewHolder.mSwitch.setVisibility(View.VISIBLE);
            controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
            controlItemViewHolder.mSeekbarValue.setVisibility(View.GONE);
        } else if (hd.isPwmOutput(currentDevice.getDeviceConfig())) {
            //PWM output

            mSeekbarStateArray.put(sparseKey, currentDevice.getState());

            //State
            controlItemViewHolder.mSeekBar.setProgress(
                    currentDevice.getState()
            );
            controlItemViewHolder.mSeekbarValue.setText(hd.getStatePercentValue(currentDevice.getState()) + "%");
            //Visibility
            controlItemViewHolder.mSwitch.setVisibility(View.GONE);
            controlItemViewHolder.mSeekBar.setVisibility(View.VISIBLE);
            controlItemViewHolder.mSeekbarValue.setVisibility(View.VISIBLE);
        } else {
            controlItemViewHolder.mSwitch.setVisibility(View.GONE);
            controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
            controlItemViewHolder.mSeekbarValue.setVisibility(View.GONE);
        }

        //Upcoming Event - TODO
        //controlItemViewHolder.mEvents.setText(getUpcomingEventString(currentDevice.getDeviceInfo().getCalendarEvents()));


        // Configuration
        if (hd.isDigitalOutput(currentDevice.getDeviceConfig())) {
            Log.d(TAG, "Device: " + currentDevice.getDeviceId() + " is digital output");
            sb.append("Output, Digital\nActive ");

            if (isActiveHigh(currentDevice.getDeviceConfig())) {
                sb.append("High");
            } else {
                sb.append("Low");
            }

            controlItemViewHolder.mSwitch.setChecked(hd.getBooleanFromDigitalState(currentDevice.getState()));
            controlItemViewHolder.mSwitch.setVisibility(View.VISIBLE);
            controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
        } else if (hd.isPwmOutput(currentDevice.getDeviceConfig())) {
            Log.d(TAG, "Device: " + currentDevice.getDeviceId() + " is PWM output");
            sb.append("Output, PWM/Analogue");

            //TODO
            //sb.append("\nGradient: " + currentDevice.getDeviceInfo().getG());

            controlItemViewHolder.mSwitch.setVisibility(View.GONE);
            controlItemViewHolder.mSeekBar.setProgress(currentDevice.getState());
            controlItemViewHolder.mSeekBar.setVisibility(View.VISIBLE);
        } else if (!hd.isDigitalOutput(currentDevice.getDeviceConfig()) && !hd.isPwmOutput(currentDevice.getDeviceConfig())) {
            Log.d(TAG, "Device: " + currentDevice.getDeviceId() + " is input");
            sb.append("Input\nInterrupt ");
            if ((currentDevice.getDeviceConfig() & 4) == 4) {
                sb.append("on level L");
            } else if ((currentDevice.getDeviceConfig() & 8) == 8) {
                sb.append("on level H");
            } else if ((currentDevice.getDeviceConfig() & 12) == 12) {
                sb.append("on change");
            } else if ((currentDevice.getDeviceConfig() & 16) == 16) {
                sb.append("on L->H");
            } else if ((currentDevice.getDeviceConfig() & 20) == 20) {
                sb.append("on H->L");
            } else {
                sb.append("not configured");
            }
            controlItemViewHolder.mSwitch.setVisibility(View.GONE);
            controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
        }

        // Enable the settings & manageEvents button
        controlItemViewHolder.mManageEventsButton.setVisibility(View.VISIBLE);
        controlItemViewHolder.mSettings.setVisibility(View.VISIBLE);

        sb.append(" (c=" + currentDevice.getDeviceConfig() + ")");

        // Parent Node
        sb.append("\nParent Node: " + currentDevice.getNodeId());

        // Configuration String
        controlItemViewHolder.mDeviceConfiguration.setText(sb.toString());

        //Status Icon
        controlItemViewHolder.mStatusIcon.setImageResource(statusIcon);
    }

    private boolean isActiveHigh(int c) {
        if ((c & 4) == 4) {
            return true;
        }
        return false;
    }

    /*@Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    public void setDeviceList(List<Device> deviceList){
        mDeviceList = deviceList;
    }*/

    public Device getDeviceAtPosition(int position) {
        return getItem(position);
    }

    private String getUpcomingEventString(List<CalendarEvent> calendarEvents) {
        String upcomingEventString = "No upcoming events";

        if (calendarEvents == null || calendarEvents.size() == 0) {
            return upcomingEventString;
        }
        upcomingEventString = "Upcoming: S:" + calendarEvents.get(0).getS() + " @" + calendarEvents.get(0).getTime().toString().substring(0, calendarEvents.get(0).getTime().toString().length() - 6);

        return upcomingEventString;
    }
}