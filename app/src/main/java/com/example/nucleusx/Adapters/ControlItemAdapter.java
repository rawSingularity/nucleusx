package com.example.nucleusx.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.nucleusx.ControlItem;
import com.example.nucleusx.R;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;

import java.util.ArrayList;
import java.util.List;

public class ControlItemAdapter extends RecyclerView.Adapter<ControlItemAdapter.ControlItemViewHolder> {

    static final String TAG = "ControlItemAdapter";

    private ArrayList<ControlItem> mControlItemList;
    //
    private OnItemClickListener mItemListener;

    private static SparseBooleanArray mSwitchStateArray = new SparseBooleanArray();

    private HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onSettingsClick(int position);
        void onSwitchChange(int position, boolean isChecked);
        void onSeekbarChange(int position, int progress, boolean fromUser);
        void onManageEventsClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemListener = listener;
    }

    public static class ControlItemViewHolder extends RecyclerView.ViewHolder{

        private Button mSettings;
        private TextView mDeviceName;
        private ImageView mStatusIcon;
        private TextView mDeviceConfiguration;
        private Switch mSwitch;
        private SeekBar mSeekBar;
        private TextView mSeekbarValue;
        private TextView mEvents;
        private Button mManageEventsButton;

        public ControlItemViewHolder(@NonNull View itemView, final OnItemClickListener itemClickListener) {
            super(itemView);
            mSettings = itemView.findViewById(R.id.ceSettings);
            mDeviceName = itemView.findViewById(R.id.ceDeviceName);
            mStatusIcon = itemView.findViewById(R.id.ceStatusIcon);
            mDeviceConfiguration = itemView.findViewById(R.id.ceDeviceConfiguration);
            mSwitch = itemView.findViewById(R.id.ceSwitch);
            mSeekBar = itemView.findViewById(R.id.ceSeekBar);
            mSeekbarValue = itemView.findViewById(R.id.ceSeekbarValue);
            mEvents = itemView.findViewById(R.id.ceEvents);
            mManageEventsButton = itemView.findViewById(R.id.ceManageEvents);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            itemClickListener.onItemClick(position);
                            mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        }
                    }
                }
            });

            mSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            itemClickListener.onSettingsClick(position);
                        }
                    }
                }
            });

            mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(itemClickListener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {

                            Log.d(TAG, "onCheckedChanged: position:" + position + " state:" + isChecked);

                            Log.d(TAG, "Checking for position: "+position+" (indexOfKey=" + mSwitchStateArray.indexOfKey(position)+")");
                            if(mSwitchStateArray.indexOfKey(position) < 0){
                                //The index doesn't exist. Add it and initialize to same state
                                Log.d(TAG, "Key/Value doesn not exist for position: "+position+" (index=" + mSwitchStateArray.indexOfKey(position)+")");
                                mSwitchStateArray.put(position, isChecked);
                            }

                            Log.d(TAG, "READING FROM SPARSE ARRAY: index->" + position + " state:" + mSwitchStateArray.get(position));

                            if(mSwitchStateArray.get(position) == isChecked){
                                // The state of switch from UI is same as last recorded in sparse array
                                // Hence, this was a phantom-trigger
                                // Maybe we can use this to refetch the data of this card??? just a thought
                                Log.d(TAG, "mSwitchState and real switch state - BOTH ARE SAME: " + isChecked);
                            }
                            else {
                                // The state of switch from UI is different from what was last recorded in sparse array
                                Log.d(TAG, "mSwitchState and real switch state - ARE DIFFERENT - invoking onSwitchChange");
                                itemClickListener.onSwitchChange(position, isChecked);
                                // Update iconography
                                mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                            }
                        }
                    }
                }
            });

            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(itemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            itemClickListener.onSeekbarChange(position, progress, fromUser);
                            // try updating icon here
                            mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    Log.d(TAG, "onStartTrackingTouch");
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    Log.d(TAG, "onStopTrackingTouch");
                }
            });

            mManageEventsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            itemClickListener.onManageEventsClick(position);
                        }
                    }
                }
            });

        }
    }

    public ControlItemAdapter(ArrayList<ControlItem> controlItemList) {
        mControlItemList = controlItemList;
    }

    public ControlItemAdapter(){

    }

    @NonNull
    @Override
    public ControlItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_control_item, viewGroup, false);
        ControlItemViewHolder cvh = new ControlItemViewHolder(v, mItemListener);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ControlItemViewHolder controlItemViewHolder, int i) {

        ControlItem currentItem = mControlItemList.get(i);
        StringBuilder sb = new StringBuilder("Configuration: ");
        int statusIcon;

        controlItemViewHolder.mDeviceName.setText(currentItem.getDeviceName());

        if(currentItem.isThisCardInitialized()){
            Log.d(TAG, "PUTTING / UPDATING INTO SPARSE ARRAY: index->" + i + " state:" + hardwareDescriptors.getBooleanFromDigitalState(currentItem.getDeviceInfo().getS()));
            mSwitchStateArray.put(i, hardwareDescriptors.getBooleanFromDigitalState(currentItem.getDeviceInfo().getS()));
        }

        if(currentItem.getDeviceInfo() != null) {
            Log.d(TAG, "onBindViewHolder: Device:" + currentItem.getDeviceInfo().getD() + " (index:" + i + ")");

            //Status Icon
            statusIcon = R.drawable.ic_check_circle_green_24dp;

            //Upcoming Event
            controlItemViewHolder.mEvents.setText(getUpcomingEventString(currentItem.getDeviceInfo().getCalendarEvents()));

            // Configuration
            if (hardwareDescriptors.isDigitalOutput(currentItem.getDeviceInfo().getC())) {
                Log.d(TAG, "Device: " + currentItem.getDeviceInfo().getD() + " is digital output");
                sb.append("Output, Digital\nActive ");

                if(isActiveHigh(currentItem.getDeviceInfo().getC())){
                    sb.append("High");
                }
                else {
                    sb.append("Low");
                }

                controlItemViewHolder.mSwitch.setChecked(hardwareDescriptors.getBooleanFromDigitalState(currentItem.getDeviceInfo().getS()));
                controlItemViewHolder.mSwitch.setVisibility(View.VISIBLE);
                controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
                controlItemViewHolder.mSeekbarValue.setVisibility(View.GONE);
            }
            else if (hardwareDescriptors.isPwmOutput(currentItem.getDeviceInfo().getC())) {
                Log.d(TAG, "Device: " + currentItem.getDeviceInfo().getD() + " is PWM output");
                sb.append("Output, PWM/Analogue");
                sb.append("\nGradient: " + currentItem.getDeviceInfo().getG());
                controlItemViewHolder.mSwitch.setVisibility(View.GONE);
                //
                controlItemViewHolder.mSeekBar.setProgress(currentItem.getDeviceInfo().getS());
                controlItemViewHolder.mSeekBar.setVisibility(View.VISIBLE);
                //
                controlItemViewHolder.mSeekbarValue.setText(hardwareDescriptors.getStatePercentValue(currentItem.getDeviceInfo().getS())+"%");
                controlItemViewHolder.mSeekbarValue.setVisibility(View.VISIBLE);
            }
            else if (!hardwareDescriptors.isDigitalOutput(currentItem.getDeviceInfo().getC()) && !hardwareDescriptors.isPwmOutput(currentItem.getDeviceInfo().getC())) {
                Log.d(TAG, "Device: " + currentItem.getDeviceInfo().getD() + " is input");
                sb.append("Input\nInterrupt ");
                if((currentItem.getDeviceInfo().getC() & 4) == 4){
                    sb.append("on level L");
                }
                else if((currentItem.getDeviceInfo().getC() & 8) == 8){
                    sb.append("on level H");
                }
                else if((currentItem.getDeviceInfo().getC() & 12) == 12){
                    sb.append("on change");
                }
                else if((currentItem.getDeviceInfo().getC() & 16) == 16){
                    sb.append("on L->H");
                }
                else if((currentItem.getDeviceInfo().getC() & 20) == 20){
                    sb.append("on H->L");
                }
                else {
                    sb.append("not configured");
                }
                controlItemViewHolder.mSwitch.setVisibility(View.GONE);
                controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
                controlItemViewHolder.mSeekbarValue.setVisibility(View.GONE);
            }
            // Enable the settings & manageEvents button
            controlItemViewHolder.mManageEventsButton.setVisibility(View.VISIBLE);
            controlItemViewHolder.mSettings.setVisibility(View.VISIBLE);
        }
        else {
            statusIcon = R.drawable.ic_in_progress_orange_24dp;

            controlItemViewHolder.mSwitch.setVisibility(View.GONE);
            controlItemViewHolder.mSeekBar.setVisibility(View.GONE);
            controlItemViewHolder.mSeekbarValue.setVisibility(View.GONE);

            controlItemViewHolder.mManageEventsButton.setVisibility(View.GONE);
            controlItemViewHolder.mSettings.setVisibility(View.GONE);
        }
        sb.append(" (c=" +currentItem.getDeviceInfo().getC()+ ")");

        // Configuration String
        controlItemViewHolder.mDeviceConfiguration.setText(sb.toString());

        //Status Icon
        controlItemViewHolder.mStatusIcon.setImageResource(statusIcon);
    }

    private boolean isActiveHigh(int c) {
        if((c & 4) == 4){
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return mControlItemList.size();
    }

    private String getUpcomingEventString(List< CalendarEvent > calendarEvents) {
        String upcomingEventString = "No upcoming events";

        if(calendarEvents == null || calendarEvents.size() == 0){
            return upcomingEventString;
        }
        upcomingEventString = "Upcoming: S:" + calendarEvents.get(0).getS() + " @" + calendarEvents.get(0).getTime().toString().substring(0,calendarEvents.get(0).getTime().toString().length()-6);

        return upcomingEventString;
    }
}
