package com.example.nucleusx.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.nucleusx.R;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class CalendarEventAdapter extends RecyclerView.Adapter<CalendarEventAdapter.CalendarEventViewHolder> {

    public static final String TAG = "CalendarEventAdapter";
    private ArrayList<CalendarEvent> mCalendarEventsList;
    private HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onDeleteClick(int position);

        void onRepeatSunClick(int position);

        void onRepeatMonClick(int position);

        void onRepeatTueClick(int position);

        void onRepeatWedClick(int position);

        void onRepeatThuClick(int position);

        void onRepeatFriClick(int position);

        void onRepeatSatClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class CalendarEventViewHolder extends RecyclerView.ViewHolder {
        private Button mDeleteButton;
        private TextView mEventHeaderTextView;
        private ImageView mCardStatusIconImageView;
        private TextView mEventSupportingInfoTextView;

        private TextView mEventStateTextView;
        private TextView mEventTimeTextView;
        private TextView mEventDateTextView;

        private TextView mEventRepeatHeaderTextView;
        private Button mRepeatSunButton;
        private Button mRepeatMonButton;
        private Button mRepeatTueButton;
        private Button mRepeatWedButton;
        private Button mRepeatThuButton;
        private Button mRepeatFriButton;
        private Button mRepeatSatButton;
        private TextView mRepeatSunTextView;
        private TextView mRepeatMonTextView;
        private TextView mRepeatTueTextView;
        private TextView mRepeatWedTextView;
        private TextView mRepeatThuTextView;
        private TextView mRepeatFriTextView;
        private TextView mRepeatSatTextView;

        private LinearLayout mRepeatSelectionLayoutContainer; //deRepeatSelection
        private LinearLayout mRepeatDaysLabelLayoutContainer; //schRepeatDays


        public CalendarEventViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            mDeleteButton = itemView.findViewById(R.id.deDeleteEvent);
            mEventHeaderTextView = itemView.findViewById(R.id.deEventHeader);
            mCardStatusIconImageView = itemView.findViewById(R.id.deStatusIcon);
            mEventSupportingInfoTextView = itemView.findViewById(R.id.deSupportingInfo);

            mEventStateTextView = itemView.findViewById(R.id.deUpcomingState);
            mEventTimeTextView = itemView.findViewById(R.id.deTime);
            mEventDateTextView = itemView.findViewById(R.id.deDate);

            mEventRepeatHeaderTextView = itemView.findViewById(R.id.deRepeat);
            mRepeatSunButton = itemView.findViewById(R.id.deRepeatSunButton);
            mRepeatMonButton = itemView.findViewById(R.id.deRepeatMonButton);
            mRepeatTueButton = itemView.findViewById(R.id.deRepeatTueButton);
            mRepeatWedButton = itemView.findViewById(R.id.deRepeatWedButton);
            mRepeatThuButton = itemView.findViewById(R.id.deRepeatThuButton);
            mRepeatFriButton = itemView.findViewById(R.id.deRepeatFriButton);
            mRepeatSatButton = itemView.findViewById(R.id.deRepeatSatButton);
            mRepeatSunTextView = itemView.findViewById(R.id.deRepeatSunTitle);
            mRepeatMonTextView = itemView.findViewById(R.id.deRepeatMonTitle);
            mRepeatTueTextView = itemView.findViewById(R.id.deRepeatTueTitle);
            mRepeatWedTextView = itemView.findViewById(R.id.deRepeatWedTitle);
            mRepeatThuTextView = itemView.findViewById(R.id.deRepeatThuTitle);
            mRepeatFriTextView = itemView.findViewById(R.id.deRepeatFriTitle);
            mRepeatSatTextView = itemView.findViewById(R.id.deRepeatSatTitle);

            mRepeatSelectionLayoutContainer = itemView.findViewById(R.id.deRepeatSelection);
            mRepeatDaysLabelLayoutContainer = itemView.findViewById(R.id.schRepeatDays);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

            mRepeatSunButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatSunButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatSunClick(position);
                        }
                    }
                }
            });

            mRepeatMonButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatMonButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatMonClick(position);
                        }
                    }
                }
            });

            mRepeatTueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatTueButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatTueClick(position);
                        }
                    }
                }
            });

            mRepeatWedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatWedButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatWedClick(position);
                        }
                    }
                }
            });

            mRepeatThuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatThuButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatThuClick(position);
                        }
                    }
                }
            });

            mRepeatFriButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatFriButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatFriClick(position);
                        }
                    }
                }
            });

            mRepeatSatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mRepeatSatButton.setBackgroundResource(R.drawable.ic_in_progress_orange_24dp);
                            listener.onRepeatSatClick(position);
                        }
                    }
                }
            });
        }
    }

    public CalendarEventAdapter(ArrayList<CalendarEvent> calendarEventsList) {
        mCalendarEventsList = calendarEventsList;
    }

    @NonNull
    @Override
    public CalendarEventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_device_event, viewGroup, false);
        CalendarEventViewHolder cevh = new CalendarEventViewHolder(v, mListener);
        return cevh;
    }

    @Override
    public void onBindViewHolder(@NonNull CalendarEventViewHolder calendarEventViewHolder, int position) {
        CalendarEvent currentEvent = mCalendarEventsList.get(position);

        Log.d(TAG, "currentEvent: " + currentEvent.toString());

        //Header & supporting information
        setHeaderAndSupportingInfoText(calendarEventViewHolder, getNextTriggerDateInMs(currentEvent));

        //State
        setAndFormatStateText(calendarEventViewHolder.mEventStateTextView, currentEvent);

        //Time
        calendarEventViewHolder.mEventTimeTextView.setText(setTimeText(currentEvent));

        //Date
        calendarEventViewHolder.mEventDateTextView.setText(setDateText(getNextTriggerDateInMs(currentEvent)));

        //Repeat title
        calendarEventViewHolder.mEventRepeatHeaderTextView.setText(setRepeatHeaderText(currentEvent.getR()));

        //Repeat components
        setRepeatComponents(calendarEventViewHolder, currentEvent.getR());

    }

    @Override
    public int getItemCount() {
        return mCalendarEventsList.size();
    }

    /* SUPPORTING DATA-MANIPULATION METHODS */

    private  boolean itIsOnWeekend(Calendar calendar){
        if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
           calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            return true;
        }
        return false;
    }

    private void setHeaderAndSupportingInfoText(CalendarEventViewHolder vh, long candidateMs) {
        StringBuilder headerSb = new StringBuilder();
        StringBuilder supInfoSb = new StringBuilder();

        Calendar candidate = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        candidate.setTimeInMillis(candidateMs);

        Calendar reference = Calendar.getInstance();
        reference.setTimeInMillis(System.currentTimeMillis() + reference.get(Calendar.ZONE_OFFSET));
        Log.d(TAG, "candidate:" + candidate.getTimeInMillis() + " reference:" + reference.getTimeInMillis());

        long difference = candidate.getTimeInMillis() - reference.getTimeInMillis();
        Log.d(TAG, "difference: reference - candidate: " + difference);

        if(difference < 0){
            Log.d(TAG, "Past event");
            //Event is in past - weird
            headerSb.append("Past Event");
            supInfoSb.append("This event has passed");
        }
        else if(difference < 60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within an hour");
        }
        else if(difference < 2*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 2 hours");
        }
        else if(difference < 3*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 3 hours");
        }
        else if(difference < 4*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 4 hours");
        }
        else if(difference < 5*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 5 hours");
        }
        else if(difference < 6*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 6 hours");
        }
        else if(difference < 7*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 7 hours");
        }
        else if(difference < 8*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 8 hours");
        }
        else if(difference < 9*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 9 hours");
        }
        else if(difference < 10*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 10 hours");
        }

        else if(difference < 1*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within a day");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 2*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 2 days");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 3*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 3 days");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 4*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 4 days");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 5*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 5 days");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 6*24*60*60*1000){
            headerSb.append("Upcoming Event");
            supInfoSb.append("within 6 days");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", this weekend");
            }
        }
        else if(difference < 7*24*60*60*1000){
            headerSb.append("Scheduled Event");
            supInfoSb.append("within a week");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", on weekend");
            }
        }
        else if(difference < 14*24*60*60*1000){
            headerSb.append("Scheduled Event");
            supInfoSb.append("next week");
            if(itIsOnWeekend(candidate)){
                supInfoSb.append(", on weekend");
            }
        }
        else {
            headerSb.append("Future Event");
            supInfoSb.append("More than 2 weeks in future");
        }


        /*
        if (Math.abs(candidate.get(Calendar.HOUR_OF_DAY) - reference.get(Calendar.HOUR_OF_DAY)) <= 1 &&
                candidate.get(Calendar.DAY_OF_MONTH) == reference.get(Calendar.DAY_OF_MONTH) &&
                candidate.get(Calendar.MONTH) == reference.get(Calendar.MONTH) &&
                candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR)) {
            headerSb.append("Upcoming Event");
            supInfoSb.append("within an hour");
        } else if (Math.abs(candidate.get(Calendar.HOUR_OF_DAY) - reference.get(Calendar.HOUR_OF_DAY)) <= 24 &&
                candidate.get(Calendar.DAY_OF_MONTH) == reference.get(Calendar.DAY_OF_MONTH) &&
                candidate.get(Calendar.MONTH) == reference.get(Calendar.MONTH) &&
                candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR)) {
            headerSb.append("Upcoming Event");
            supInfoSb.append("today, in less than 24 hours");
        } else if (candidate.get(Calendar.MONTH) == reference.get(Calendar.MONTH) &&
                candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR) &&
                candidate.get(Calendar.DAY_OF_MONTH) - reference.get(Calendar.DAY_OF_MONTH) == 1) {
            headerSb.append("Upcoming Event");
            supInfoSb.append("scheduled for tomorrow");
        } else if (candidate.get(Calendar.DAY_OF_MONTH) - reference.get(Calendar.DAY_OF_MONTH) <= 7 &&
                candidate.get(Calendar.MONTH) == reference.get(Calendar.MONTH) &&
                candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR)) {
            headerSb.append("Upcoming Event");
            supInfoSb.append("within an hour");
        } else if (candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR) &&
                candidate.get(Calendar.MONTH) - reference.get(Calendar.MONTH) <= 1 &&
                candidate.get(Calendar.DAY_OF_YEAR) - reference.get(Calendar.DAY_OF_MONTH) < 31) {
            headerSb.append("Scheduled Event");
            supInfoSb.append("within a month");
        } else if (candidate.get(Calendar.YEAR) == reference.get(Calendar.YEAR) &&
                candidate.get(Calendar.MONTH) - reference.get(Calendar.MONTH) <= 3) {
            headerSb.append("Future event");
            supInfoSb.append("within 3 months");
        } else {
            headerSb.append("Future event");
            supInfoSb.append("scheduled");
        }
        */

        vh.mEventHeaderTextView.setText(headerSb.toString());
        vh.mEventSupportingInfoTextView.setText(supInfoSb.toString());

    }

    private void setRepeatComponents(CalendarEventViewHolder vh, int r) {

        if (r == 0) {
            vh.mRepeatSelectionLayoutContainer.setVisibility(LinearLayout.GONE);
            vh.mRepeatDaysLabelLayoutContainer.setVisibility(LinearLayout.GONE);
            return;
        } else {
            vh.mRepeatSelectionLayoutContainer.setVisibility(LinearLayout.VISIBLE);
            vh.mRepeatDaysLabelLayoutContainer.setVisibility(LinearLayout.VISIBLE);
            //
            vh.mRepeatSunButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatMonButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatTueButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatWedButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatThuButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatFriButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
            vh.mRepeatSatButton.setBackgroundResource(R.drawable.ic_cross_gray_24dp);
        }

        if (hardwareDescriptors.repeatsOnSunday(r)) {
            vh.mRepeatSunButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnMonday(r)) {
            vh.mRepeatMonButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnTuesday(r)) {
            vh.mRepeatTueButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnWednesday(r)) {
            vh.mRepeatWedButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnThursday(r)) {
            vh.mRepeatThuButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnFriday(r)) {
            vh.mRepeatFriButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
        if (hardwareDescriptors.repeatsOnSaturday(r)) {
            vh.mRepeatSatButton.setBackgroundResource(R.drawable.ic_check_circle_green_24dp);
        }
    }

    private String setRepeatHeaderText(int r) {
        StringBuilder sb = new StringBuilder();

        switch (hardwareDescriptors.numberOfRepeatDays(r)) {
            case 0:
                sb.append("Event does not repeat");
                break;

            case 1:
                sb.append("Repeats once a week on");
                break;

            case 2:
                sb.append("Repeats twice a week on");
                break;

            case 3:
                sb.append("Repeats 3 times a week on");
                break;

            case 4:
                sb.append("Repeats 4 times a week on");
                break;

            case 5:
                sb.append("Repeats 5 times a week on");
                break;

            case 6:
                sb.append("Repeats 6 times a week on");
                break;

            case 7:
                sb.append("Repeats everyday");
                break;
        }

        return sb.toString();
    }

    private long getNextTriggerDateInMs(CalendarEvent c) {

        /*
        1. Get CalendarEvent object
        2. Does it repeat?
           2a. No:  Is it in past?
               2a1: No:  Send it to setTimeText
               2a2: Yes: Discard. Do not display
           2b. Yes: Is it in past?
               2b1: Yes: Bring it to the next day on which it is set to repeat.
               2b2: No:  Send it to setTimeText
         */

        Calendar candidate = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Log.d(TAG, "getNextTriggerDateInMs: c.getT(): " + c.getT());

        long tMs = (long)c.getT();
        tMs *= 1000;

        candidate.setTimeInMillis(tMs);
        Log.d(TAG, "getNextTriggerDateInMs: candidate: " + candidate.getTimeInMillis());

        Calendar reference = Calendar.getInstance();
        reference.setTimeInMillis(System.currentTimeMillis() + reference.get(Calendar.ZONE_OFFSET));
        Log.d(TAG, "getNextTriggerDateInMs: reference: " + reference.getTimeInMillis());

        if (c.getR() == 0) {
            //Does not repeat
            if (candidate.getTimeInMillis() < reference.getTimeInMillis()) {
                //candidate is in past
            } else {
                //candidate is in future
            }
        } else {
            //Repeats
            if (candidate.getTimeInMillis() < reference.getTimeInMillis()) {
                Log.d(TAG, "candidate is in past");
                //candidate is in past
                //1. first, bring it to present.
                candidate.set(Calendar.YEAR, reference.get(Calendar.YEAR));
                candidate.set(Calendar.MONTH, reference.get(Calendar.MONTH));
                candidate.set(Calendar.DAY_OF_MONTH, reference.get(Calendar.DAY_OF_MONTH));
            }

            //If it is still in past, then add 24 hours
            if (candidate.getTimeInMillis() < reference.getTimeInMillis()) {
                Log.d(TAG, "candidate is still in past: " + candidate.getTimeInMillis());
                candidate.setTimeInMillis(candidate.getTimeInMillis() + (24 * 60 * 60 * 1000));
            }


            //2. Now, check if it is set to trigger today
            for (int i = 0; i < 7; i++) {
                if (hardwareDescriptors.eventIsSetToTriggerForRepeatMask(candidate, c.getR())) {
                    Log.d(TAG, "i:"+ i + " event is set to trigger: candidate:" + candidate.getTimeInMillis());
                    break;
                } else {
                    candidate.setTimeInMillis(candidate.getTimeInMillis() + (24 * 60 * 60 * 1000));
                }
            }

            //At this point whatever we have is correctly offset
        }
        return candidate.getTimeInMillis();
    }

    private String setDateText(long c) {
        StringBuilder stringBuilder = new StringBuilder();

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.setTimeInMillis(c);

        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                stringBuilder.append("Sunday, ");
                break;

            case 2:
                stringBuilder.append("Monday, ");
                break;

            case 3:
                stringBuilder.append("Tuesday, ");
                break;

            case 4:
                stringBuilder.append("Wednesday, ");
                break;

            case 5:
                stringBuilder.append("Thursday, ");
                break;

            case 6:
                stringBuilder.append("Friday, ");
                break;

            case 7:
                stringBuilder.append("Saturday, ");
                break;
        }

        /*
        The Months are numbered from 0 (January) to 11 (December).
        Reference: http://download.oracle.com/javase/6/docs/api/java/util/Calendar.html#MONTH
         */
        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                stringBuilder.append("Jan ");
                break;
            case 1:
                stringBuilder.append("Feb ");
                break;
            case 2:
                stringBuilder.append("Mar ");
                break;
            case 3:
                stringBuilder.append("Apr ");
                break;
            case 4:
                stringBuilder.append("May ");
                break;
            case 5:
                stringBuilder.append("Jun ");
                break;
            case 6:
                stringBuilder.append("Jul ");
                break;
            case 7:
                stringBuilder.append("Aug ");
                break;
            case 8:
                stringBuilder.append("Sep ");
                break;
            case 9:
                stringBuilder.append("Oct ");
                break;
            case 10:
                stringBuilder.append("Nov ");
                break;
            case 11:
                stringBuilder.append("Dec ");
                break;
        }

        stringBuilder.append(calendar.get(Calendar.DAY_OF_MONTH));
        stringBuilder.append(", ");
        stringBuilder.append(calendar.get(Calendar.YEAR));

        return stringBuilder.toString();
    }

    private String setTimeText(CalendarEvent c) {

        //Calendar calendar = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

        StringBuilder sbTime = new StringBuilder();
        StringBuilder sbAmPm = new StringBuilder();

        long tMs = (long)c.getT();
        tMs *= 1000;

        calendar.setTimeInMillis(tMs);
        Log.d(TAG, "Hour_of_day: " + calendar.get(Calendar.HOUR_OF_DAY));
        Log.d(TAG, "Minute:" + calendar.get(Calendar.MINUTE));

        Log.d(TAG, "Hour: " + calendar.get(Calendar.HOUR));
        Log.d(TAG, "Minute:" + calendar.get(Calendar.MINUTE));

        if (calendar.get(Calendar.HOUR_OF_DAY) == 0) {
            sbTime.append("12");
            sbAmPm.append("AM");
        } else if (calendar.get(Calendar.HOUR_OF_DAY) > 0 && calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            sbTime.append("0").append(calendar.get(Calendar.HOUR_OF_DAY));
            sbAmPm.append("AM");
        } else if (calendar.get(Calendar.HOUR_OF_DAY) >= 10 && calendar.get(Calendar.HOUR_OF_DAY) < 12) {
            sbTime.append(calendar.get(Calendar.HOUR_OF_DAY));
            sbAmPm.append("AM");
        } else if (calendar.get(Calendar.HOUR_OF_DAY) == 12) {
            sbTime.append("12");
            sbAmPm.append("PM");
        } else if (calendar.get(Calendar.HOUR_OF_DAY) > 12 && calendar.get(Calendar.HOUR_OF_DAY) <= 23) {
            int h = calendar.get(Calendar.HOUR_OF_DAY) - 12;
            if (h < 10) {
                sbTime.append("0");
            }
            sbTime.append(h);
            sbAmPm.append("PM");
        }

        sbTime.append(":");

        if (calendar.get(Calendar.MINUTE) < 10) {
            sbTime.append("0");
        }
        sbTime.append(calendar.get(Calendar.MINUTE)).append(" ").append(sbAmPm);

        return sbTime.toString();
    }

    private void setAndFormatStateText(TextView v, CalendarEvent c) {
        String stateString = hardwareDescriptors.getDeviceStateStringFromHardwareStateValue(c.getS(), 0);
        v.setText(stateString);
        if (stateString.equalsIgnoreCase("off")) {
            v.setTextColor(Color.GRAY);
        } else {
            v.setTextColor(Color.parseColor("#773f00"));
        }
        Log.d(TAG, "setAndFormatStateText: " + stateString);
    }
}
