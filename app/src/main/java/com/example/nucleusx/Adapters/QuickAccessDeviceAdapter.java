package com.example.nucleusx.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.R;

import java.util.Calendar;

public class QuickAccessDeviceAdapter extends ListAdapter<Device, QuickAccessDeviceAdapter.QuickAccessDeviceHolder> {

    private static final String TAG = "QuickAccessDeviceAdaptr";

    private HardwareDescriptors hd = new HardwareDescriptors();

    CompoundButton.OnCheckedChangeListener switchChangeListener;

    private static SparseBooleanArray mSwitchStateArray = new SparseBooleanArray();
    private static SparseArray mSeekbarStateArray = new SparseArray();

    //private List<Device> quickAccessDevices = new ArrayList<>();
    private HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onSwitchChange(int position, boolean isChecked);
        void onSeekbarChange(int position, int progress, boolean fromUser);
        void onStatusIconClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public QuickAccessDeviceAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Device> DIFF_CALLBACK = new DiffUtil.ItemCallback<Device>() {
        @Override
        public boolean areItemsTheSame(@NonNull Device oldDevice, @NonNull Device newDevice) {

            return oldDevice.getId() == newDevice.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Device oldDevice, @NonNull Device newDevice) {
            return oldDevice.getNodeId() == newDevice.getNodeId() &&
                    oldDevice.getDeviceId() == newDevice.getDeviceId() &&
                    oldDevice.getDeviceConfig() == newDevice.getDeviceConfig() &&
                    oldDevice.getQuickAccessMember() == newDevice.getQuickAccessMember() &&
                    oldDevice.getLocation().equalsIgnoreCase(newDevice.getLocation()) &&
                    oldDevice.getState() == newDevice.getState() &&
                    oldDevice.getUts() == newDevice.getUts() &&
                  //REVALUATE - WHY DO WE NEED THIS
                  //oldDevice.getProcessedByRepositoryTimeStamp() == newDevice.getProcessedByRepositoryTimeStamp() &&
                  //(Math.abs(oldDevice.getProcessedByRepositoryTimeStamp().getTime() - newDevice.getProcessedByRepositoryTimeStamp().getTime()) < 15000) &&
                    (System.currentTimeMillis()-newDevice.getProcessedByRepositoryTimeStamp().getTime() < 15000) &&
                    oldDevice.getName().equalsIgnoreCase(newDevice.getName());
        }
    };

    @NonNull
    @Override
    public QuickAccessDeviceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_quick_access_device_item, viewGroup, false);
        return new QuickAccessDeviceHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull QuickAccessDeviceHolder quickAccessDeviceHolder, int position) {
        Device currentDevice = getItem(position);
        int statusIcon;

        Log.d(TAG, "onBindViewHolder: position: " + position + " uts: " + currentDevice.getUts());

        //Iconography
        if(currentDevice.getUts() == 0){
            statusIcon = R.drawable.ic_in_progress_orange_24dp;
        }
        else if(hd.getCurrentUts() - currentDevice.getUts() > 15){
            statusIcon = R.drawable.ic_check_circle_green_005822_24dp;
        }
        else {
            statusIcon = R.drawable.ic_check_circle_green_008221_24dp;
        }

        //SparseArray swtich state
        int sparseKey = hd.getCombinedNodeDeviceId(currentDevice.getNodeId(), currentDevice.getDeviceId());

        // Components Visibility & State Configuration
        if (hardwareDescriptors.isDigitalOutput(currentDevice.getDeviceConfig())) {
            // Digital Switch

            mSwitchStateArray.put(sparseKey, hd.getBooleanFromDigitalState(currentDevice.getState()));

            // State
            //1. Disable the listener to avoid phantom-changes
            //quickAccessDeviceHolder.mSwitch.setOnCheckedChangeListener(null);
            //2. Change the switch state
            Log.d(TAG, "received current state: " + currentDevice.getState() + " name:" + hd.sanitzeDeviceNameForDevice(currentDevice.getName(), currentDevice.getDeviceId()));
            quickAccessDeviceHolder.mSwitch.setChecked(
                    hd.getBooleanFromDigitalState(currentDevice.getState())
            );
            //3. Reenable it back
            //quickAccessDeviceHolder.mSwitch.setOnCheckedChangeListener(switchChangeListener);

            //Visibility
            quickAccessDeviceHolder.mSwitch.setVisibility(View.VISIBLE);
            quickAccessDeviceHolder.mSeekbar.setVisibility(View.GONE);
            quickAccessDeviceHolder.mTextViewSeekbarValue.setVisibility(View.GONE);
        } else if (hardwareDescriptors.isPwmOutput(currentDevice.getDeviceConfig())) {
            //PWM output

            mSeekbarStateArray.put(sparseKey, currentDevice.getState());

            //State
            quickAccessDeviceHolder.mSeekbar.setProgress(
                    currentDevice.getState()
            );
            quickAccessDeviceHolder.mTextViewSeekbarValue.setText(hd.getStatePercentValue(currentDevice.getState())+"%");
            //Visibility
            quickAccessDeviceHolder.mSwitch.setVisibility(View.GONE);
            quickAccessDeviceHolder.mSeekbar.setVisibility(View.VISIBLE);
            quickAccessDeviceHolder.mTextViewSeekbarValue.setVisibility(View.VISIBLE);
        } else {
            quickAccessDeviceHolder.mSwitch.setVisibility(View.GONE);
            quickAccessDeviceHolder.mSeekbar.setVisibility(View.GONE);
            quickAccessDeviceHolder.mTextViewSeekbarValue.setVisibility(View.GONE);
        }

        //Device Name
        quickAccessDeviceHolder.mTextViewDeviceName.setText(
                hd.sanitzeDeviceNameForDevice(currentDevice.getName(), currentDevice.getDeviceId())
        );

        //Device Location
        quickAccessDeviceHolder.mTextViewDeviceLocation.setText(currentDevice.getLocation());

        //Status Icon
        quickAccessDeviceHolder.mStatusIcon.setImageResource(statusIcon);
    }

    class QuickAccessDeviceHolder extends RecyclerView.ViewHolder {
        private ImageView mStatusIcon;
        private TextView mTextViewDeviceName;
        private TextView mTextViewDeviceLocation;
        private TextView mTextViewSeekbarValue;
        private Switch mSwitch;
        private SeekBar mSeekbar;


        public QuickAccessDeviceHolder(@NonNull View itemView) {
            super(itemView);
            mStatusIcon = itemView.findViewById(R.id.cqaStatusIcon);
            mTextViewDeviceName = itemView.findViewById(R.id.cqaDeviceName);
            mTextViewDeviceLocation = itemView.findViewById(R.id.cqaDeviceLocation);
            mTextViewSeekbarValue = itemView.findViewById(R.id.cqaSeekbarValue);
            mSeekbar = itemView.findViewById(R.id.cqaSeekBar);
            mSwitch = itemView.findViewById(R.id.cqaSwitch);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position);
                        //mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                    }
                }
            });

            mStatusIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onStatusIconClick(position);
                        mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                    }
                }
            });

            switchChangeListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {

                        Device device = getItem(position);
                        // Create the unique key: nodeId + deviceId
                        int key = hd.getCombinedNodeDeviceId(device.getNodeId(), device.getDeviceId());
                        if(mSwitchStateArray.indexOfKey(key) < 0){
                            //This key does not exist in the array yet
                            mSwitchStateArray.put(key, isChecked);
                        }

                        if(mSwitchStateArray.get(key) == isChecked){
                            // the switch state is same as last recorded - so this might be a phantom trigger.
                        }
                        else {
                            listener.onSwitchChange(position, isChecked);
                            mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                            Log.d(TAG, "setOnCheckedChangeListener. state is " + isChecked);
                        }

                    }
                }
            };
            mSwitch.setOnCheckedChangeListener(switchChangeListener);

            /*mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onSwitchChange(position, isChecked);
                        mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        Log.d(TAG, "setOnCheckedChangeListener. state is " + isChecked);
                    }
                }
            });*/

            mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {

                        Device device = getItem(position);
                        int key = (device.getNodeId() << 8) | device.getDeviceId();
                        if(mSeekbarStateArray.indexOfKey(key)<0){
                            //This key does not exist in the array yet
                            Log.d(TAG, "key " + key + " does not exist in sparseARray");
                            mSeekbarStateArray.put(key, device.getState());
                        }

                        //Send request only if the difference is more than preset
                        int difference = Math.abs(progress - (int)mSeekbarStateArray.get(key));
                        Log.d(TAG, "for key: " + key + "  state: " + device.getState() + " progress: " + mSeekbarStateArray.get(key) + " difference:" + difference);
                        if(difference > 75){
                            Log.d(TAG, "difference is more than 10");
                            listener.onSeekbarChange(position, progress, fromUser);

                            //The response will set it to the correct value - but to avoid flicker on the UI element, set it manually here as well.
                            mSeekbarStateArray.put(key, progress);
                        }

                        //But update UI elements immediately
                        mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        mTextViewSeekbarValue.setText(hd.getStatePercentValue(progress)+"%");
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {

                        listener.onSeekbarChange(position, seekBar.getProgress(), true);

                        //update UI elements immediately
                        mStatusIcon.setImageResource(R.drawable.ic_in_progress_orange_24dp);
                        mTextViewSeekbarValue.setText(hd.getStatePercentValue(seekBar.getProgress())+"%");
                    }
                }
            });

        }
    }

    public Device getDeviceAtPosition(int position) {
        return getItem(position);
    }
}
