package com.example.nucleusx.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ScannedNodeItemAdapter extends RecyclerView.Adapter<ScannedNodeItemAdapter.ScannedNodeItemViewHolder> {

    private static final String TAG = "ScannedNodeItemAdapter";

    private List<ScannedNodeItem> mNodeList;
    private OnItemClickListener mListener;

    //For Click capture
    public interface OnItemClickListener {
        void onItemClick(int position);
        void onLocateClick(int position);
        void onConfigureClick(int position);
        void onMoreInfoClick(int position);
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public static class ScannedNodeItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView mConfigureImage;
        private ImageView mLocateImage;
        private ImageView mMoreInfoImage;
        private Button mDeleteButton;

        private TextView mPrimaryHeader;
        private ImageView mStatusIcon;
        private TextView mSecondaryHeader;
        private TextView mAdditionalInfoField;
        private TextView mIpField;

        public ScannedNodeItemViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            mLocateImage = itemView.findViewById(R.id.sniLocateImage);
            mConfigureImage = itemView.findViewById(R.id.sniConfigureImage);
            mMoreInfoImage = itemView.findViewById(R.id.sniMoreInfoImage);
            mDeleteButton = itemView.findViewById(R.id.sniDeleteNode);
            mPrimaryHeader = itemView.findViewById(R.id.sniPrimaryHeader);
            mStatusIcon = itemView.findViewById(R.id.sniStatusIcon);
            mSecondaryHeader = itemView.findViewById(R.id.sniSecondaryHeader);
            mAdditionalInfoField = itemView.findViewById(R.id.sniAdditionalInfoField);
            mIpField = itemView.findViewById(R.id.sniIpField);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            mLocateImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onLocateClick(position);
                        }
                    }
                }
            });

            mConfigureImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onConfigureClick(position);
                        }
                    }
                }
            });

            mMoreInfoImage.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onMoreInfoClick(position);
                        }
                    }
                }
            });

            mDeleteButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public ScannedNodeItemAdapter(List<ScannedNodeItem> nodeList) {
        mNodeList = nodeList;
    }

    public ScannedNodeItemAdapter(){
        mNodeList = new ArrayList<ScannedNodeItem>();
    }

    @NonNull
    @Override
    public ScannedNodeItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_node_item, viewGroup, false);
        ScannedNodeItemViewHolder nvh = new ScannedNodeItemViewHolder(v, mListener);
        return nvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ScannedNodeItemViewHolder scannedNodeItemViewHolder, int i) {
        ScannedNodeItem currentItem = mNodeList.get(i);

        scannedNodeItemViewHolder.mPrimaryHeader.setText("Node Id " + String.valueOf(currentItem.getNodeId()));
        scannedNodeItemViewHolder.mSecondaryHeader.setText("Unregistered");
        scannedNodeItemViewHolder.mIpField.setText(currentItem.getNodeIp());

        //Last updated - date
        StringBuilder sb = new StringBuilder();
        sb.append("Seen ");
        DateFormat dateFormat;
        if(System.currentTimeMillis() - currentItem.getLastUpdated().getTime() < 30000){
            sb.append("few seconds ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else if(System.currentTimeMillis() - currentItem.getLastUpdated().getTime() < 60000){
            sb.append("less than a minute ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else if(System.currentTimeMillis() - currentItem.getLastUpdated().getTime() < 5*60*1000){
            sb.append("few minutes ago ");
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        }
        else {
            dateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
        }

        sb.append(dateFormat.format(currentItem.getLastUpdated()));
        scannedNodeItemViewHolder.mAdditionalInfoField.setText(sb.toString());
    }

    @Override
    public int getItemCount() {
        return mNodeList.size();
    }

    public void setmNodeList(List<Node> nodes){
        for(Node node : nodes){
            if(nodeExistsInScannedNodeItemList(node)){
                int index = indexOfScannedNodeItemWithNodeId(node.getNodeId());

                //Update
                mNodeList.get(index).setNodeIp(node.getIp());
                mNodeList.get(index).setLastUpdated(node.getLastUpdated());

                Log.d(TAG, "UPDATE: nodeExistsInScannedNodeItemList: node:" + mNodeList.get(index).getNodeId() +
                        " ip:" + mNodeList.get(index).getNodeIp() +
                        " last updated:" + mNodeList.get(index).getLastUpdated());

                //Notify adapter
                notifyItemChanged(index);
            }
            else {
                //Add
                ScannedNodeItem scannedNodeItem = new ScannedNodeItem(
                        node.getNodeId(),
                        node.getIp(),
                        node.getRegistered().toString()
                );

                scannedNodeItem.setLastUpdated(node.getLastUpdated());

                mNodeList.add(scannedNodeItem);
                Log.d(TAG, "ADD: nodeExistsInScannedNodeItemList: node:" + mNodeList.get(mNodeList.indexOf(scannedNodeItem)).getNodeId() + " ip:" + mNodeList.get(mNodeList.indexOf(scannedNodeItem)).getNodeIp());

                notifyItemInserted(mNodeList.indexOf(scannedNodeItem));
            }
        }
    }

    private boolean nodeExistsInScannedNodeItemList(Node node){
        for(ScannedNodeItem scannedNodeItem : mNodeList){
            if(scannedNodeItem.getNodeId() == node.getNodeId()){
               return true;
            }
        }
        return false;
    }

    private int indexOfScannedNodeItemWithNodeId(int nodeId){
        int i = -1;
        for(ScannedNodeItem scannedNodeItem : mNodeList){
            if(scannedNodeItem.getNodeId() == nodeId){
                i = mNodeList.indexOf(scannedNodeItem);
            }
        }
        return i;
    }
}
