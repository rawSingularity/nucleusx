package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.ActivityParcels.TemporaryDeviceInfo;
import com.example.nucleusx.Adapters.ControlItemAdapter;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeApis.GenericGetApi;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.DeviceElement;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceList;
import com.example.nucleusx.ViewModels.DeviceViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.graphics.Color.rgb;

public class NodeControlActivity extends AppCompatActivity {

    static final String TAG = "NodeControlActivity";

    //This will be the list to render UI components
    private ArrayList<ControlItem> controlItemList = new ArrayList<>();

    //Recycler View
    private RecyclerView mRecyclerView;
    private ControlItemAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    //ViewModel
    private DeviceViewModel deviceViewModel;

    //Retrofit
    Retrofit retrofit;

    //Hardware Descriptors
    private HardwareDescriptors hd;

    private TextView vcvIpAddress;
    private Button vcvButton;
    //
    private int nodeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        setTitle("Node Controls");

        vcvIpAddress = findViewById(R.id.cvIpAddress);
        vcvButton = findViewById(R.id.cvButton);

        hd = new HardwareDescriptors(this);

        vcvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hide keyboard
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(NodeControlActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                Snackbar.make(v, "Fetching controls", Toast.LENGTH_LONG).show();

                //Reset nodeId if this is a different IP
                nodeId = 0;

                //Reset controlItemList & notify UI
                controlItemList.clear();
                mAdapter.notifyDataSetChanged();

                // First Get Device List
                getDeviceList();
            }
        });

        // Build Sample items
        //addSampleControlItems();

        // Build recycler view
        buildRecyclerView();

        // To get data from Node Scanner Activity
        Intent intent = getIntent();
        ScannedNodeItem nodeItem = intent.getParcelableExtra("Node Item");
        if (nodeItem != null) {
            vcvIpAddress.setText(nodeItem.getNodeIp());
            //Simulate click
            vcvButton.performClick();
            //Hide elements
            vcvIpAddress.setVisibility(View.GONE);
            vcvButton.setVisibility(View.GONE);
            //
            //Set title
            setTitle("Controls for Node " + nodeItem.getNodeId());
            //Set nodeId
            this.nodeId = nodeItem.getNodeId();

            Snackbar.make(findViewById(android.R.id.content), "Incoming parcel: node: " + nodeItem.getNodeId() + " ip:" + nodeItem.getNodeIp(), Snackbar.LENGTH_LONG).show();
        }


        //To get device from quickAccess mainActivity
        com.example.nucleusx.ActivityParcels.Device device = intent.getParcelableExtra("Device Item");
        if(device != null) {

            //LiveData
            deviceViewModel = ViewModelProviders.of(this).get(DeviceViewModel.class);
            deviceViewModel.getDeviceInfo(device.getNodeId(), device.getDeviceId()).observe(this, new Observer<List<Device>>() {
                @Override
                public void onChanged(@Nullable List<Device> devices) {
                    //Toast.makeText(MainActivity.this, "Got all quick access devices", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "getDeviceInfo: onChanged");
                }
            });
        }
    }

    private void getDeviceList() {
        String ip = vcvIpAddress.getText().toString();
        if (!hd.validIp(ip)) {
            Snackbar.make(findViewById(android.R.id.content), ip + " is not a valid IP address", Snackbar.LENGTH_LONG).show();
            return;
        }

        retrofit = new Retrofit.Builder()
                .baseUrl("http://" + ip + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GenericGetApi getDeviceListApi = retrofit.create(GenericGetApi.class);
        Call<NodeResponseObjDeviceList> call = getDeviceListApi.getNodeResponseObjDeviceList();
        call.enqueue(new Callback<NodeResponseObjDeviceList>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceList> call, Response<NodeResponseObjDeviceList> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeControlActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                NodeResponseObjDeviceList deviceListObj = response.body();
                if (deviceListObj.getDeviceList() == null) {
                    Log.d(TAG, "ERROR: getDeviceList: received NULL device list");
                    return;
                }

                Log.d(TAG, "getDeviceList: Received device list of size: " + deviceListObj.getDeviceList().size());
                buildDeviceEndPointsFromDeviceList(deviceListObj.getDeviceList());

                //Set title now that we have info
                setTitle("Controls for Node " + deviceListObj.getNode());

                //Now start building of deviceInfo for each device card
                startFetchingInfoForDeviceCards();
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceList> call, Throwable t) {
                Log.d(TAG, "getDeviceList>onFailure: " + t.getMessage());
            }
        });
    }

    private void startFetchingInfoForDeviceCards() {
        Log.d(TAG, "controlItemList size: " + controlItemList.size());
        if(controlItemList.size() > 0){
            //Toast.makeText(this, "Refreshing Device States", Toast.LENGTH_LONG).show();
        }

        for (ControlItem c : controlItemList) {
            performDeviceQuery(c.getDeviceInfo().getD());
        }
    }

    private void buildDeviceEndPointsFromDeviceList(List<DeviceElement> deviceList) {

        //TODO: Sort this list

        // Create Cards
        ControlItem item;
        int position;

        for (DeviceElement d : deviceList) {
            Log.d(TAG, "post-sort: d:" + d.getD());

            String name = hd.sanitzeDeviceNameForDevice(d.getName(), d.getD());

            item = new ControlItem(
                    new TemporaryDeviceInfo(d.getD()), name
            );

            controlItemList.add(item);
            position = controlItemList.indexOf(item);

            //Insert at the position
            Log.d(TAG, "AddDeviceInfoToRecyclerView > mAdapter.notifyItemInserted");
            mAdapter.notifyItemInserted(position);
        }
    }

    private void performDeviceQuery(final int deviceId) {
        String ip = vcvIpAddress.getText().toString();
        if (!hd.validIp(ip)) {
            Snackbar.make(findViewById(android.R.id.content), ip + " is not a valid IP address", Snackbar.LENGTH_LONG).show();
            return;
        }

        GenericGetApi getDeviceInfoApi = retrofit.create(GenericGetApi.class);
        Call<NodeResponseObjDeviceInfo> call = getDeviceInfoApi.getNodeResponseObjDeviceInfo(deviceId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeControlActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                NodeResponseObjDeviceInfo deviceInfo = response.body();
                Log.d(TAG, "Device: " + deviceInfo.getD() + " current state: " + deviceInfo.getS() + " configured: " + deviceInfo.getC() + "\n");
                //Toast.makeText(NodeControlActivity.this, "Fetched: d=" + deviceInfo.getD() + ", s=" + deviceInfo.getS(), Toast.LENGTH_SHORT).show();

                //Update NodeId if not set
                if (nodeId == 0) {
                    nodeId = deviceInfo.getNode();
                }

                //Update it to the list
                AddDeviceInfoToRecyclerView(deviceInfo);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void setDeviceState(int deviceId, int state) {
        if (nodeId == 0) {
            Snackbar.make(findViewById(android.R.id.content), "Node Id missing - cannot set device state", Snackbar.LENGTH_LONG).show();
            return;
        }

        String ip = vcvIpAddress.getText().toString();
        if (!hd.validIp(ip)) {
            Snackbar.make(findViewById(android.R.id.content), ip + " is not a valid IP address", Snackbar.LENGTH_LONG).show();
            return;
        }

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjDeviceInfo(deviceId, state, nodeId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(NodeControlActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                NodeResponseObjDeviceInfo deviceInfo = response.body();
                //Toast.makeText(NodeControlActivity.this, "Device " + deviceInfo.getD() + " set to " + deviceInfo.getS(), Toast.LENGTH_SHORT).show();
                Toast.makeText(NodeControlActivity.this, deviceInfo.getN() + " " + hd.getDeviceStateString(deviceInfo.getS()), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Received response: D" + deviceInfo.getD() + " S:" + deviceInfo.getS());

                //Update it to the list
                AddDeviceInfoToRecyclerView(deviceInfo);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void AddDeviceInfoToRecyclerView(NodeResponseObjDeviceInfo deviceInfo) {
        ControlItem item;
        int position;

        if (entryExistsForDevice(deviceInfo.getD())) {
            Log.d(TAG, "AddDeviceInfoToRecyclerView: D" + deviceInfo.getD() + " already exist!");
            position = indexOfDevice(deviceInfo.getD());

            if (position == -1) {
                //Something is wrong
                Log.d(TAG, "Device D" + deviceInfo.getD() + " not found in controlItemList");
                return;
            }

            item = controlItemList.get(indexOfDevice(position));

            //update
            copyDeviceInfoIntoItem(item.getDeviceInfo(), deviceInfo);
            item.setDeviceName(hd.sanitzeDeviceNameForDevice(deviceInfo.getN(), deviceInfo.getD()));

            // Mark the card as initialized
            item.setThisCardInitialized(true);

            //update controlItemList
            controlItemList.set(position, item);

            //Update
            Log.d(TAG, "AddDeviceInfoToRecyclerView > mAdapter.notifyItemChanged");
            mAdapter.notifyItemChanged(indexOfDevice(position));
        } else {
            // else create new one
            // first copy
            TemporaryDeviceInfo temporaryDeviceInfo = new TemporaryDeviceInfo();
            copyDeviceInfoIntoItem(temporaryDeviceInfo, deviceInfo);

            item = new ControlItem(
                    temporaryDeviceInfo,
                    "Device D" + deviceInfo.getD()
            );

            controlItemList.add(item);
            position = controlItemList.size();

            //Insert at the position
            Log.d(TAG, "AddDeviceInfoToRecyclerView > mAdapter.notifyItemInserted");
            mAdapter.notifyItemInserted(position);
        }

        //Update Switch position
        //controlItemList.get(position).

        //Scroll to the newly added position
        //mRecyclerView.smoothScrollToPosition(position);
    }

    private void copyDeviceInfoIntoItem(TemporaryDeviceInfo temporaryDeviceInfo, NodeResponseObjDeviceInfo deviceInfo) {
        temporaryDeviceInfo.setCalendarEvents(deviceInfo.getCalendarEvents());
        temporaryDeviceInfo.setNode(deviceInfo.getNode());
        temporaryDeviceInfo.setTime(deviceInfo.getTime());
        temporaryDeviceInfo.setUTS(deviceInfo.getUTS());
        //temporaryDeviceInfo.setReqProcessingTime(deviceInfo.getReqProcessingTime());
        temporaryDeviceInfo.setIp(deviceInfo.getIp());
        temporaryDeviceInfo.setN(deviceInfo.getN());
        temporaryDeviceInfo.setC(deviceInfo.getC());
        temporaryDeviceInfo.setD(deviceInfo.getD());
        temporaryDeviceInfo.setG(deviceInfo.getG());
        temporaryDeviceInfo.setM(deviceInfo.getM());
        temporaryDeviceInfo.setS(deviceInfo.getS());
    }

    private boolean entryExistsForDevice(int d) {
        for (ControlItem i : controlItemList) {
            if (i.getDeviceInfo().getD() == d) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        startFetchingInfoForDeviceCards();
    }

    private int indexOfDevice(int d) {
        for (ControlItem i : controlItemList) {
            if (i.getDeviceInfo().getD() == d) {
                Log.d(TAG, "indexOfDevice d:" + d + " is " + controlItemList.indexOf(i));
                return controlItemList.indexOf(i);
            }
        }
        return -1;
    }

    public void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.cvRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ControlItemAdapter(controlItemList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ControlItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Toast.makeText(NodeControlActivity.this, "Refetching device:" + controlItemList.get(position).getDeviceInfo().getD() + " info", Toast.LENGTH_LONG).show();
                //
                refetchDeviceInfo(position);
            }

            @Override
            public void onSettingsClick(int position) {
                //Open activity
                openSettingsActivity(controlItemList.get(position));
            }

            @Override
            public void onSwitchChange(final int position, boolean isChecked) {
                Log.d(TAG, "onSwitchChange: position:" + position + " state:" + isChecked);
                int state = hd.getDigitalStateFromBoolean(isChecked);
                setDeviceState(controlItemList.get(position).getDeviceInfo().getD(), state);
            }

            @Override
            public void onSeekbarChange(int position, int progress, boolean fromUser) {
                Log.d(TAG, "onSeekbarChange: position:" + position + " progress:" + progress + " fromUser?:" + fromUser);
                if (fromUser) {
                    setDeviceState(controlItemList.get(position).getDeviceInfo().getD(), progress);
                }
            }

            @Override
            public void onManageEventsClick(int position) {
                //Open activity
                openManageEventsActivity(controlItemList.get(position));
            }
        });
    }

    private void openSettingsActivity(ControlItem parcel) {
        // Flow into next Activity
        if (parcel == null) {
            //Snackbar.make(NodeControlActivity.this, "Data not yet ready.\nCannot flow into Controls", Snackbar.LENGTH_LONG).show();
            Log.d(TAG, "Error: unable to open device settings - parcel is empty");
            return;
        }
        Intent intent = new Intent(NodeControlActivity.this, DeviceSettingsActivity.class);
        intent.putExtra("Device Info Parcel", parcel);
        startActivity(intent);
    }

    private void openManageEventsActivity(ControlItem parcel){
        if(parcel == null){
            return;
        }
        Intent intent = new Intent(NodeControlActivity.this, ManageEventsActivity.class);
        intent.putExtra("Device Info Parcel", parcel);
        startActivity(intent);
    }

    public void refetchDeviceInfo(int position) {
        //controlItemList.get(position).setmImageResource(R.drawable.ic_in_progress_orange_24dp);
        //Log.d(TAG, "refetchDeviceInfo > mAdapter.notifyItemChanged");

        //mAdapter.notifyItemChanged(position);
        //
        performDeviceQuery(controlItemList.get(position).getDeviceInfo().getD());
    }

    public void addSampleControlItems() {
        controlItemList.add(new ControlItem(
                null,
                "Porch light (D1)"
        ));

        controlItemList.add(new ControlItem(
                null,
                "Coffee maker (D2)"
        ));

        controlItemList.add(new ControlItem(
                null,
                "Entertainment center (D3)"
        ));

        controlItemList.add(new ControlItem(
                null,
                "Desk lamp (D4)"
        ));
    }
}