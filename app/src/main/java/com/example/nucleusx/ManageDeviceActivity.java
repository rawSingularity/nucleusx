package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.nucleusx.ActivityParcels.DeviceListParcel;
import com.example.nucleusx.ActivityParcels.TemporaryDeviceInfo;
import com.example.nucleusx.Adapters.ManageDeviceAdapter;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.ViewModels.DeviceViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ManageDeviceActivity extends AppCompatActivity {

    private static final String TAG = "ManageDeviceActivity";

    private DeviceViewModel deviceViewModel;
    private List<Device> mAllDevices;
    //This will be the list to render UI components
    private List<Device> mFilteredDevices = new ArrayList<>();
    private List<Integer> mRequestedDevices;

    //Recycler View
    private RecyclerView mRecyclerView;
    private ManageDeviceAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private String mListName;

    HardwareDescriptors hd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_device);

        hd = new HardwareDescriptors(this);

        buildRecyclerView();

        getIncomingParcel();

        StringBuilder sb = new StringBuilder("");
        if(mRequestedDevices.size() == 1){
            sb.append("Manage Device");
        }
        else {
            sb.append(mListName.trim());
            sb.append(" devices");
        }

        setTitle(sb.toString());

        deviceViewModel = ViewModelProviders.of(this).get(DeviceViewModel.class);
        deviceViewModel.getAllDevices().observe(this, new Observer<List<Device>>() {
            @Override
            public void onChanged(@Nullable List<Device> devices) {
                mAllDevices = devices;

                BuildCustomDeviceListForThisContextAsyncTask buildCustomDeviceListForThisContextAsyncTask = new BuildCustomDeviceListForThisContextAsyncTask();
                buildCustomDeviceListForThisContextAsyncTask.execute();
            }
        });
    }

    private void getIncomingParcel() {
        Intent intent = getIntent();
        //mRequestedDevices = (List) intent.getParcelableArrayListExtra("Device List");
        DeviceListParcel deviceListParcel = intent.getParcelableExtra("Device List");

        mListName = deviceListParcel.getListName();

        if(mRequestedDevices == null){
            mRequestedDevices = new ArrayList<Integer>();
        }
        else {
            mRequestedDevices.clear();
        }

        for(com.example.nucleusx.ActivityParcels.Device device : deviceListParcel.getDeviceList()){
            mRequestedDevices.add(hd.getCombinedNodeDeviceId(device.getNodeId(), device.getDeviceId()));
        }

        for(int i : mRequestedDevices){
            Log.d(TAG, "Requested Device: NodeId: " + (i>>8) + " deviceId: " + (i&0xFF));
        }
    }

    public void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.dcRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ManageDeviceAdapter();

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ManageDeviceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Device device = mAdapter.getDeviceAtPosition(position);
                deviceViewModel.refetchDeviceStateFromDevice(device, hd.getIpForNodeId(device.getNodeId()));
            }

            @Override
            public void onSettingsClick(int position) {
                //Open activity
                openSettingsActivity(convertDeviceIntoControlItem(mAdapter.getDeviceAtPosition(position)));
            }

            @Override
            public void onSwitchChange(final int position, boolean isChecked) {
                Log.d(TAG, "onSwitchChange: position:" + position + " state:" + isChecked);
                int state = hd.getDigitalStateFromBoolean(isChecked);
                //setDeviceState(controlItemList.get(position).getDeviceInfo().getD(), state);

                Device device = mAdapter.getDeviceAtPosition(position);
                Device candidate = new Device(device);
                candidate.setState(state);
                deviceViewModel.setDeviceState(candidate, hd.getIpForNodeId(device.getNodeId()));
            }

            @Override
            public void onSeekbarChange(int position, int progress, boolean fromUser) {
                Log.d(TAG, "onSeekbarChange: position:" + position + " progress:" + progress + " fromUser?:" + fromUser);
                if (fromUser) {
                    //setDeviceState(controlItemList.get(position).getDeviceInfo().getD(), progress);
                    Device device = mAdapter.getDeviceAtPosition(position);
                    Device candidate = new Device(device);
                    candidate.setState(progress);
                    deviceViewModel.setDeviceState(candidate, hd.getIpForNodeId(device.getNodeId()));
                }
            }

            @Override
            public void onManageEventsClick(int position) {
                //Open activity
                openManageEventsActivity(convertDeviceIntoControlItem(mAdapter.getDeviceAtPosition(position)));
            }
        });
    }

    private class BuildCustomDeviceListForThisContextAsyncTask extends AsyncTask<Void, Void, List<Device>>{

        @Override
        protected List<Device> doInBackground(Void... voids) {
            List<Device> customDevices = new ArrayList<Device>();

            for(Device device : mAllDevices){
                if(devicePartOfRequestList(device.getNodeId(), device.getDeviceId())){
                    customDevices.add(device);
                }
            }

            return customDevices;
        }

        @Override
        protected void onPostExecute(List<Device> devices) {
            super.onPostExecute(devices);
            mFilteredDevices = devices;

            //Let Adapter know about the change
            //mAdapter.setDeviceList(mFilteredDevices);
            //mAdapter.notifyDataSetChanged();
            mAdapter.submitList(mFilteredDevices);
        }
    }

    private boolean devicePartOfRequestList(int nodeId, int deviceId){

        for(Integer i : mRequestedDevices){
            if(i == hd.getCombinedNodeDeviceId(nodeId,deviceId)){
                return true;
            }
        }
        return false;
    }


    // LEGACY CODE - THIS NEEDS TO GO EVENTUALLY

    private ControlItem convertDeviceIntoControlItem(Device device){

        TemporaryDeviceInfo deviceInfo = new TemporaryDeviceInfo();
        deviceInfo.setUTS((int)device.getUts());
        deviceInfo.setIp(hd.getIpForNodeId(device.getNodeId()));
        deviceInfo.setN(device.getName());
        deviceInfo.setD(device.getDeviceId());
        deviceInfo.setNode(device.getNodeId());
        deviceInfo.setC(device.getDeviceConfig());
        deviceInfo.setS(device.getState());

        ControlItem controlItem = new ControlItem(deviceInfo);
        controlItem.setDeviceName(device.getName());

        return controlItem;
    }

    private void openSettingsActivity(ControlItem parcel) {
        // Flow into next Activity
        if (parcel == null) {
            //Snackbar.make(NodeControlActivity.this, "Data not yet ready.\nCannot flow into Controls", Snackbar.LENGTH_LONG).show();
            Log.d(TAG, "Error: unable to open device settings - parcel is empty");
            return;
        }
        Intent intent = new Intent(ManageDeviceActivity.this, DeviceSettingsActivity.class);
        intent.putExtra("Device Info Parcel", parcel);
        startActivity(intent);
    }

    private void openManageEventsActivity(ControlItem parcel){
        if(parcel == null){
            return;
        }
        Intent intent = new Intent(ManageDeviceActivity.this, ManageEventsActivity.class);
        intent.putExtra("Device Info Parcel", parcel);
        startActivity(intent);
    }
}
