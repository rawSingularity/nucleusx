package com.example.nucleusx;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nucleusx.ActivityParcels.TemporaryDeviceInfo;

public class ControlItem implements Parcelable {

    //
    private TemporaryDeviceInfo deviceInfo;
    //
    private String deviceName;
    //
    private boolean thisCardInitialized;


    protected ControlItem(Parcel in) {
        deviceInfo = in.readParcelable(TemporaryDeviceInfo.class.getClassLoader());
        deviceName = in.readString();
        thisCardInitialized = in.readByte() != 0;
    }

    public static final Creator<ControlItem> CREATOR = new Creator<ControlItem>() {
        @Override
        public ControlItem createFromParcel(Parcel in) {
            return new ControlItem(in);
        }

        @Override
        public ControlItem[] newArray(int size) {
            return new ControlItem[size];
        }
    };

    public void setThisCardInitialized(boolean thisCardInitialized) {
        this.thisCardInitialized = thisCardInitialized;
    }

    public boolean isThisCardInitialized() {
        return thisCardInitialized;
    }

    public ControlItem(TemporaryDeviceInfo deviceInfo, String deviceName) {
        this.deviceInfo = deviceInfo;
        this.deviceName = deviceName;
    }

    public ControlItem(TemporaryDeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public TemporaryDeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(TemporaryDeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(deviceInfo, flags);
        dest.writeString(deviceName);
        dest.writeByte((byte) (thisCardInitialized ? 1 : 0));
    }
}
