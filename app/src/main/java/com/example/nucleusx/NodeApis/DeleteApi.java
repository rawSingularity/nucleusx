package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjCalendarEvents;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DeleteApi {

    @GET("delete/calendarEvents")
    Call<NodeResponseObjCalendarEvents> getNodeResponseObjCalendarEventsPostEventDeletion(@Query("j") int eventIndex,
                                                                                          @Query("i") int id);
}
