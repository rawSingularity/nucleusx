package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjMdnsCache;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MdnsCacheApi {

    @GET("get/mdnsCache")
    Call<NodeResponseObjMdnsCache> getNodeResponseObjMdnsCache();
}
