package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjSysStat;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SysStatApi {

    @GET("get/sysStat")
    Call<NodeResponseObjSysStat> getNodeResponseObjSysStat();
}
