package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GenericGetApi {
    @GET("get")
    Call<NodeResponseObjDeviceInfo> getNodeResponseObjDeviceInfo(@Query("d") int deviceId);

    @GET("get/deviceList")
    Call<NodeResponseObjDeviceList> getNodeResponseObjDeviceList();
}
