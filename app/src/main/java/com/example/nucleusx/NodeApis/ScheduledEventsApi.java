package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjScheduledEvents;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ScheduledEventsApi {

    @GET("get/scheduledEvents")
    Call<NodeResponseObjScheduledEvents> getNodeResponseObjScheduledEvents();
}
