package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjId;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IdApi {

    @GET("get/id")
    Call<NodeResponseObjId> getNodeResponseObjId();
}
