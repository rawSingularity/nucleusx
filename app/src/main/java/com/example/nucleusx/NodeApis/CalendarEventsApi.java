package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjCalendarEvents;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CalendarEventsApi {

    @GET("get/calendarEvents")
    Call<NodeResponseObjCalendarEvents> getNodeResponseObjCalendarEvents();

    @GET("get/calendarEvents")
    Call<NodeResponseObjCalendarEvents> getNodeResponseObjCalendarEventsForDevice(@Query("d") int deviceId);
}
