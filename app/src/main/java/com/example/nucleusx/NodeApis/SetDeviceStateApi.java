package com.example.nucleusx.NodeApis;

import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SetDeviceStateApi {

    @GET("set")
    //@GET("set?i=2937520&d=1&s=1")
    Call<NodeResponseObjDeviceInfo> getNodeResponseObjDeviceInfo(@Query("d") int deviceId,
                                                                 @Query("s") int state,
                                                                 @Query("i") int id);

    @GET("set/configuration")
    Call<NodeResponseObjDeviceInfo> getNodeResponseObjPostConfiguration(@Query("d") int deviceId,
                                                                        @Query("c") int config,
                                                                        @Query("i") int id);

    @GET("set/configuration")
    Call<NodeResponseObjDeviceInfo> getNodeResponseObjPostDeviceNameChange(@Query("d") int deviceId,
                                                                           @Query("n") String deviceName,
                                                                           @Query("i") int id);

    @GET("set/calendarEvents")
    Call<NodeResponseObjDeviceInfo> getNodeResponseObjPostCalendarEventAddition(@Query("d") int deviceId,
                                                                                @Query("s") int state,
                                                                                @Query("t") long time,
                                                                                @Query("r") int repeatMask,
                                                                                @Query("i") int id);

    @GET("set/beacon")
    Call<NodeResponseObjDeviceInfo> setBeacon(@Query("i") int nodeId);

    @GET("set/reboot")
    Call<NodeResponseObjDeviceInfo> setReboot(@Query("i") int nodeId);

    @GET("set/reset")
    Call<NodeResponseObjDeviceInfo> setReset(@Query("i") int nodeId);
}
