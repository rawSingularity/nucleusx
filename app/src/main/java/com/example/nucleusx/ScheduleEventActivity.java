package com.example.nucleusx;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.TemporaryDeviceInfo;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.CalendarEvent;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScheduleEventActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "ScheduleEventActivity";

    TextView deviceNameHeader; //schDeviceName
    Button pickDateButton;
    Button pickTimeButton;
    TextView dateValue;
    TextView timeValue;
    CheckBox repeatCheckbox;
    TextView repeatTitle; //schRepeatTitle
    LinearLayout repeatSelection; //schRepeatSelection;
    LinearLayout repeatRepeatDays; //schRepeatDays

    TextView stateValue; //schStateValue
    Switch stateSwitch; //schSwitch
    SeekBar stateSeekbar;

    //Reset timezone to GMT to avoid interference of local device's timezone
    Calendar selected = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    ControlItem incomingParcel;
    HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_event);

        setTitle("Schedule an Event");

        deviceNameHeader = (TextView) findViewById(R.id.schDeviceName);
        dateValue = (TextView) findViewById(R.id.schEventPickDateValue);
        timeValue = (TextView) findViewById(R.id.schEventPickTimeValue);

        //Get current date according to this device's timezone
        Calendar localTime = Calendar.getInstance();
        //onDateSet(null, selected.get(Calendar.YEAR), selected.get(Calendar.MONTH), selected.get(Calendar.DAY_OF_MONTH));
        onDateSet(null, localTime.get(Calendar.YEAR), localTime.get(Calendar.MONTH), localTime.get(Calendar.DAY_OF_MONTH));

        pickDateButton = (Button) findViewById(R.id.schEventPickDateButton);
        pickDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });

        pickTimeButton = (Button) findViewById(R.id.schEventPickTimeButton);
        pickTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "Time Picker");
            }
        });

        repeatSelection = findViewById(R.id.schRepeatSelection);
        repeatSelection.setVisibility(LinearLayout.GONE);
        repeatRepeatDays = findViewById(R.id.schRepeatDays);
        repeatRepeatDays.setVisibility(LinearLayout.GONE);
        stateValue = (TextView) findViewById(R.id.schStateValue);
        repeatTitle = (TextView) findViewById(R.id.schRepeatTitle);

        repeatCheckbox = (CheckBox) findViewById(R.id.schRepeatCheckBox);
        repeatCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repeatCheckbox.isChecked()) {
                    repeatTitle.setText("Event will repeat on every checked day");
                    repeatSelection.setVisibility(LinearLayout.VISIBLE);
                    repeatRepeatDays.setVisibility(LinearLayout.VISIBLE);
                } else {
                    repeatTitle.setText("Repeat this event");
                    repeatSelection.setVisibility(LinearLayout.GONE);
                    repeatRepeatDays.setVisibility(LinearLayout.GONE);
                }
            }
        });

        stateSwitch = (Switch) findViewById(R.id.schSwitch);
        stateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    stateValue.setText("ON");
                } else {
                    stateValue.setText("OFF");
                }
            }
        });

        stateSeekbar = findViewById(R.id.schSeekBar);
        stateSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) {
                    stateValue.setText("OFF");
                } else {
                    stateValue.setText(progress + "%");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //stateValue.setTextColor(000);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (stateSwitch.isChecked()) {
            stateValue.setText("ON");
        } else {
            stateValue.setText("OFF");
        }

        Menu m = findViewById(R.id.menuActionSave);

        // To get data from Device Settings Activity
        Intent intent = getIntent();
        incomingParcel = intent.getParcelableExtra("Device Info Parcel");
        if (incomingParcel != null) {
            populateSchedulerWithExisting(incomingParcel);
        }

    }

    private void populateSchedulerWithExisting(ControlItem incomingParcel) {
        String name = hardwareDescriptors.sanitzeDeviceNameForDevice(incomingParcel.getDeviceInfo().getN(), incomingParcel.getDeviceInfo().getD());
        deviceNameHeader.setText(name);

        int mode = hardwareDescriptors.getDeviceModeStringResourceId(incomingParcel.getDeviceInfo().getC());
        switch (mode) {
            case R.string.device_mode_output_digital:
                stateSwitch.setVisibility(View.VISIBLE);
                stateSeekbar.setVisibility(View.GONE);
                break;

            case R.string.device_mode_output_pwm:
                stateSwitch.setVisibility(View.GONE);
                stateSeekbar.setVisibility(View.VISIBLE);
                break;

            default:
                stateValue.setText("Not configurable");
                stateSwitch.setVisibility(View.GONE);
                stateSeekbar.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuActionSave:
                createRequest();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createRequest() {

        /*
        Request: /set/calendarEvent/i=<>,d=<>,t=<>,s=<>,r=<>
         */
        //Validation
        if (timeValue.getText().toString().isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content), "Please provide time", Snackbar.LENGTH_LONG).show();
            return;
        }

        // To compare the time against the current, local time we need an instance of Calendar object
        // in local time-zone.
        Calendar current = Calendar.getInstance();

        // Get Time
        //Lets reset the seconds - first for selected object
        selected.set(Calendar.SECOND, 0);
        selected.set(Calendar.MILLISECOND, 0);
        //and then for current object
        current.set(Calendar.SECOND, 0);
        current.set(Calendar.MILLISECOND, 0);

        /*
           current.getTimeInMillis() returns milliseconds from epoch adjusted for GMT.
           So, to get it back to current device's timezone, we need to correct for it.
           The ZONE_OFFSET it +ve for Timezones east of GMT and -ve for timezones west of GMT
           So, we simply add the ZONE_OFFSET to current.getTimeInMillis() to get current time in ms
             for the timezone the device is in. this is the true comparator to selected.getTimeInMs()
        */
        long currentMs = current.getTimeInMillis() + current.get(Calendar.ZONE_OFFSET);

        StringBuilder serviceBundlerSb = new StringBuilder();
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("incomingParcel.getDeviceName(): " + incomingParcel.getDeviceName());
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("incomingParcel.getDeviceInfo().toString()" + incomingParcel.getDeviceInfo().toString());

        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("Selected Time");
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.YEAR): " + selected.get(Calendar.YEAR));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.MONTH): " + selected.get(Calendar.MONTH));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.DAY_OF_MONTH): " + selected.get(Calendar.DAY_OF_MONTH));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.HOUR_OF_DAY): " + selected.get(Calendar.HOUR_OF_DAY));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.MINUTE): " + selected.get(Calendar.MINUTE));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.SECOND): " + selected.get(Calendar.SECOND));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.MILLISECOND): " + selected.get(Calendar.MILLISECOND));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.get(Calendar.ZONE_OFFSET): " + selected.get(Calendar.ZONE_OFFSET));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("selected.getTimeInMillis(): " + selected.getTimeInMillis());

        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("\nCurrent Time");
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.YEAR): " + current.get(Calendar.YEAR));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.MONTH): " + current.get(Calendar.MONTH));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.DAY_OF_MONTH): " + current.get(Calendar.DAY_OF_MONTH));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.HOUR_OF_DAY): " + current.get(Calendar.HOUR_OF_DAY));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.MINUTE): " + current.get(Calendar.MINUTE));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.SECOND): " + current.get(Calendar.SECOND));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.MILLISECOND): " + current.get(Calendar.MILLISECOND));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.get(Calendar.ZONE_OFFSET): " + current.get(Calendar.ZONE_OFFSET));
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("current.getTimeInMillis(): " + current.getTimeInMillis());
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("ACTUAL COMPARATOR: currentMs: " + currentMs);

        Log.d(TAG, "selected: " + selected.getTimeInMillis() + " current: " + currentMs);


        int repeatMask = 0;
        if (repeatCheckbox.isChecked()) {
            CheckBox cbSun = findViewById(R.id.schRepeatSunCb);
            CheckBox cbMon = findViewById(R.id.schRepeatMonCb);
            CheckBox cbTue = findViewById(R.id.schRepeatTueCb);
            CheckBox cbWed = findViewById(R.id.schRepeatWedCb);
            CheckBox cbThu = findViewById(R.id.schRepeatThuCb);
            CheckBox cbFri = findViewById(R.id.schRepeatFriCb);
            CheckBox cbSat = findViewById(R.id.schRepeatSatCb);

            repeatMask = hardwareDescriptors.computeRepeatMask(
                    cbSun.isChecked(),
                    cbMon.isChecked(),
                    cbTue.isChecked(),
                    cbWed.isChecked(),
                    cbThu.isChecked(),
                    cbFri.isChecked(),
                    cbSat.isChecked()
            );

            serviceBundlerSb.append("\n");
            serviceBundlerSb.append("\nComputed repeat mask: " + repeatMask);
        }

        // Validation
        serviceBundlerSb.append("\n");
        serviceBundlerSb.append("DIFFERENCE: Selected - current: " + (selected.getTimeInMillis() - currentMs));

        if (selected.getTimeInMillis() < currentMs) {
            serviceBundlerSb.append("\n");
            serviceBundlerSb.append("Selected time is in past");
            raiseSnackbarAndSendDiagnostics("Selected time is in past", serviceBundlerSb.toString());
            return;
        }

        int s=0;
        if (stateSwitch.getVisibility() == View.VISIBLE) {
            s = hardwareDescriptors.getDigitalStateFromBoolean(stateSwitch.isChecked());
        } else if (stateSeekbar.getVisibility() == View.VISIBLE) {
            s = hardwareDescriptors.getHardwareStateFromSoftwareSlider(stateSeekbar.getProgress(), stateSeekbar.getMax());
        }
        else {
            Snackbar.make(findViewById(android.R.id.content), "State of this device cannot be scheduled", Snackbar.LENGTH_LONG).show();
            return;
        }

        long uts = selected.getTimeInMillis() / 1000L;
        // This uts is for GMT+00:00 timezone - so we need to adjust it to ESP's time zone
        // At this moment, we don't have access to z-value in the incoming Parcel.
        // So we will interpret ESP's timezone from the time it reports
        // uts += hardwareDescriptors.getHardwareTimeZoneSecondsFromReportedTimeString(incomingParcel.getDeviceInfo().getTime());
        Log.d(TAG, "uts: " + uts);

        /*Snackbar.make(findViewById(android.R.id.content), "/set/calendarEvents?" +
                "d=" + incomingParcel.getDeviceInfo().getD() +
                "&i=" + incomingParcel.getDeviceInfo().getNode() +
                "&r=" + repeatMask +
                "&s=" + s +
                "&t=" + uts, Snackbar.LENGTH_LONG).show();*/
        String requestStr = "Parameters: /set/calendarEvents?" +
                "d=" + incomingParcel.getDeviceInfo().getD() +
                "&i=" + incomingParcel.getDeviceInfo().getNode() +
                "&r=" + repeatMask +
                "&s=" + s +
                "&t=" + uts;
        Log.d(TAG, "Parameters: /set/calendarEvents?" +
                "d=" + incomingParcel.getDeviceInfo().getD() +
                "&i=" + incomingParcel.getDeviceInfo().getNode() +
                "&r=" + repeatMask +
                "&s=" + s +
                "&t=" + uts);

        serviceBundlerSb.append("\n");
        serviceBundlerSb.append(requestStr);
        serviceBundlerSb.append("\n");

        performSetEventRequest(s,uts,repeatMask);

        raiseSnackbarAndSendDiagnostics("Request sent", serviceBundlerSb.toString());
    }

    public void raiseSnackbarAndSendDiagnostics(String snackbarMessage, final String body){
        CoordinatorLayout coordinatorLayout = findViewById(R.id.scheduleEventCoordinatorLayout);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, snackbarMessage, Snackbar.LENGTH_LONG)
                .setAction("Report to Pratik", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onEmailRunner(body);
                    }
                });
        snackbar.show();
        Log.d(TAG, body);
    }

    public void onEmailRunner(String body) {

        //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).show();

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","pp423@cornell.edu", null));

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Nucleus Node Diagnostic Data");
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);

        startActivity(Intent.createChooser(emailIntent, "Send nucleus node diagnostics via email"));
    }

    public void performSetEventRequest(final int s, final long t, final int r){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+incomingParcel.getDeviceInfo().getIp()+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjPostCalendarEventAddition(
                incomingParcel.getDeviceInfo().getD(),
                s,
                t,
                r,
                incomingParcel.getDeviceInfo().getNode()
        );
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }
                //Replace device info
                //incomingParcel.setDeviceInfo(response.body());
                copyDeviceInfoIntoItem(incomingParcel.getDeviceInfo(), response.body());

                for(CalendarEvent ce : incomingParcel.getDeviceInfo().getCalendarEvents()){
                    if(ce.getT() == t && ce.getS() == s){
                        Toast.makeText(ScheduleEventActivity.this, "Event Scheduled!", Toast.LENGTH_SHORT).show();
                        finish();
                        return;
                    }
                }

                // If here, request failed for some reason
                Toast.makeText(ScheduleEventActivity.this, "Failure! How many existing events are there?", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void copyDeviceInfoIntoItem(TemporaryDeviceInfo temporaryDeviceInfo, NodeResponseObjDeviceInfo deviceInfo) {
        temporaryDeviceInfo.setCalendarEvents(deviceInfo.getCalendarEvents());
        temporaryDeviceInfo.setNode(deviceInfo.getNode());
        temporaryDeviceInfo.setTime(deviceInfo.getTime());
        temporaryDeviceInfo.setUTS(deviceInfo.getUTS());
        temporaryDeviceInfo.setIp(deviceInfo.getIp());
        temporaryDeviceInfo.setN(deviceInfo.getN());
        temporaryDeviceInfo.setC(deviceInfo.getC());
        temporaryDeviceInfo.setD(deviceInfo.getD());
        temporaryDeviceInfo.setG(deviceInfo.getG());
        temporaryDeviceInfo.setM(deviceInfo.getM());
        temporaryDeviceInfo.setS(deviceInfo.getS());
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Need to set this to selected (timezone = GMT) since this is where the UTS will
        // ultimately computed from
        selected.set(Calendar.YEAR, year);
        selected.set(Calendar.MONTH, month);
        selected.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        // Get LocalTime instance for display purpose
        Calendar localTime = Calendar.getInstance();
        SimpleDateFormat sdf2;
        if(localTime.get(Calendar.YEAR) != year){
            sdf2 = new SimpleDateFormat("EEEE, MMM dd, yyyy");
        }
        else {
            sdf2  = new SimpleDateFormat("EEEE, MMMM dd");
        }
        localTime.set(Calendar.YEAR, year);
        localTime.set(Calendar.MONTH, month);
        localTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Log.d(TAG, "localTime EEE, MMM dd: " + sdf2.format(localTime.getTime()));

        //dateValue.setText(currentDateString);
        dateValue.setText(sdf2.format(localTime.getTime()));
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        selected.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selected.set(Calendar.MINUTE, minute);
        //String currentTimeString = DateFormat.getTimeInstance().format(selected.getTime());
        StringBuilder sb = new StringBuilder();
        StringBuilder sbAmPm = new StringBuilder();

        if(hourOfDay == 0){
            sb.append("12");
            sbAmPm.append(" AM");
        }
        else if(hourOfDay > 0 && hourOfDay < 10){
            sb.append("0").append(hourOfDay);
            sbAmPm.append(" AM");
        }
        else if(hourOfDay >= 10 && hourOfDay < 12){
            sb.append(hourOfDay);
            sbAmPm.append(" AM");
        }
        else if(hourOfDay == 12){
            sb.append(hourOfDay);
            sbAmPm.append(" PM");
        }
        else if(hourOfDay > 12 && hourOfDay < 22){
            sb.append("0").append(hourOfDay - 12);
            sbAmPm.append(" PM");
        }
        else if(hourOfDay >= 22 && hourOfDay <= 23){
            sb.append(hourOfDay - 12);
            sbAmPm.append(" PM");
        }


        sb.append(":");

        if(minute < 10){
            sb.append("0");
        }
        sb.append(minute).append(sbAmPm);

        timeValue.setText(sb.toString());
    }
}
