package com.example.nucleusx;

public class RequestListElement {

    private String requestType;
    private Boolean requestProcessed;

    public String getRequestType() {
        return requestType;
    }

    public Boolean getRequestProcessed() {
        return requestProcessed;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public void setRequestProcessed(Boolean requestProcessed) {
        this.requestProcessed = requestProcessed;
    }

    public RequestListElement(String requestType, Boolean requestProcessed) {
        this.requestType = requestType;
        this.requestProcessed = requestProcessed;
    }
}
