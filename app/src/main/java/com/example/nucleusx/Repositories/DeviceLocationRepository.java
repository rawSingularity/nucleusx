package com.example.nucleusx.Repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.nucleusx.DataAccessObjects.DeviceLocationDao;
import com.example.nucleusx.Database.PrimaryDatabase;
import com.example.nucleusx.DatabaseEntities.DeviceLocation;

import java.util.Date;
import java.util.List;

public class DeviceLocationRepository {

    private DeviceLocationDao deviceLocationDao;
    private LiveData<List<DeviceLocation>> allDeviceLocations;

    public DeviceLocationRepository(Application application) {
        PrimaryDatabase database = PrimaryDatabase.getInstance(application);
        deviceLocationDao = database.deviceLocationDao();
        allDeviceLocations = deviceLocationDao.getAllDeviceLocations();
    }

    public void insert(DeviceLocation deviceLocation) {
        new InsertDeviceLocationAsyncTask(deviceLocationDao).execute(deviceLocation);
    }

    public void update(DeviceLocation deviceLocation) {
        new UpdateDeviceLocationAsyncTask(deviceLocationDao).execute(deviceLocation);
    }

    public void delete(DeviceLocation deviceLocation) {
        new DeleteDeviceLocationAsyncTask(deviceLocationDao).execute(deviceLocation);
    }

    public void deleteAllDeviceLocations() {
        new DeleteAllDeviceLocationsAsyncTask(deviceLocationDao).execute();
    }

    public LiveData<List<DeviceLocation>> getAllDeviceLocations() {
        return allDeviceLocations;
    }

    private static class InsertDeviceLocationAsyncTask extends AsyncTask<DeviceLocation, Void, Void> {
        private DeviceLocationDao deviceLocationDao;

        private InsertDeviceLocationAsyncTask(DeviceLocationDao deviceLocationDao) {
            this.deviceLocationDao = deviceLocationDao;
        }


        @Override
        protected Void doInBackground(DeviceLocation... deviceLocations) {
            deviceLocationDao.insert(deviceLocations[0]);
            return null;
        }
    }

    private static class UpdateDeviceLocationAsyncTask extends AsyncTask<DeviceLocation, Void, Void> {
        private DeviceLocationDao deviceLocationDao;

        private UpdateDeviceLocationAsyncTask(DeviceLocationDao deviceLocationDao) {
            this.deviceLocationDao = deviceLocationDao;
        }

        @Override
        protected Void doInBackground(DeviceLocation... deviceLocations) {
            deviceLocations[0].setCreated(new Date(System.currentTimeMillis()));
            deviceLocationDao.update(deviceLocations[0]);
            return null;
        }
    }

    private static class DeleteDeviceLocationAsyncTask extends AsyncTask<DeviceLocation, Void, Void> {
        private DeviceLocationDao deviceLocationDao;

        private DeleteDeviceLocationAsyncTask(DeviceLocationDao deviceLocationDao) {
            this.deviceLocationDao = deviceLocationDao;
        }

        @Override
        protected Void doInBackground(DeviceLocation... deviceLocations) {
            deviceLocationDao.delete(deviceLocations[0]);
            return null;
        }
    }

    private static class DeleteAllDeviceLocationsAsyncTask extends AsyncTask<Void, Void, Void> {
        private DeviceLocationDao deviceLocationDao;

        private DeleteAllDeviceLocationsAsyncTask(DeviceLocationDao deviceLocationDao) {
            this.deviceLocationDao = deviceLocationDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            deviceLocationDao.deleteAllDeviceLocations();
            return null;
        }
    }

}
