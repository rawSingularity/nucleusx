package com.example.nucleusx.Repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.example.nucleusx.DataAccessObjects.NodeDao;
import com.example.nucleusx.Database.PrimaryDatabase;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;
import com.example.nucleusx.R;
import com.example.nucleusx.Utilities.HardwareDescriptors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NodeRepository {

    private static  final String TAG = "NodeRepository";
    private NodeDao nodeDao;
    private LiveData<List<Node>> allNodes;
    private MutableLiveData<List<Node>> nodeItems = new MutableLiveData<List<Node>>();
    private HardwareDescriptors hd;

    public MutableLiveData<List<Node>> getSearchResults() {
        return nodeItems;
    }

    public NodeRepository(Application application){
        PrimaryDatabase database = PrimaryDatabase.getInstance(application);
        nodeDao = database.nodeDao();
        allNodes = nodeDao.getAllNodes();
        hd = new HardwareDescriptors();
    }

    public void insert(Node node){
        new InsertNodeAsyncTask(nodeDao).execute(node);
    }

    public void update(Node node){
        new UpdateNodeAsyncTask(nodeDao).execute(node);
    }

    public void updateIpForNodeId(Node node){
        new UpdateIpForNodeIdAsyncTask(nodeDao).execute(node);
    }

    public void delete(Node node){
        new DeleteNodeAsyncTask(nodeDao).execute(node);
    }

    public void deleteNodeForNodeId(int nodeId){
        new DeleteNodeForNodeIdAsncTask(nodeDao).execute(nodeId);
    }

    public void deleteUnregisteredNodes(){
        new DeleteUnregisteredNodesAsyncTask(nodeDao).execute();
    }

    public void deleteAllNodes(){
        new DeleteAllNodesAsyncTask(nodeDao).execute();
    }

    public LiveData<List<Node>> getAllNodes(){
        return allNodes;
    }

    public void getDataForNodeId(int nodeId){
        Log.d(TAG, "getDataForNodeId: " + nodeId);
        GetDataForNodeIdAsyncTask getDataForNodeIdAsyncTask = new GetDataForNodeIdAsyncTask(nodeDao);
        getDataForNodeIdAsyncTask.delegate = this;
        getDataForNodeIdAsyncTask.execute(nodeId);
    }

    private void searchResultsReady(List<Node> nodes){
        nodeItems.setValue(nodes);
    }

    private static class GetDataForNodeIdAsyncTask extends AsyncTask<Integer, Void, List<Node>> {
        private NodeDao nodeDao;
        private NodeRepository delegate = null;

        private GetDataForNodeIdAsyncTask(NodeDao nodeDao){
            Log.d(TAG, "GetDataForNodeIdAsyncTask");
            this.nodeDao = nodeDao;
        }

        @Override
        protected List<Node> doInBackground(Integer... integers) {
            Log.d(TAG, "doInBackground: " + integers[0]);
            return (nodeDao.getDataForNodeId(integers[0]));
        }

        @Override
        protected void onPostExecute(List<Node> nodes) {
            Log.d(TAG, "onPostExecute: size:" + nodes.size());
            super.onPostExecute(nodes);
            delegate.searchResultsReady(nodes);
        }
    }

    private static class InsertNodeAsyncTask extends AsyncTask<Node, Void, Void>{

        private NodeDao nodeDao;

        private InsertNodeAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Node... nodes) {
            nodeDao.insert(nodes[0]);
            return null;
        }
    }

    private static class UpdateNodeAsyncTask extends AsyncTask<Node, Void, Void>{

        private NodeDao nodeDao;

        private UpdateNodeAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Node... nodes) {
            Log.d(TAG, "UpdateNodeAsyncTask: Node:" + nodes[0].getNodeId() + " reg:" + nodes[0].getRegistered());
            nodes[0].setLastUpdated(new Date(System.currentTimeMillis()));
            nodeDao.update(nodes[0]);
            return null;
        }
    }

    private static class UpdateIpForNodeIdAsyncTask extends AsyncTask<Node, Void, Void>{

        private NodeDao nodeDao;

        private UpdateIpForNodeIdAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Node... nodes) {
            nodeDao.updateIpForNodeId(
                    nodes[0].getIp(),
                    new Date(System.currentTimeMillis()),
                    nodes[0].getNodeId());
            return null;
        }
    }

    private static class DeleteNodeAsyncTask extends AsyncTask<Node, Void, Void>{

        private NodeDao nodeDao;

        private DeleteNodeAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Node... nodes) {
            nodeDao.delete(nodes[0]);
            return null;
        }
    }

    private static class DeleteNodeForNodeIdAsncTask extends AsyncTask<Integer, Void, Void>{
        private NodeDao nodeDao;

        private DeleteNodeForNodeIdAsncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            nodeDao.deleteNodeForNodeId(integers[0]);
            return null;
        }
    }

    private static class DeleteUnregisteredNodesAsyncTask extends AsyncTask<Void, Void, Void>{

        private NodeDao nodeDao;

        private DeleteUnregisteredNodesAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            nodeDao.deleteUnregisteredNodes();
            return null;
        }
    }

    private static class DeleteAllNodesAsyncTask extends AsyncTask<Void, Void, Void>{

        private NodeDao nodeDao;

        private DeleteAllNodesAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            nodeDao.deleteAllNodes();
            return null;
        }
    }

    public void resetNode(int nodeId, String ip){
        Log.d(TAG, "setDeviceStateForIp: " + ip);

        if (!hd.validIp(ip)) {
            Log.d(TAG, "setDeviceStateForIp: Not a valid ip: " + ip + ". Aborting!");
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + ip + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.setReset(nodeId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }
                NodeResponseObjDeviceInfo deviceInfo = response.body();
                //Toast.makeText(NodeControlActivity.this, "Device " + deviceInfo.getD() + " set to " + deviceInfo.getS(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Received response: D" + deviceInfo.getD() + " S:" + deviceInfo.getS());
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });

    }

    public void rebootNode(int nodeId, String ip){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+ip+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.setReboot(nodeId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("call Failure", "response failure. code: " + response.code());
                    return;
                }
                //TODO: Verify that it actually succeeded
                //flashBeaconUi(position);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    public void requestBeacon(int nodeId, String ip){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+ip+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.setBeacon(nodeId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("call Failure", "response failure. code: " + response.code());
                    return;
                }

                Log.d(TAG, "Beacon turned on. Look for blinking light on module");
                //TODO: Verify that it actually succeeded
                //flashBeaconUi(position);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }
}
