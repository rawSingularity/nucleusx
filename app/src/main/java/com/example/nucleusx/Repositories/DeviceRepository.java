package com.example.nucleusx.Repositories;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.nucleusx.DataAccessObjects.DeviceDao;
import com.example.nucleusx.DataAccessObjects.NodeDao;
import com.example.nucleusx.Database.PrimaryDatabase;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.NodeApis.GenericGetApi;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;
import com.example.nucleusx.Utilities.HardwareDescriptors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceRepository {

    private static final String TAG = "DeviceRepository";

    private DeviceDao deviceDao;
    private NodeDao nodeDao;
    private LiveData<List<Device>> allDevices;
    private LiveData<List<Device>> allQuickAccessDevices;
    private LiveData<List<Device>> deviceInfo;

    private HardwareDescriptors hd;

    private static Device device;

    public void setDevice(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return device;
    }

    private static Device setDevice;

    public static Device getSetDevice() {
        return setDevice;
    }

    public void setDeviceInfo(LiveData<List<Device>> deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public DeviceRepository(Application application) {
        PrimaryDatabase database = PrimaryDatabase.getInstance(application);
        deviceDao = database.deviceDao();
        nodeDao = database.nodeDao();
        allDevices = deviceDao.getAllDevices();
        allQuickAccessDevices = deviceDao.getAllQuickAccessDevices();
        hd = new HardwareDescriptors();
    }


    /*
        These are the public-APIs that the DeviceRepository exposes to world.
        Eventually, all the device-related operations should happen via these.
     */
    public void insert(Device device) {
        new InsertDeviceAsyncTask(deviceDao).execute(device);
    }

    public void update(Device device) {
        new UpdateDeviceAsyncTask(deviceDao).execute(device);
    }

    public void updateDeviceRemoteInfo(Device device){
        new UpdateDeviceRemoteInfoAsyncTask(deviceDao).execute(device);
    }

    public void delete(Device device) {
        new DeleteDeviceAsyncTask(deviceDao).execute(device);
    }

    public void deleteAllDevices() {
        new DeleteAllDevicesAsyncTask(deviceDao).execute();
    }

    public void setDeviceQuickAccessPreference(Device device) {
        new SetDeviceQuickAccessPreferenceAysncTask(deviceDao).execute(device);
    }

    public void setDeviceLocation(Device device){
        new SetDeviceLocationAysncTask(deviceDao).execute(device);
    }

    public void removeDevicesFromLocation(String location){
        new RemoveDevicesFromLocationAsyncTask(deviceDao).execute(location);
    }

    public void removeDevicesForNode(int nodeId){
        new RemoveDevicesForNodeAsyncTask(deviceDao).execute(nodeId);
    }

    public void updateAllDeviceLocationIn(String oldLocation, String newLocation){
        new UpdateAllDeviceLocationInAsyncTask(deviceDao).execute(oldLocation, newLocation);
    }

    public LiveData<List<Device>> getDeviceInfo(int nodeId, int deviceId){

        GetDeviceInfoAsyncTask getDeviceInfoAsyncTask = new GetDeviceInfoAsyncTask(deviceDao);
        getDeviceInfoAsyncTask.delegate = this;
        getDeviceInfoAsyncTask.execute(nodeId, deviceId);

        if(deviceInfo == null){
            deviceInfo = new LiveData<List<Device>>() {
                @Override
                public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<List<Device>> observer) {
                    super.observe(owner, observer);
                }
            };
        }

        return deviceInfo;
    }

    public LiveData<List<Device>> getAllDevices() {
        return allDevices;
    }

    public LiveData<List<Device>> getAllQuickAccessDevices() {

        //We have already returned all the data from database - but lets also refetch from web

        /*
        List<Device> allDevicesOnDemand = deviceDao.getAllDevicesOnDemand();
        Log.d(TAG, "LiveData getter: getAllQuickAccessDevices");
        for(Device d : allDevicesOnDemand){
            Log.d(TAG, "Device: " + device.getDeviceId() + " node:" + device.getNodeId());
        }*/

        return allQuickAccessDevices;
    }

    public void refetchDeviceStatesViaMediator(List<Device> devices, List<Node> nodes) {
        new PerformDeviceQueriesAsyncTask(deviceDao, nodeDao).execute();
    }

    //Retrofit
    /*
    Here, we will implement retrofit methods to retrieve device state from device itself
    and save it to database on successfull response.
     */
    public void refetchDeviceStateFromDevice(final Device device, String ip) {
        /*
        Request looks like this: http://<ip>/get?d=<deviceId>
        Things we need to perform this action:
        1. IP of the node this device is on from database node_table
        2. deviceId
         */

//        this.device = device;
//        GetIpForNodeIdAsyncTask getIpForNodeIdAsyncTask = new GetIpForNodeIdAsyncTask(nodeDao);
//        getIpForNodeIdAsyncTask.delegate = this;
//        getIpForNodeIdAsyncTask.execute(device.getNodeId());

        Log.d(TAG, "fetchDeviceStateForIp: " + ip + " deviceId:" + device.getDeviceId());

        if (!hd.validIp(ip)) {
            Log.d(TAG, "fetchDeviceStateForIp: Not a valid ip: " + ip + ". Aborting!");
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + ip + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GenericGetApi getDeviceInfoApi = retrofit.create(GenericGetApi.class);
        Call<NodeResponseObjDeviceInfo> call = getDeviceInfoApi.getNodeResponseObjDeviceInfo(device.getDeviceId());
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }
                NodeResponseObjDeviceInfo deviceInfo = response.body();
                //Log.d(TAG, "Response: IP: " + deviceInfo.getIp() + " Device: " + deviceInfo.getD() + " current state: " + deviceInfo.getS() + " configured: " + deviceInfo.getC() + "\n");

                //Update in database
                Device updatedDevice = new Device(
                        hd.sanitizeRoomName(deviceInfo.getN()),
                        deviceInfo.getD(),
                        deviceInfo.getC(),
                        deviceInfo.getS(),
                        deviceInfo.getNode(),
                        deviceInfo.getUTS(),
                        device.getLocation(),
                        device.getQuickAccessMember()
                );

                //Timestamp
                updatedDevice.setProcessedByRepositoryTimeStamp(new Date(System.currentTimeMillis()));

                //Id
                updatedDevice.setId(device.getId());

                Log.d(TAG, "refetchDeviceStateFromDevice: Updating latest device info in database");
                Log.d(TAG, updatedDevice.toString());
                //update(updatedDevice);

                updateDeviceRemoteInfo(updatedDevice);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });

    }

    public void setDeviceState(final Device device, String ip) {
        /*this.setDevice = device;
        GetIpForNodeIdAsyncTask1 getIpForNodeIdAsyncTask = new GetIpForNodeIdAsyncTask1(nodeDao);
        getIpForNodeIdAsyncTask.delegate = this;
        getIpForNodeIdAsyncTask.execute(device.getNodeId());*/

        Log.d(TAG, "setDeviceStateForIp: " + ip);

        if (!hd.validIp(ip)) {
            Log.d(TAG, "setDeviceStateForIp: Not a valid ip: " + ip + ". Aborting!");
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + ip + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjDeviceInfo(
                device.getDeviceId(), device.getState(), device.getNodeId());

        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    return;
                }
                NodeResponseObjDeviceInfo deviceInfo = response.body();
                //Toast.makeText(NodeControlActivity.this, "Device " + deviceInfo.getD() + " set to " + deviceInfo.getS(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Received response: D" + deviceInfo.getD() + " S:" + deviceInfo.getS());

                //Update in database
                Device updatedDevice = new Device(
                        deviceInfo.getN(),
                        deviceInfo.getD(),
                        deviceInfo.getC(),
                        deviceInfo.getS(),
                        deviceInfo.getNode(),
                        deviceInfo.getUTS(),
                        device.getLocation(),
                        device.getQuickAccessMember()
                );
                updatedDevice.setId(device.getId());
                update(updatedDevice);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }


    /*
       ////////////////////////////////////////////////////////////////////////////////////////////
                ALL ASYNC TASKS ARE DOWN HERE
       ////////////////////////////////////////////////////////////////////////////////////////////
     */
    private static class InsertDeviceAsyncTask extends AsyncTask<Device, Void, Void> {

        private DeviceDao deviceDao;

        private InsertDeviceAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            deviceDao.insert(devices[0]);
            return null;
        }
    }

    private static class UpdateDeviceAsyncTask extends AsyncTask<Device, Void, Void> {

        private DeviceDao deviceDao;

        private UpdateDeviceAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            Log.d(TAG, "deviceDao.update: device: " + devices[0].getName() + " state: " + devices[0].getState());
            Log.d(TAG, devices[0].toString());
            deviceDao.update(devices[0]);
            return null;
        }
    }

    private static class UpdateDeviceRemoteInfoAsyncTask extends  AsyncTask<Device, Void, Void>{

        private DeviceDao deviceDao;

        private UpdateDeviceRemoteInfoAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            //void updateDeviceRemoteInfo(String name, int deviceConfig, int state, int uts, Date processedByRepositoryTimeStamp, int id, int deviceId, int nodeId);
            deviceDao.updateDeviceRemoteInfo(
                    devices[0].getName(),
                    devices[0].getDeviceConfig(),
                    devices[0].getState(),
                    devices[0].getUts(),
                    devices[0].getProcessedByRepositoryTimeStamp(),
                    devices[0].getId(),
                    devices[0].getDeviceId(),
                    devices[0].getNodeId()
            );
            return null;
        }
    }

    private static class DeleteDeviceAsyncTask extends AsyncTask<Device, Void, Void> {

        private DeviceDao deviceDao;

        private DeleteDeviceAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            deviceDao.delete(devices[0]);
            return null;
        }
    }

    private static class SetDeviceQuickAccessPreferenceAysncTask extends AsyncTask<Device, Void, Void> {
        private DeviceDao deviceDao;

        private SetDeviceQuickAccessPreferenceAysncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            deviceDao.setDeviceQuickAccessPreference(devices[0].getDeviceId(), devices[0].getNodeId(), devices[0].getQuickAccessMember());
            return null;
        }
    }

    private static class SetDeviceLocationAysncTask extends AsyncTask<Device, Void, Void> {
        private DeviceDao deviceDao;

        private SetDeviceLocationAysncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Device... devices) {
            deviceDao.setDeviceLocation(devices[0].getDeviceId(), devices[0].getNodeId(), devices[0].getLocation());
            return null;
        }
    }

    private static class RemoveDevicesFromLocationAsyncTask extends AsyncTask<String, Void, Void> {
        private DeviceDao deviceDao;

        private RemoveDevicesFromLocationAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            deviceDao.removeDevicesFromLocation(strings[0]);
            return null;
        }
    }

    private static class RemoveDevicesForNodeAsyncTask extends AsyncTask<Integer, Void, Void>{
        private DeviceDao deviceDao;

        private RemoveDevicesForNodeAsyncTask(DeviceDao deviceDao){
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            deviceDao.removeDevicesForNode(integers[0]);
            return null;
        }
    }

    private static class UpdateAllDeviceLocationInAsyncTask extends AsyncTask<String, Void, Void> {
        private DeviceDao deviceDao;

        private UpdateAllDeviceLocationInAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            deviceDao.updateAllDeviceLocationIn(strings[0], strings[1]);
            return null;
        }
    }



    private static class DeleteAllDevicesAsyncTask extends AsyncTask<Void, Void, Void> {

        private DeviceDao deviceDao;

        private DeleteAllDevicesAsyncTask(DeviceDao deviceDao) {
            this.deviceDao = deviceDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            deviceDao.deleteAllDevices();
            return null;
        }
    }

    private static class GetDeviceInfoAsyncTask extends AsyncTask<Integer, Void, LiveData<List<Device>>>{
        private DeviceDao deviceDao;
        private DeviceRepository delegate;

        private GetDeviceInfoAsyncTask(DeviceDao deviceDao){
            this.deviceDao = deviceDao;
        }


        @Override
        protected LiveData<List<Device>> doInBackground(Integer... integers) {
            return deviceDao.getDeviceInfo(integers[0], integers[1]);
        }

        @Override
        protected void onPostExecute(LiveData<List<Device>> deviceInfo) {
            super.onPostExecute(deviceInfo);
            delegate.setDeviceInfo(deviceInfo);
        }
    }

    private static class PerformDeviceQueriesAsyncTask extends AsyncTask<Void, Void, Void> {

        private DeviceDao deviceDao;
        private NodeDao nodeDao;

        private PerformDeviceQueriesAsyncTask(DeviceDao deviceDao, NodeDao nodeDao) {
            this.deviceDao = deviceDao;
            this.nodeDao = nodeDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<Device> allDevicesOnDemand = deviceDao.getAllDevicesOnDemand();
            Log.d(TAG, "PerformDeviceQueriesAsyncTask: allDevices");
            for (Device device : allDevicesOnDemand) {

                if(device == null){
                    continue;
                }

                String ip = nodeDao.getIpForNodeId(device.getNodeId());

                Log.d(TAG, "Device: " + device.getDeviceId() + " node:" + device.getNodeId() + " ip:" + ip);

                //Update only if the last update happened more than some time ago
                long lastProcessed = device.getProcessedByRepositoryTimeStamp().getTime();
                long current = System.currentTimeMillis();
                long difference = current - lastProcessed;

                Log.d(TAG, "getProcessedByRepositoryTimeStamp: current: " + current + " lastProcessed:" + lastProcessed + " difference:" + difference);
                if (difference < 10000) {
                    continue;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "onPostExecute: PerformDeviceQueriesAsyncTask complete!");
        }
    }

    /*private static class GetIpForNodeIdAsyncTask extends AsyncTask<Integer, Void, String> {
        private NodeDao nodeDao;
        private DeviceRepository delegate;

        private GetIpForNodeIdAsyncTask(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String ip;
            try {
                ip = nodeDao.getIpForNodeId(integers[0]);
                return ip;
            }
            catch (Exception e){
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            delegate.fetchDeviceStateForIp(s);
        }
    }

    private static class GetIpForNodeIdAsyncTask1 extends AsyncTask<Integer, Void, String> {
        private NodeDao nodeDao;
        private DeviceRepository delegate;

        private GetIpForNodeIdAsyncTask1(NodeDao nodeDao){
            this.nodeDao = nodeDao;
        }

        @Override
        protected String doInBackground(Integer... integers) {
            String ip;
            try {
                ip = nodeDao.getIpForNodeId(integers[0]);
                return ip;
            }
            catch (Exception e){
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            delegate.setDeviceStateForIp(s);
        }
    }*/
}
