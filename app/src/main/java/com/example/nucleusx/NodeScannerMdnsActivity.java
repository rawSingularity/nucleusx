package com.example.nucleusx;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.MdnsCache;
import com.example.nucleusx.ActivityParcels.MdnsQueryResultParcel;
import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.Adapters.ScannedNodeItemAdapter;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NodeScannerMdnsActivity extends AppCompatActivity {

    public static final String TAG = "NodeScannerMdnsActivity";
    ArrayList<ScannedNodeItem> scannedNodeItems = new ArrayList<>();

    private RecyclerView mRecyclerView;
    private ScannedNodeItemAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_scanner_mdns);
        setTitle("Node Search");

        // To get data from Node Scanner Activity
        Intent intent = getIntent();
        final MdnsQueryResultParcel MDNS_QUERY_RESULT_PARCEL = intent.getParcelableExtra("MdnsQueryResultParcel");

        for(MdnsCache mdnsCache : MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList()){
            if(mdnsCache.getServiceInfo() == null){
                Log.d(TAG, "mdnsCache.getServiceInfo is null");
            }

            int nodeId = mdnsCache.getNodeIdFromServiceInfo();
            String ipAddress = mdnsCache.getIpFromServiceInfo();

            scannedNodeItems.add(new ScannedNodeItem(nodeId, ipAddress, mdnsCache.getResolved().toString()));
        }

        //buildSampleData();

        mRecyclerView = findViewById(R.id.mdnsSearchResultRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ScannedNodeItemAdapter(scannedNodeItems);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ScannedNodeItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Toast.makeText(NodeScannerMdnsActivity.this, "TODO: Initiate flow to device controls", Toast.LENGTH_SHORT).show();


                ScannedNodeItem parcel = getScannedNodeItemParcel(MDNS_QUERY_RESULT_PARCEL, position);

                Intent intent = new Intent(NodeScannerMdnsActivity.this, NodeQueryActivity.class);
                intent.putExtra("Node Item", parcel);
                startActivity(intent);
            }

            @Override
            public void onConfigureClick(int position) {
                //Toast.makeText(NodeScannerMdnsActivity.this, "TODO: Initiate flow to device configuration", Toast.LENGTH_SHORT).show();

                ScannedNodeItem parcel = getScannedNodeItemParcel(MDNS_QUERY_RESULT_PARCEL, position);

                Intent intent = new Intent(NodeScannerMdnsActivity.this, NodeControlActivity.class);
                intent.putExtra("Node Item", parcel);
                startActivity(intent);
            }

            @Override
            public void onLocateClick(int position) {
                //Toast.makeText(NodeScannerMdnsActivity.this, "TODO: Initiate turn-on-beacon request", Toast.LENGTH_SHORT).show();
                String ipAddress = MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getIpFromServiceInfo();
                int nodeId = MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getNodeIdFromServiceInfo();

                requestBeacon(ipAddress, nodeId, position);
            }

            @Override
            public void onMoreInfoClick(int position) {
                ScannedNodeItem parcel = getScannedNodeItemParcel(MDNS_QUERY_RESULT_PARCEL, position);

                Intent intent = new Intent(NodeScannerMdnsActivity.this, NodeQueryActivity.class);
                intent.putExtra("Node Item", scannedNodeItems.get(position));
                startActivity(intent);
            }

            @Override
            public void onDeleteClick(int position) {
                Toast.makeText(NodeScannerMdnsActivity.this, "TODO: Initiate factory-reset and node-purge activities", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ScannedNodeItem getScannedNodeItemParcel(MdnsQueryResultParcel MDNS_QUERY_RESULT_PARCEL, int position) {
        int idx = MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getServiceInfo().getServiceName().toString().lastIndexOf("-") + 1;

        String ipAddress = MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getIpFromServiceInfo();

        ScannedNodeItem scannedNodeItem = new ScannedNodeItem(
                Integer.parseInt(MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getServiceInfo().getServiceName().toString().substring(idx)),
                ipAddress,
                MDNS_QUERY_RESULT_PARCEL.getMdnsCacheList().get(position).getResolved().toString()
        );
        return scannedNodeItem;
    }

    private void requestBeacon(String ip, int nodeId, final int position){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://"+ip+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.setBeacon(nodeId);
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("call Failure", "response failure. code: " + response.code());
                    return;
                }

                Log.d(TAG, "Beacon turned on. Look for blinking light on module");

                CoordinatorLayout coordinatorLayout = findViewById(R.id.nodeScannerMdnsCoordinatorLayout);
                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Beacon turned on. Look for flashing light on device. It will turn off in 20 seconds", Snackbar.LENGTH_LONG);
                snackbar.show();
                //Verify that it actually succceeded
                //flashBeaconUi(position);
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void flashBeaconUi(final int position){
        new CountDownTimer(20000, 100) {
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "onTick..");
                if(position % 200 == 0){
                    //Off
                    //scannedNodeItems.get(position).set
                }
                else {
                    //On
                }

            }

            public void onFinish() {
                Log.d(TAG, "done ..");

            }
        }.start();
    }

    private void buildSampleData(){
        scannedNodeItems.add(new ScannedNodeItem(8475938, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(362456, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(5736745, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(362456, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(5736745, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(362456, "string 1", "string 2"));
        scannedNodeItems.add(new ScannedNodeItem(5736745, "string 1", "string 2"));
    }
}
