package com.example.nucleusx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DeveloperHomeActivity extends AppCompatActivity {

    private Button mMdnsNodeScanner;
    private Button mLegacyNodeScannerButton;
    private Button mNodeQueryButton;
    private Button mNodeControlButton;
    private Button mNodeDbSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer_home);
        setTitle("Developer's Home Page");

        mMdnsNodeScanner = findViewById(R.id.dhMdnsNodeScannerButton);
        mLegacyNodeScannerButton = findViewById(R.id.dhLegacyNodeScannerButton);
        mNodeQueryButton = findViewById(R.id.dhNodeQueryButton);
        mNodeControlButton = findViewById(R.id.dhNodeControlButton);
        mNodeDbSearchButton = findViewById(R.id.dhNodeDbSearchButton);

        mMdnsNodeScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickMdnsSearch();
            }
        });

        mLegacyNodeScannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLegacyNodeScannerButton();
            }
        });

        mNodeQueryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNodeQueryButton();
            }
        });

        mNodeControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNodeControlButton();
            }
        });

        mNodeDbSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNodeDbSearch();
            }
        });
    }

    private void onClickNodeDbSearch(){
        Intent intent = new Intent(this, DeveloperNodeIdDatabaseSearchActivity.class);
        startIntent(intent);
    }

    private void onClickMdnsSearch(){
        Intent intent = new Intent(this, DeveloperMdnsSearchActivity.class);
        startIntent(intent);
    }

    private void onClickNodeControlButton(){
        Intent intent = new Intent(this,    NodeControlActivity.class);
        startIntent(intent);
    }

    private void onClickNodeQueryButton() {
        Intent intent = new Intent(this, NodeQueryActivity.class);
        startIntent(intent);
    }

    private void onClickLegacyNodeScannerButton(){
        Intent intent = new Intent(this, NodeScannerActivity.class);
        startIntent(intent);
    }

    private void startIntent(Intent i){
        if(i!=null){
            startActivity(i);
        }
    }
}
