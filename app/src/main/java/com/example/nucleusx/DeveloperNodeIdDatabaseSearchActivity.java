package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.util.List;

public class DeveloperNodeIdDatabaseSearchActivity extends AppCompatActivity {

    private static final String TAG = "DevNodeIdDbSearchAct";

    //Database
    private NodeViewModel nodeViewModel;

    //Database emulation (dev)
    private EditText mdevNodeSearchField;
    private Button mdevNodeSearchButton;
    private TextView mdevNodeSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer_node_id_database_search);
        setTitle("Node Id - Database search");

        mdevNodeSearchField = findViewById(R.id.devNodeSearchField);
        mdevNodeSearchButton = findViewById(R.id.devNodeSearchButton);
        mdevNodeSearchResult = findViewById(R.id.devNodeSearchResult);

        nodeViewModel = ViewModelProviders.of(this).get(NodeViewModel.class);
        databaseDataObserverSetup();

        mdevNodeSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nodeViewModel.findDataForNodeId(Integer.parseInt(String.valueOf(mdevNodeSearchField.getText())));
            }
        });
    }

    private void databaseDataObserverSetup() {

        nodeViewModel.getSearchResults().observe(this, new Observer<List<Node>>() {
            @Override
            public void onChanged(@Nullable List<Node> nodes) {
                StringBuilder sb = new StringBuilder();
                sb.append("Search result size: " + nodes.size());

                for(Node node : nodes){
                    sb.append("\nFound: " + node.getNodeId() + " at " + node.getIp());
                    sb.append("\nRegistered?: " + node.getRegistered().toString());
                    sb.append("\nFirmware build: " + node.getFirmwareBuildDate());
                    sb.append("\nRegistered on: " + node.getRegisteredOn());
                    sb.append("\nRegistered to: " + node.getRegisteredTo());
                    sb.append("\nLast updated on: " + node.getLastUpdated());
                    sb.append("\n");
                }

                Log.d(TAG, sb.toString());
                mdevNodeSearchResult.setText("");
                mdevNodeSearchResult.setText(sb.toString());
            }
        });
    }
}
