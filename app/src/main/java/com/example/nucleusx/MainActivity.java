package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcel;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.DeviceListParcel;
import com.example.nucleusx.ActivityParcels.MdnsCache;
import com.example.nucleusx.ActivityParcels.MdnsQueryResultParcel;
import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.Adapters.QuickAccessDeviceAdapter;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.Utilities.MdnsServiceQuery;
import com.example.nucleusx.ViewModels.DeviceViewModel;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.net.nsd.NsdManager.FAILURE_ALREADY_ACTIVE;
import static android.net.nsd.NsdManager.FAILURE_INTERNAL_ERROR;
import static android.net.nsd.NsdManager.FAILURE_MAX_LIMIT;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "MainActivity";
    private HardwareDescriptors hd;

    // for all services
    //private static final String SERVICE_TYPE = "_services._dns-sd._udp";
    FloatingActionButton refetchDeviceStatesButton;
    private boolean refetchButtonHasBeenManuallyHidden;
    private RelativeLayout idleHomeRelativeLayout; // = findViewById(R.id.homeActivityEmptyRelativieLayout);
    private boolean idleHomeScreenShown;

    // = new ArrayList<Node>();

    private TextView mhaManageNodesButton;

    private Boolean swipeToRemoveEnabled;

    //QuickAccess
    private DeviceViewModel deviceViewModel;
    RecyclerView recyclerView;
    QuickAccessDeviceAdapter quickAccessDeviceAdapter;
    ItemTouchHelper itemTouchHelper;
    private List<Device> quickAccessDeviceList;

    MdnsServiceQuery mdnsServiceQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        idleHomeRelativeLayout = findViewById(R.id.homeActivityEmptyRelativieLayout);
        recyclerView = findViewById(R.id.homeActivityRecyclerView);
        refetchDeviceStatesButton = (FloatingActionButton) findViewById(R.id.fab);
        mhaManageNodesButton = findViewById(R.id.haManageNodesButton);
        showIdleHomeScreen();

        setTitle("Quick Access Devices");

        hd = new HardwareDescriptors(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //databaseDataObserverSetup();

        /*
        // Enable to perform long tasks on UI thread
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        */

        mdnsServiceQuery = MdnsServiceQuery.getInstance(this);
        mdnsServiceQuery.searchForServices();

        swipeToRemoveEnabled = false;
        refetchButtonHasBeenManuallyHidden = false;

        mhaManageNodesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnotherActivity(new Intent(MainActivity.this, ManageNodesActivity.class));
            }
        });

        //QuickAccess

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        quickAccessDeviceAdapter = new QuickAccessDeviceAdapter();
        recyclerView.setAdapter(quickAccessDeviceAdapter);

        deviceViewModel = ViewModelProviders.of(this).get(DeviceViewModel.class);
        deviceViewModel.getAllQuickAccessDevices().observe(this, new Observer<List<Device>>() {
            @Override
            public void onChanged(@Nullable List<Device> devices) {
                //Toast.makeText(MainActivity.this, "Got all quick access devices", Toast.LENGTH_SHORT).show();

                //First,
                if (devices.size() > 0) {
                    Log.d(TAG, "devicesSize > 0: size:" + devices.size());
                    setupDelayedHomeDisappearance();
                    mhaManageNodesButton.setVisibility(View.GONE);
                } else {
                    Log.d(TAG, "devicesSize == 0: size:" + devices.size());
                    showIdleHomeScreen();
                    mhaManageNodesButton.setVisibility(View.VISIBLE);
                }

                //quickAccessDeviceAdapter.setQuickAccessDevices(devices);
                quickAccessDeviceAdapter.submitList(devices);

                //Store a local copy for on-demand actions
                quickAccessDeviceList = devices;
            }
        });

        ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                Log.d(TAG, "ItemTouchHelper: onSwiped");
                Device device = quickAccessDeviceAdapter.getDeviceAtPosition(viewHolder.getAdapterPosition());
                Device candidate = new Device(
                        device.getName(),
                        device.getDeviceId(),
                        device.getDeviceConfig(),
                        device.getState(),
                        device.getNodeId(),
                        device.getUts(),
                        device.getLocation(),
                        false
                );
                Log.d(TAG, device.toString());
                //
                deviceViewModel.setDeviceQuickAccessPreference(candidate);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            /*
            @Override
            public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                //return super.getMovementFlags(recyclerView, viewHolder);
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }
            */
        };
        itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallBack);

        quickAccessDeviceAdapter.setOnItemClickListener(new QuickAccessDeviceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Prepare Parcel to flow into NodeControlActivity but to display only this device
                Log.d(TAG, "Item position: " + position);
                // Flow into next Activity

                //List<Integer> parcel = new ArrayList<Integer>();
                Device qaDevice = quickAccessDeviceAdapter.getDeviceAtPosition(position);
                //parcel.add(hd.getCombinedNodeDeviceId(qaDevice.getNodeId(), qaDevice.getDeviceId()));

                DeviceListParcel deviceListParcel = new DeviceListParcel(qaDevice.getName());
                deviceListParcel.getDeviceList().add(new com.example.nucleusx.ActivityParcels.Device(qaDevice.getDeviceId(), qaDevice.getNodeId()));


                Intent intent = new Intent(MainActivity.this, ManageDeviceActivity.class);
                //intent.putParcelableArrayListExtra("Device List", (ArrayList)parcel);
                intent.putExtra("Device List", deviceListParcel);
                startActivity(intent);
            }

            @Override
            public void onSwitchChange(int position, boolean isChecked) {
                int state = hd.getDigitalStateFromBoolean(isChecked);
                Log.d(TAG, "onSwitchChange: position: " + position + " state:" + state);

                Device device = quickAccessDeviceAdapter.getDeviceAtPosition(position);
                Device candidate = new Device(device);
                candidate.setState(state);
                deviceViewModel.setDeviceState(candidate, hd.getIpForNodeId(device.getNodeId()));

                if(swipeToRemoveEnabled){
                    disableSwipeToRemove();
                }
            }

            @Override
            public void onSeekbarChange(int position, int progress, boolean fromUser) {
                if (fromUser) {
                    Device device = quickAccessDeviceAdapter.getDeviceAtPosition(position);
                    Device candidate = new Device(device);
                    candidate.setState(progress);
                    deviceViewModel.setDeviceState(candidate, hd.getIpForNodeId(device.getNodeId()));

                    if(swipeToRemoveEnabled){
                        disableSwipeToRemove();
                    }
                }
            }

            @Override
            public void onStatusIconClick(int position) {
                Device device = quickAccessDeviceAdapter.getDeviceAtPosition(position);
                deviceViewModel.refetchDeviceStateFromDevice(device, hd.getIpForNodeId(device.getNodeId()));
            }
        });

        refetchDeviceStatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performQuickAccessDeviceQuery();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if(!swipeToRemoveEnabled) {
                        setupDelayedButtonShow();
                    }
                }
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && refetchDeviceStatesButton.isShown())
                    refetchDeviceStatesButton.hide();
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        setupDelayedDeviceRefetch();
    }

    private void setupDelayedHomeDisappearance(){

        new CountDownTimer(2500, 2500){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                hideIdleHomeScreen();
                recyclerView.setBackgroundColor(Color.parseColor("#000000"));
            }
        }.start();
    }

    private void showIdleHomeScreen(){
        idleHomeRelativeLayout.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        refetchDeviceStatesButton.hide();
        getSupportActionBar().hide();
        idleHomeScreenShown = true;

    }
    private void hideIdleHomeScreen(){
        idleHomeRelativeLayout.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        refetchDeviceStatesButton.show();
        getSupportActionBar().show();
        idleHomeScreenShown = false;
    }

    private void setupDelayedButtonShow(){
        new CountDownTimer(2500, 2500) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Log.d(TAG, "setupDelayedButtonShow");
                if(!refetchButtonHasBeenManuallyHidden && !idleHomeScreenShown) {
                    refetchDeviceStatesButton.show();
                }
            }
        }.start();
    }

    private void setupDelayedDeviceRefetch(){
        new CountDownTimer(400, 400) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Log.d(TAG, "setupDelayedDeviceRefetch: refetching states");
                performQuickAccessDeviceQuery();
            }
        }.start();
    }

    private void setupDeviceStateRefetchMechanism() {
        new CountDownTimer(3000, 3000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Log.d(TAG, "setupDeviceStateRefetchMechanism: refetching states");
                performQuickAccessDeviceQuery();

                Log.d(TAG, "setupDeviceStateRefetchMechanism: setting up next");
                setupDeviceStateRefetchMechanism();
            }
        }.start();
    }

    private void performQuickAccessDeviceQuery(){
        if(quickAccessDeviceList == null){
            return;
        }

        for(Device d : quickAccessDeviceList){
            // Change iconography
            // quickAccessDeviceAdapter.submitList(devices);

            //Setting UTS to 0 signifies adapter to change iconography to 'in progress'
            d.setUts(0);
            quickAccessDeviceAdapter.notifyItemChanged(quickAccessDeviceList.indexOf(d));

            // Fire the request
            Log.d(TAG, "performQuickAccessDeviceQuery for device: " + d.getName() + " IP:" + hd.getIpForNodeId(d.getNodeId()));
            deviceViewModel.refetchDeviceStateFromDevice(d, hd.getIpForNodeId(d.getNodeId()));
        }

        //deviceViewModel.refetchDeviceStatesViaMediator(null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mdnsServiceQuery.searchForServices();

        disableSwipeToRemove();
        //initiateServiceDiscovery();

        //performQuickAccessDeviceQuery();
        setupDelayedDeviceRefetch();

        //setupDelayedHomeDisappearance();
    }

    /*public void showNodeSearchSnackbar(int size) {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.mainActivityCoordinatorLayout);
        StringBuilder sb = new StringBuilder();

        sb.append(size);
        sb.append(" unregistered node");
        if (size > 1) {
            sb.append("s");
        }
        sb.append(" found on the network");

        snackbarLastRaisedForTotalServices = size;

        Snackbar snackbar = Snackbar.make(coordinatorLayout, sb.toString(), Snackbar.LENGTH_INDEFINITE)
                .setAction("MANAGE", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, ManageNodesActivity.class);
                        startActivity(intent);

                    }
                });
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                Log.d(TAG, "Snackbar dismissed");
                snackbarShown = false;
            }

            @Override
            public void onShown(Snackbar sb) {
                super.onShown(sb);
                Log.d(TAG, "Snackbar shown");
                snackbarShown = true;
            }
        });
        snackbar.show();
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void disableSwipeToRemove(){

        if(swipeToRemoveEnabled){
            showGenericToast("Swipe-to-remove disabled!");
            itemTouchHelper.attachToRecyclerView(null);
            setupDelayedButtonShow();
            swipeToRemoveEnabled = false;
            //Change background of recyclerView
            recyclerView.setBackgroundColor(Color.parseColor("#000000"));
            quickAccessDeviceAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_enable_swipe_to_remove_quick_access_device:
                itemTouchHelper.attachToRecyclerView(recyclerView);
                showGenericToast("Swipe out devices to remove from quick-access home screen");

                final Animation animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
                CardView cv = (CardView) findViewById(R.id.cqaCard);
                cv.startAnimation(animShake);
                refetchDeviceStatesButton.hide();
                swipeToRemoveEnabled = true;

                //Change background of recyclerView
                recyclerView.setBackgroundColor(Color.parseColor("#00f6ff"));
                quickAccessDeviceAdapter.notifyDataSetChanged();

                return true;

            case R.id.action_disable_swipe_to_remove_quick_access_device:
                disableSwipeToRemove();
                return true;

            case R.id.action_scan_for_nodes:
                //searchRequestedByUser = true;
                //initiateServiceDiscovery();
                return true;

            case R.id.action_toggle_fab_visibility:
                if(refetchButtonHasBeenManuallyHidden){
                    refetchButtonHasBeenManuallyHidden = false;
                    refetchDeviceStatesButton.show();
                }
                else {
                    refetchButtonHasBeenManuallyHidden = true;
                    refetchDeviceStatesButton.hide();
                }
                return true;

            case R.id.main_activity_my_home_menu:

                if(recyclerView.getVisibility() == View.VISIBLE){
                    Log.d(TAG, "recyclerView is VISIBLE");
                    showIdleHomeScreen();
                    if(quickAccessDeviceList == null || quickAccessDeviceList.size()==0){
                        mhaManageNodesButton.setVisibility(View.VISIBLE);
                    }
                    else {
                        mhaManageNodesButton.setVisibility(View.GONE);
                    }
                    setupDelayedHomeDisappearance();
                }
                else {
                    Log.d(TAG, "recyclerView is GONE");
                    hideIdleHomeScreen();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showGenericToast(String message){
        Toast toast = Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 20);
        toast.show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dev_login) {
            startAnotherActivity(new Intent(this, DeveloperLoginActivity.class));
        } else if (id == R.id.nav_manage_nodes) {
            startAnotherActivity(new Intent(this, ManageNodesActivity.class));
        }
        else if(id == R.id.nav_my_rooms){
            startAnotherActivity(new Intent(this, RoomsActivity.class));
        }
        else if(id == R.id.nav_my_devices){
            prepareMyDevicesList();
        }
        // Developer's sandbox
        /*else if(id == R.id.nav_mockup_control){
            intent = new Intent(this, DeveloperMdnsSearchActivity.class);
        }
        else if(id == R.id.nav_mockup_setting){
            intent = new Intent(this, DeviceSettingsActivity.class);
        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startAnotherActivity(Intent intent) {
        if (intent == null) {
            return;
        }
        startActivity(intent);
    }

    private void prepareMyDevicesList(){
        DeviceListParcel deviceListParcel = new DeviceListParcel("Manage All Devices");

        List<Device> allDevices = hd.getAllDevices();

        if(allDevices == null){
            Log.d(TAG, "allDevices is still null");
            return;
        }

        deviceListParcel.setDeviceList(hd.getListOfAllDeviceParcels());

        if(deviceListParcel.getDeviceList() == null || deviceListParcel.getDeviceList().size() == 0){
            Toast.makeText(MainActivity.this, "There are no devices to show", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(MainActivity.this, ManageDeviceActivity.class);
        intent.putExtra("Device List", deviceListParcel);
        startActivity(intent);
    }
}
