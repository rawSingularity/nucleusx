package com.example.nucleusx;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.ref.WeakReference;
import java.net.InetAddress;

/*
public class NodeScanner extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "nodeScanner";

    private WeakReference<Context> mContextRef;

    private boolean blMyAsyncTask;
    private boolean cancelTask;

    public void NetworkSniffTask(Context context) {
        Log.d(TAG, "Method invoked: NetworkSniffTask");
        mContextRef = new WeakReference<Context>(context);
    }

    public static String getMacFromArpCache(String ip) {
        Log.d(TAG, "Checking Arp cache for " + ip);
        if (ip == null)
            return null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {
                        Log.d(TAG, mac);
                        return mac;
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d(TAG, "Let's sniff the network");

        try {
            Context context = mContextRef.get();

            if (context != null) {

                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                WifiInfo connectionInfo = wm.getConnectionInfo();
                int ipAddress = connectionInfo.getIpAddress();

                Log.d(TAG, "Int IP: " + ipAddress);

                String ipStringRaw = Formatter.formatIpAddress(ipAddress);


                Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                Log.d(TAG, "ipString: " + String.valueOf(ipStringRaw));

                //Change 3rd octet
                int dot1 = ipStringRaw.indexOf(".");
                int dot2 = ipStringRaw.indexOf(".", dot1) + dot1 + 1;
                int dot3 = ipStringRaw.lastIndexOf('.');
                Log.d(TAG, "dot1:" + dot1 + " dot2:" + dot2 + " dot3:" + dot3);
                String ipString = ipStringRaw.substring(0, dot2+1) + "1" + ipStringRaw.substring(dot3, ipStringRaw.length());
                Log.d(TAG, ipString);



                String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                Log.d(TAG, "prefix: " + prefix);

                for (int i = 0; i < 255; i++) {

                    // CHECK IF THE USER HAS CANCELLED THE TASK
                    if (isCancelled())  {
                        cancelTask = true; // (OPTIONAL) THIS IS AN ADDITIONAL CHECK I HAVE USED. THINK OF IT AS A FAIL SAFE.
                        break; // REMOVE IF NOT USED IN A FOR LOOP
                    }

                    String testIp = prefix + String.valueOf(i);
                    Log.d(TAG, "Probing: " + testIp);

                    InetAddress address = InetAddress.getByName(testIp);
                    boolean reachable = address.isReachable(500);
                    String hostName = address.getCanonicalHostName();

                    if (reachable) {
                        Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");
                        getMacFromArpCache(String.valueOf(testIp));
                    }
                }
            }
        } catch (Throwable t) {
            Log.e(TAG, "Well that's not good.", t);
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        blMyAsyncTask = true;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        blMyAsyncTask = false;

        if (cancelTask == false)    {
            // THE NORMAL CODE YOU HAVE IN YOUR onPostExecute()
        }
    }

    public static String integerToStringIP(int ip) {
//        eturn ((ip >> 24 ) & 0xFF) + "." +
//
//                ((ip >> 16 ) & 0xFF) + "." +
//
//                ((ip >>  8 ) & 0xFF) + "." +
//
//                ( ip        & 0xFF);

        return (ip & 0xFF)+"."+((ip >> 8) & 0xFF)+"."+((ip >> 16) & 0xFF)+"."+((ip >> 24) & 0xFF);
    }
}
*/
