package com.example.nucleusx.ActivityParcels;

import android.os.Parcel;
import android.os.Parcelable;

public class Device implements Parcelable {
    private int deviceId;
    private int nodeId;

    public Device(int deviceId, int nodeId) {
        this.deviceId = deviceId;
        this.nodeId = nodeId;
    }

    protected Device(Parcel in) {
        deviceId = in.readInt();
        nodeId = in.readInt();
    }

    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public int getNodeId() {
        return nodeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(deviceId);
        dest.writeInt(nodeId);
    }
}
