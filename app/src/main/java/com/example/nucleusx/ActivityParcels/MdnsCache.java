package com.example.nucleusx.ActivityParcels;

import android.net.nsd.NsdServiceInfo;
import android.os.Parcel;
import android.os.Parcelable;

public class MdnsCache implements Parcelable {

    private NsdServiceInfo serviceInfo;
    //private String ip;
    private Boolean resolved;
    private Boolean foundInDatabase;
    private Boolean databaseAddInitiated;

    public MdnsCache(NsdServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
        this.resolved = false;
        this.foundInDatabase = false;
        this.databaseAddInitiated = false;
    }


    protected MdnsCache(Parcel in) {
        serviceInfo = in.readParcelable(NsdServiceInfo.class.getClassLoader());
        byte tmpResolved = in.readByte();
        resolved = tmpResolved == 0 ? null : tmpResolved == 1;
        byte tmpFoundInDatabase = in.readByte();
        foundInDatabase = tmpFoundInDatabase == 0 ? null : tmpFoundInDatabase == 1;
        byte tmpDatabaseAddInitiated = in.readByte();
        databaseAddInitiated = tmpDatabaseAddInitiated == 0 ? null : tmpDatabaseAddInitiated == 1;
    }

    public String getIpFromServiceInfo(){

        NsdServiceInfo serviceInfo = this.getServiceInfo();
        String ipAddress = "Address Unknown";
        if(serviceInfo != null) {
            if (serviceInfo.getHost() != null) {
                if (serviceInfo.getHost().getHostAddress() != null) {
                    ipAddress = serviceInfo.getHost().getHostAddress();
                }
            }
        }
        return ipAddress;
    }

    public int getNodeIdFromServiceInfo(){
        NsdServiceInfo serviceInfo = this.getServiceInfo();
        int nodeId = 0;

        if(serviceInfo == null){
            return nodeId;
        }

        int idx = serviceInfo.getServiceName().lastIndexOf("-") + 1;
        nodeId = Integer.parseInt(serviceInfo.getServiceName().substring(idx));

        return nodeId;
    }

    public static final Creator<MdnsCache> CREATOR = new Creator<MdnsCache>() {
        @Override
        public MdnsCache createFromParcel(Parcel in) {
            return new MdnsCache(in);
        }

        @Override
        public MdnsCache[] newArray(int size) {
            return new MdnsCache[size];
        }
    };

    public void setFoundInDatabase(Boolean foundInDatabase) {
        this.foundInDatabase = foundInDatabase;
    }

    public void setServiceInfo(NsdServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    public void setResolved(Boolean resolved) {
        this.resolved = resolved;
    }

    public NsdServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    public Boolean getResolved() {
        return resolved;
    }

    public Boolean getDatabaseAddInitiated() {
        return databaseAddInitiated;
    }

    public void setDatabaseAddInitiated(Boolean databaseAddInitiated) {
        this.databaseAddInitiated = databaseAddInitiated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(serviceInfo, flags);
        dest.writeByte((byte) (resolved == null ? 0 : resolved ? 1 : 2));
        dest.writeByte((byte) (foundInDatabase == null ? 0 : foundInDatabase ? 1 : 2));
        dest.writeByte((byte) (databaseAddInitiated == null ? 0 : databaseAddInitiated ? 1 : 2));
    }
}
