package com.example.nucleusx.ActivityParcels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ScannedNodeItem implements Parcelable {
    private int nodeId;
    private String nodeIp;
    private Date lastUpdated;
    private String registrationStatus;

    public ScannedNodeItem(int nodeId, String nodeIp, String registrationStatus) {
        this.nodeId = nodeId;
        this.nodeIp = nodeIp;
        this.registrationStatus = registrationStatus;
        this.lastUpdated = new Date(System.currentTimeMillis());
    }


    protected ScannedNodeItem(Parcel in) {
        nodeId = in.readInt();
        nodeIp = in.readString();
        registrationStatus = in.readString();
    }

    public static final Creator<ScannedNodeItem> CREATOR = new Creator<ScannedNodeItem>() {
        @Override
        public ScannedNodeItem createFromParcel(Parcel in) {
            return new ScannedNodeItem(in);
        }

        @Override
        public ScannedNodeItem[] newArray(int size) {
            return new ScannedNodeItem[size];
        }
    };

    public int getNodeId() {
        return nodeId;
    }

    public String getNodeIp() {
        return nodeIp;
    }

    public String getRegistrationStatus() {
        return registrationStatus;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public void setNodeIp(String nodeIp) {
        this.nodeIp = nodeIp;
    }

    public void setRegistrationStatus(String registrationStatus) {
        this.registrationStatus = registrationStatus;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nodeId);
        dest.writeString(nodeIp);
        dest.writeString(registrationStatus);
    }
}
