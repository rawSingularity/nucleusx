package com.example.nucleusx.ActivityParcels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class DeviceListParcel implements Parcelable {

    String listName;
    List<Device> deviceList;

    public DeviceListParcel(String listName) {
        this.listName = listName;
        this.deviceList = new ArrayList<Device>();
    }

    protected DeviceListParcel(Parcel in) {
        listName = in.readString();
        deviceList = in.createTypedArrayList(Device.CREATOR);
    }

    public static final Creator<DeviceListParcel> CREATOR = new Creator<DeviceListParcel>() {
        @Override
        public DeviceListParcel createFromParcel(Parcel in) {
            return new DeviceListParcel(in);
        }

        @Override
        public DeviceListParcel[] newArray(int size) {
            return new DeviceListParcel[size];
        }
    };

    public void setListName(String listName) {
        this.listName = listName;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }

    public String getListName() {
        return listName;
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(listName);
        dest.writeTypedList(deviceList);
    }
}
