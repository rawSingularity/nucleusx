package com.example.nucleusx.ActivityParcels;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MdnsQueryResultParcel implements Parcelable {

    private List<MdnsCache> mdnsCacheList;

    public MdnsQueryResultParcel(List<MdnsCache> mdnsCacheList) {
        this.mdnsCacheList = mdnsCacheList;
    }

    protected MdnsQueryResultParcel(Parcel in) {
        mdnsCacheList = in.createTypedArrayList(MdnsCache.CREATOR);
    }

    public static final Creator<MdnsQueryResultParcel> CREATOR = new Creator<MdnsQueryResultParcel>() {
        @Override
        public MdnsQueryResultParcel createFromParcel(Parcel in) {
            return new MdnsQueryResultParcel(in);
        }

        @Override
        public MdnsQueryResultParcel[] newArray(int size) {
            return new MdnsQueryResultParcel[size];
        }
    };

    public List<MdnsCache> getMdnsCacheList() {
        return mdnsCacheList;
    }

    public void setMdnsCacheList(List<MdnsCache> mdnsCacheList) {
        this.mdnsCacheList = mdnsCacheList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mdnsCacheList);
    }
}
