package com.example.nucleusx.ActivityParcels;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nucleusx.NodeResponseObjects.CalendarEvent;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TemporaryDeviceInfo implements Parcelable {

    private String ip;
    private int node;
    private int UTS;
    private String time;
    private int d;
    private int c;
    private String n;
    private String m;
    private int s;
    private int g;
    private List<CalendarEvent> calendarEvents;

    public String toString(){

        StringBuilder sb = new StringBuilder();

        sb.append("\n");
        sb.append("ip: " + ip);
        sb.append("\n");
        sb.append("node: " + node);
        sb.append("\n");
        sb.append("UTS: " + UTS);
        sb.append("\n");
        sb.append("time: " + time);
        sb.append("\n");
        sb.append("d: " + d);
        sb.append("\n");
        sb.append("c: " + c);
        sb.append("\n");
        sb.append("n: " + n);
        sb.append("\n");
        sb.append("m: " + m);
        sb.append("\n");
        sb.append("s: " + s);
        sb.append("\n");
        sb.append("g: " + g);
        //sb.append("\n");
        //sb.append("calendarEvents: " + calendarEvents.toString());
        sb.append("\n");
        sb.append("\n");

        return sb.toString();
    }

    public TemporaryDeviceInfo(int d) {
        this.d = d;
    }

    public TemporaryDeviceInfo() {
    }

    protected TemporaryDeviceInfo(Parcel in) {
        ip = in.readString();
        node = in.readInt();
        UTS = in.readInt();
        time = in.readString();
        d = in.readInt();
        c = in.readInt();
        n = in.readString();
        m = in.readString();
        s = in.readInt();
        g = in.readInt();
        calendarEvents = in.createTypedArrayList(CalendarEvent.CREATOR);
    }

    public static final Creator<TemporaryDeviceInfo> CREATOR = new Creator<TemporaryDeviceInfo>() {
        @Override
        public TemporaryDeviceInfo createFromParcel(Parcel in) {
            return new TemporaryDeviceInfo(in);
        }

        @Override
        public TemporaryDeviceInfo[] newArray(int size) {
            return new TemporaryDeviceInfo[size];
        }
    };

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setNode(int node) {
        this.node = node;
    }

    public void setUTS(int UTS) {
        this.UTS = UTS;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setD(int d) {
        this.d = d;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void setN(String n) {
        this.n = n;
    }

    public void setM(String m) {
        this.m = m;
    }

    public void setS(int s) {
        this.s = s;
    }

    public void setG(int g) {
        this.g = g;
    }

    public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
        this.calendarEvents = calendarEvents;
    }

    public String getIp() {
        return ip;
    }

    public int getNode() {
        return node;
    }

    public int getUTS() {
        return UTS;
    }

    public String getTime() {
        return time;
    }

    public int getD() {
        return d;
    }

    public int getC() {
        return c;
    }

    public String getN() {
        return n;
    }

    public String getM() {
        return m;
    }

    public int getS() {
        return s;
    }

    public int getG() {
        return g;
    }

    public List<CalendarEvent> getCalendarEvents() {
        return calendarEvents;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ip);
        dest.writeInt(node);
        dest.writeInt(UTS);
        dest.writeString(time);
        dest.writeInt(d);
        dest.writeInt(c);
        dest.writeString(n);
        dest.writeString(m);
        dest.writeInt(s);
        dest.writeInt(g);
        dest.writeTypedList(calendarEvents);
    }
}
