package com.example.nucleusx.Utilities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.R;
import com.example.nucleusx.ViewModels.DeviceViewModel;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import static java.lang.Math.abs;

public class HardwareDescriptors {

    private static final String TAG = "HardwareDescriptors";
    //
    private NodeViewModel nodeViewModel;
    private List<Node> mAllNodes;
    //
    private DeviceViewModel deviceViewModel;
    private List<Device> mAllDevices;

    // IP validation pattern
    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public HardwareDescriptors(Activity activity) {
        nodeViewModel = ViewModelProviders.of((FragmentActivity) activity).get(NodeViewModel.class);
        nodeViewModel.getAllNodes().observe((LifecycleOwner) activity, new Observer<List<Node>>() {
            @Override
            public void onChanged(@Nullable List<Node> nodes) {
                Log.d(TAG, "onChanged: mAllNodes changed. New size:" + nodes.size());
                mAllNodes = nodes;
            }
        });

        deviceViewModel = ViewModelProviders.of((FragmentActivity) activity).get(DeviceViewModel.class);
        deviceViewModel.getAllDevices().observe((LifecycleOwner) activity, new Observer<List<Device>>() {
            @Override
            public void onChanged(@Nullable List<Device> devices) {
                Log.d(TAG, "allDevices changed. New size:" + devices.size());
                mAllDevices = devices;
            }
        });
    }

    public List<Device> getAllDevices() {
        return mAllDevices;
    }

    public void removeDevicesFromRoom(String roomName) {
        deviceViewModel.removeDevicesFromLocation(roomName);
    }

    public void updateAllDeviceLocationIn(String oldName, String newName) {
        deviceViewModel.updateAllDeviceLocationIn(oldName, newName);
    }

    public List<Integer> getListOfCombinedNodeIdAndDeviceIdForRoom(String roomName) {
        Log.d(TAG, "Incoming roomName: " + roomName);
        List<Integer> list = new ArrayList<Integer>();

        if (roomName == null) {
            Log.d(TAG, "roomName is null");
            return list;
        }

        if (mAllDevices == null) {
            Log.d(TAG, "mAllDevices is null");
            return list;
        }

        for (Device device : mAllDevices) {
            Log.d(TAG, "Comparing with: " + device.getLocation());
            if (device.getLocation().trim().equalsIgnoreCase(roomName.trim())) {
                list.add(this.getCombinedNodeDeviceId(device.getNodeId(), device.getDeviceId()));
            }
        }

        return list;
    }

    public List<com.example.nucleusx.ActivityParcels.Device> getListOfDevicesForRoom(String roomName) {
        Log.d(TAG, "Incoming roomName: " + roomName);
        List<com.example.nucleusx.ActivityParcels.Device> list = new ArrayList<com.example.nucleusx.ActivityParcels.Device>();

        if (roomName == null) {
            Log.d(TAG, "roomName is null");
            return list;
        }

        if (mAllDevices == null) {
            Log.d(TAG, "mAllDevices is null");
            return list;
        }

        for (Device device : mAllDevices) {
            Log.d(TAG, "Comparing with: " + device.getLocation());
            if (device.getLocation().trim().equalsIgnoreCase(roomName.trim())) {
                list.add(new com.example.nucleusx.ActivityParcels.Device(device.getDeviceId(), device.getNodeId()));
            }
        }

        return list;
    }

    public List<com.example.nucleusx.ActivityParcels.Device> getListOfAllDeviceParcels() {
        List<com.example.nucleusx.ActivityParcels.Device> list = new ArrayList<com.example.nucleusx.ActivityParcels.Device>();

        if (mAllDevices == null) {
            Log.d(TAG, "mAllDevices is null");
            return list;
        }

        for (Device device : mAllDevices) {
            Log.d(TAG, "Comparing with: " + device.getLocation());
            list.add(new com.example.nucleusx.ActivityParcels.Device(device.getDeviceId(), device.getNodeId()));

        }

        return list;
    }

    public int getNumberOfDevicesInRoom(String roomName) {
        int numberOfDevices = 0;

        for (Device device : mAllDevices) {
            if (device.getLocation().trim().equalsIgnoreCase(roomName.trim())) {
                numberOfDevices++;
            }
        }
        return numberOfDevices;
    }

    public String getIpForNodeId(int nodeId) {
        String ip = "";

        if (mAllNodes == null) {
            return ip;
        }

        for (Node node : mAllNodes) {
            if (node.getNodeId() == nodeId) {
                return node.getIp();
            }
        }

        return ip;
    }

    public HardwareDescriptors() {
        Log.d(TAG, "HardwareDescriptors empty constructor");
    }

    public int getConfigurationValueForConfigurationText(List<Integer> configList) {
        int c = 0;

        if (configList == null) {
            return -1;
        }

        if (configList.size() == 0) {
            return -1;
        }

        for (int i : configList) {
            Log.d(TAG, "Analyzing config value: " + i);
            switch (i) {
                //Primary Mode
                case R.string.device_mode_input_external_pullup:
                    c |= 0;
                    break;

                case R.string.device_mode_output_digital:
                    c |= 1;
                    break;

                case R.string.device_mode_input_internal_pullup:
                    c |= 2;
                    break;

                case R.string.device_mode_output_pwm:
                    c |= 3;
                    break;

                //Levels
                case R.string.active_level_low:
                    c |= 0;
                    break;

                case R.string.active_level_high:
                    c |= 0b00000100;
                    break;

                //Interrupt Mode
                case R.string.interrupt_mode_no_interrupt:
                    c |= 0;
                    break;

                case R.string.interrupt_mode_low_to_high:
                    c |= 0b00000100;
                    break;

                case R.string.interrupt_mode_high_to_low:
                    c |= 0b00001000;
                    break;

                case R.string.interrupt_mode_change:
                    c |= 0b0001100;
                    break;

                case R.string.interrupt_mode_level_low:
                    c |= 0b00010000;
                    break;

                case R.string.interrupt_mode_level_high:
                    c |= 0b00010100;
                    break;
            }
        }

        Log.d(TAG, "Computed c-value: " + c);
        return c;
    }

    public int getDeviceModeStringResourceId(int c) {
        int val = (c & 3);
        Log.d(TAG, "Analyzing: " + val + " for c=" + c);

        switch (val) {
            case 0:
                return R.string.device_mode_input_external_pullup;

            case 1:
                return R.string.device_mode_output_digital;

            case 2:
                return R.string.device_mode_input_internal_pullup;

            case 3:
                return R.string.device_mode_output_pwm;

            default:
                return R.string.error;
        }
    }

    public int getDeviceSecondaryModeString(int c) {
        int val;
        if ((getDeviceModeStringResourceId(c) == R.string.device_mode_input_external_pullup) ||
                (getDeviceModeStringResourceId(c) == R.string.device_mode_input_internal_pullup)) {

            val = (c & 0b00011100);

            switch (val) {
                case 0b00000000:
                    return R.string.interrupt_mode_no_interrupt;

                case 0b00000100:
                    return R.string.interrupt_mode_low_to_high;

                case 0b00001000:
                    return R.string.interrupt_mode_high_to_low;

                case 0b00001100:
                    return R.string.interrupt_mode_change;

                case 0b00010000:
                    return R.string.interrupt_mode_level_low;

                case 0b00010100:
                    return R.string.interrupt_mode_level_high;

                default:
                    return R.string.error;
            }
        } else {
            val = (c & 0b00000100);
            Log.d(TAG, "val = " + val);
            switch (val) {
                case 0:
                    return R.string.active_level_low;

                case 0b00000100:
                    return R.string.active_level_high;

                default:
                    return R.string.error;
            }

        }
    }

    public boolean isActiveLevel(int config) {
        if ((config & 0b00000100) == 0b00000100) {
            return true;
        }
        return false;
    }

    public int getDigitalStateFromBoolean(boolean b) {
        if (b) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean getBooleanFromDigitalState(int state) {
        if (state == 0) {
            return false;
        }
        return true;
    }


    public int getHardwareStateFromSoftwareSlider(int progress, int max) {
        return (progress * 255) / max;
    }

    public int toggleDayForRepeatMask(int repeatMask, int calendarWeekDay) {

        /*
        1. We have repeat Mask value 0b0xxxxxxxx
        2. We have calendarWeekDay
        3. Goal: we have to toggle the bit corresponding to the calendarWeekDay
        4. So, first, convert calendarWeekDay into the bitMask e.g.: 0b00000001
        5. second, let's get the bit value at that position: value = bitMask & repeatMask
           This can be either 0b00000000 or 0b00000001
        6. So it needs to be converted into either 0b00000001 or 0b00000000
        7. How to do that?
         */

        int bitMask = convertCalendarWeekDayIntoHardwareRepeatDayBitMask(calendarWeekDay);
        int value = bitMask & repeatMask;

        return ((((~bitMask) & 0b01111111) & repeatMask) | (value ^ bitMask));
    }

    public int convertCalendarWeekDayIntoHardwareRepeatDayBitMask(int calendarWeekDay) {
        switch (calendarWeekDay) {
            case Calendar.SUNDAY:
                return 0b01000000;

            case Calendar.MONDAY:
                return 0b00100000;

            case Calendar.TUESDAY:
                return 0b00010000;

            case Calendar.WEDNESDAY:
                return 0b00001000;

            case Calendar.THURSDAY:
                return 0b00000100;

            case Calendar.FRIDAY:
                return 0b00000010;

            case Calendar.SATURDAY:
                return 0b00000001;

            default:
                return -1;
        }
    }

    public int computeRepeatMask(Boolean su, boolean mo, boolean tu, boolean we, boolean th, boolean fr, boolean sa) {
        int repeatMask = 0;

        if (su) {
            repeatMask |= 0b01000000;
        }
        if (mo) {
            repeatMask |= 0b00100000;
        }
        if (tu) {
            repeatMask |= 0b00010000;
        }
        if (we) {
            repeatMask |= 0b00001000;
        }
        if (th) {
            repeatMask |= 0b00000100;
        }
        if (fr) {
            repeatMask |= 0b00000010;
        }
        if (sa) {
            repeatMask |= 0b00000001;
        }

        return repeatMask;
    }

    //Repeat mask
    public String getDaysStringFromRepeatMask(int r) {
        StringBuilder sbShort = new StringBuilder();
        StringBuilder sbLong = new StringBuilder();

        // Sunday
        if ((r & 0b01000000) == 0b01000000) {
            sbShort.append("S ");
            sbLong.append(("Sun  "));
        }

        // Monday
        if ((r & 0b00100000) == 0b00100000) {
            sbShort.append("M ");
            sbLong.append(("Mon  "));
        }

        // Tuesday
        if ((r & 0b00010000) == 0b00010000) {
            sbShort.append("T ");
            sbLong.append(("Tue  "));
        }

        // Wednesday
        if ((r & 0b00001000) == 0b00001000) {
            sbShort.append("W ");
            sbLong.append(("Wed  "));
        }

        // Thursday
        if ((r & 0b00000100) == 0b00000100) {
            sbShort.append("T ");
            sbLong.append(("Thu  "));
        }

        // Friday
        if ((r & 0b00000010) == 0b00000010) {
            sbShort.append("F ");
            sbLong.append(("Fri  "));
        }

        // Saturday
        if ((r & 0b00000001) == 0b00000001) {
            sbShort.append("S ");
            sbLong.append(("Sat"));
        }

        return sbLong.toString();
    }

    public boolean repeatsOnSunday(int r) {
        // Sunday
        if ((r & 0b01000000) == 0b01000000) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnMonday(int r) {
        // Monday
        if ((r & 0b00100000) == 0b00100000) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnTuesday(int r) {
        // Tuesday
        if ((r & 0b00010000) == 0b00010000) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnWednesday(int r) {
        // Wednesday
        if ((r & 0b00001000) == 0b00001000) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnThursday(int r) {
        // Thursday
        if ((r & 0b00000100) == 0b00000100) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnFriday(int r) {
        // Friday
        if ((r & 0b00000010) == 0b00000010) {
            return true;
        }
        return false;
    }

    public boolean repeatsOnSaturday(int r) {
        // Saturday
        if ((r & 0b00000001) == 0b00000001) {
            return true;
        }
        return false;
    }

    public boolean eventIsSetToTriggerForRepeatMask(Calendar calendar, int repeatMask) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                return repeatsOnSunday(repeatMask);

            case Calendar.MONDAY:
                return repeatsOnMonday(repeatMask);

            case Calendar.TUESDAY:
                return repeatsOnTuesday(repeatMask);

            case Calendar.WEDNESDAY:
                return repeatsOnWednesday(repeatMask);

            case Calendar.THURSDAY:
                return repeatsOnThursday(repeatMask);

            case Calendar.FRIDAY:
                return repeatsOnFriday(repeatMask);

            case Calendar.SATURDAY:
                return repeatsOnSaturday(repeatMask);

            default:
                return false;
        }
    }


    public String getFormattedTimeZoneFromMinutes(int minutes) {
        int hour = abs(minutes / 60);
        int min = abs(minutes % 60);

        String direction;
        if (minutes > 0) {
            direction = "+";
        } else {
            direction = "-";
        }

        String formatted = String.format("%s%02d:%02d", direction, hour, min);
        Log.d("formatted timezone", formatted);
        return formatted;
    }

    public String getDeviceStateString(int state) {
        if (state == 1) {
            return "turned on";
        } else if (state == 0) {
            return "turned off";
        } else {
            return "set to " + String.valueOf(getStatePercentValue(state)) + "%";
        }
    }

    public int getStatePercentValue(int state) {
        return (state * 100) / 255;
    }

    // IP address validation
    public boolean validIp(final String ip) {
        return PATTERN.matcher(ip).matches();
    }

    //Room/deviceLocation name
    public String sanitizeRoomName(String name) {
        String newName = "";

        if (name == null || name.length() == 0) {
            newName = "Unnamed Room";
        } else {
            newName = name;
        }
        return (capitalizeFirstLetterOfEachWord(newName));
    }

    //Name
    public String sanitzeDeviceNameForDevice(String name, int d) {
        String newName = "";

        if (name == null || name.length() == 0) {
            newName = "Device D" + d;
        } else {
            newName = name;
        }
        return (capitalizeFirstLetterOfEachWord(newName));
    }

    private String capitalizeFirstLetterOfEachWord(String str) {

        String[] splitPhrase = str.split(" ");
        String result = "";

        for (String word : splitPhrase) {
            result += toTitleCase(word) + " ";
        }
        return (result.trim());
    }

    private static String toTitleCase(String word) {
        return Character.toUpperCase(word.charAt(0)) + word.substring(1);
    }

    public String getDeviceStateStringFromHardwareStateValue(int state, int config) {
        if (state == 0) {
            return "OFF";
        } else if (state == 1) {
            return "ON";
        } else {
            return (String.valueOf(getStatePercentValue(state)) + "%");
        }
    }

    public long getHardwareTimeZoneSecondsFromReportedTimeString(String espTimeZoneStr) {

        //String espTimeZoneStr = "lskdflsdflsjf+05:30";

        String tzHour = espTimeZoneStr.substring(espTimeZoneStr.length() - 6, espTimeZoneStr.length() - 3);
        int espTzHourValue = Integer.parseInt(tzHour);
        int multiplier = 0;
        if (espTzHourValue < 0) {
            multiplier = -1;
        } else {
            multiplier = 1;
        }
        espTzHourValue = Math.abs(espTzHourValue);
        int espTzMinuteValue = Integer.parseInt(espTimeZoneStr.substring(espTimeZoneStr.length() - 2));
        long espTimeZoneValue = (espTzHourValue * 60 * 60) + (espTzMinuteValue * 60);
        espTimeZoneValue *= multiplier;

        Log.d(TAG, "espTimeZoneStr: " + espTimeZoneStr);
        Log.d(TAG, "tzHour: " + tzHour);
        Log.d(TAG, "multiplier: " + multiplier);
        Log.d(TAG, "espTzHourValue: " + espTzHourValue);
        Log.d(TAG, "espTzMinuteValue: " + espTzMinuteValue);
        Log.d(TAG, "espTimeZoneValue: " + espTimeZoneValue);

        return espTimeZoneValue;
    }

    public int numberOfRepeatDays(int r) {
        int counter = 0;
        for (int i = 0; i < 8; i++) {
            if ((r & 1) == 1) {
                counter++;
            }
            r = r >> 1;
        }
        return counter;
    }

    public boolean isDigitalOutput(int c) {
        if ((c & 3) == 1) {
            return true;
        }
        return false;
    }

    public boolean isPwmOutput(int c) {
        if ((c & 3) == 3) {
            return true;
        }
        return false;
    }

    public long getCurrentUts() {
        long uts = 0;

        Calendar c = Calendar.getInstance();
        uts = c.getTimeInMillis() + c.get(Calendar.ZONE_OFFSET);
        uts /= 1000; //Convert to seconds

        Log.d(TAG, "getCurrentUts:" + uts);

        return uts;
    }

    public int getCombinedNodeDeviceId(int nodeId, int deviceId) {
        return ((nodeId << 8) | deviceId);
    }
}
