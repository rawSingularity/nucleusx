package com.example.nucleusx.Utilities;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/*
Reference: https://stackoverflow.com/questions/8818290/how-do-i-connect-to-a-specific-wi-fi-network-in-android-programmatically
 */

public class WifiConnectionService {

    private static final String TAG = "WifiConnectionService";
    private Context mContext;

    public WifiConnectionService(Context context){
        mContext = context;
    }

    public void initiateConnection(String mNetworkSSID){

        if(mNetworkSSID == null){
            Log.d(TAG, "Error! mNetworkSSID is null");
            return;
        }

        if(mContext == null){
            Log.d(TAG, "Error! mContext is null");
            return;
        }

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + mNetworkSSID + "\"";

        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);

        //Enable
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            if(i.SSID != null && i.SSID.equals("\"" + mNetworkSSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();
                Toast.makeText(mContext, "Connecting to " + mNetworkSSID + ". Please wait.", Toast.LENGTH_LONG).show();
                break;
            }
        }
    }
}
