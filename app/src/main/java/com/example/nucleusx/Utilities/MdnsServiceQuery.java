package com.example.nucleusx.Utilities;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.nucleusx.ActivityParcels.MdnsCache;
import com.example.nucleusx.ActivityParcels.MdnsQueryResultParcel;
import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static android.net.nsd.NsdManager.FAILURE_ALREADY_ACTIVE;
import static android.net.nsd.NsdManager.FAILURE_INTERNAL_ERROR;
import static android.net.nsd.NsdManager.FAILURE_MAX_LIMIT;

public class MdnsServiceQuery {

    private static final String TAG = "MdnsServiceQuery";

    //MDNS
    private NsdManager nsdManager;
    private NsdManager.DiscoveryListener discoveryListener;
    private NsdManager.ResolveListener resolveListener;
    private NsdServiceInfo nsdServiceInfo;
    private Boolean firstServiceSubmitted;
    private Boolean firstServiceResolved;
    private Boolean noPendingServicesLeftToResolve;
    private Boolean serviceDiscoveryInProgress;

    private List<MdnsCache> mdnsCacheList;
    private MdnsQueryResultParcel mMdsnQueryResultParcel;
    private static final String SERVICE_TYPE = "_nucleus._tcp.";
    private Boolean searchRequestedByUser;


    private Boolean reInitializeDiscoveryListener;

    private HardwareDescriptors hd;

    //Database
    private NodeViewModel nodeViewModel;
    private List<Node> dbNodeList;

    //Singleton
    private static Context mContext;
    private static MdnsServiceQuery mInstance;

    private MdnsServiceQuery(Context context) {
        mContext = context;
    }

    private static Activity mActivity;

    public static MdnsServiceQuery getInstance(Activity activity) {
        //Create instance
        if (mInstance == null) {
            mInstance = new MdnsServiceQuery(activity.getApplicationContext());
        } else {
            mContext = activity.getApplicationContext();
        }

        //Setup other members
        mActivity = activity;

        if (mInstance.mdnsCacheList == null) {
            mInstance.mdnsCacheList = new ArrayList<>();
        }

        if (mInstance.mMdsnQueryResultParcel == null) {
            mInstance.mMdsnQueryResultParcel = new MdnsQueryResultParcel(mInstance.mdnsCacheList);
        }

        mInstance.reInitializeDiscoveryListener = false;

        // Setup Observer to live data of allNodes
        mInstance.databaseDataObserverSetup(mActivity);

        if (mInstance.nsdManager == null) {
            Log.d(TAG, "mInstance.nsdManager is null.");
            mInstance.nsdManager = (NsdManager) (mContext.getSystemService(Context.NSD_SERVICE));
        }

        if(mInstance.hd == null){
            mInstance.hd = new HardwareDescriptors();
        }

        return mInstance;
    }


    public void searchForServices() {

        firstServiceSubmitted = false;
        firstServiceResolved = false;
        noPendingServicesLeftToResolve = false;
        serviceDiscoveryInProgress = false;
        searchRequestedByUser = true;


        initializeDiscoveryListener();


        new CountDownTimer(5000, 5000) {
            int counter = 0;

            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "nsdManager: onTick: count:" + ++counter);
                //Check if new services are found
                //checkIfUnregisteredNucleusServicesFound();
            }

            public void onFinish() {
                //Log.d(TAG, "Stopping nsdManager: done!");
                //At this point, stop the discovery service to reduce network activity

                //nsdManager.stopServiceDiscovery(discoveryListener);
            }
        }.start();
    }

    public void refetchIpsForExistingNodes() {
        for(MdnsCache mdnsCache : mdnsCacheList){
            mdnsCache.setResolved(false);
        }

        NsdServiceInfo serviceInfo = mdnsCacheList.get(0).getServiceInfo();
        if (serviceInfo != null) {
            nsdManager.resolveService(nsdServiceInfo, new MyResolveListener());
        }
    }


    private void initializeDiscoveryListener() {

        if (discoveryListener != null) {
            Log.d(TAG, "discoverListener already exist. Stopping the instance...");
            nsdManager.stopServiceDiscovery(discoveryListener);
            mInstance.reInitializeDiscoveryListener = true;

            discoveryListener = null;
            return;
        }

        discoveryListener = new NsdManager.DiscoveryListener() {
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                //nsdManager.stopServiceDiscovery(this);
                Log.d(TAG, "onStartDiscoveryFailed: errorCode:" + errorCode);
                //vtvMockup.append("\nonStartDiscoveryFailed: errorCode:" + errorCode);
                serviceDiscoveryInProgress = false;
                firstServiceSubmitted = false;
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                nsdManager.stopServiceDiscovery(this);
                Log.d(TAG, "onStopDiscoveryFailed: errorCode:" + errorCode);
                //vtvMockup.append("\nonStopDiscoveryFailed: errorCode:" + errorCode);
                serviceDiscoveryInProgress = false;
                firstServiceSubmitted = false;
            }

            @Override
            public void onDiscoveryStarted(String serviceType) {
                Log.d(TAG, "onDiscoveryStarted");
                //vtvMockup.append("\nonDiscoveryStarted");
                serviceDiscoveryInProgress = true;
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.d(TAG, "onDiscoveryStopped");
                //vtvMockup.append("\nonDiscoverStopped");
                serviceDiscoveryInProgress = false;
                firstServiceSubmitted = false;

                //Initiate new if nothing was found
                if (mdnsCacheList.size() == 0) {
                    //initiateServiceDiscovery();
                }

                if (reInitializeDiscoveryListener) {
                    reInitializeDiscoveryListener = false;
                    initializeDiscoveryListener();
                }
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                // A service was found!  Do something with it.
                String name = service.getServiceName();
                String type = service.getServiceType();
                //vtvMockup.append("\nServiceFound: name:" + name + " type:" + type);
                Log.d(TAG, "\n\nService Found: " + name);

                addFoundServiceToMdnsCacheList(service);

                /*
                Since onServiceFound are called asynchronously , there is a chance of duplication
                of services in the list.
                So, we won't add services to database from here.
                 */
                //addFoundServiceToDatabase(service);

                if (!firstServiceSubmitted) {
                    Log.d(TAG, "This is the first service to be resolved!!");
                    //vtvMockup.append("\nThis is the first service to be resolved!");
                    nsdManager.resolveService(service, new MyResolveListener());
                    firstServiceSubmitted = true;
                }
                if (noPendingServicesLeftToResolve) {
                    Log.d(TAG, "New service found to resolve");
                    //vtvMockup.append("\n\nNew service found to resolve");
                    nsdManager.resolveService(service, new MyResolveListener());
                    noPendingServicesLeftToResolve = false;
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo serviceInfo) {

            }
        };

        //Since we will be performing new query from scratch, let's mark all of them as resolved
        for(MdnsCache mdnsCache : mdnsCacheList){
            mdnsCache.setResolved(false);
        }

        //Finally, Kick off discovery
        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, discoveryListener);
    }


    private void addFoundServiceToMdnsCacheList(NsdServiceInfo service) {

        Boolean serviceFoundInMdnsCacheList = false;
        for (MdnsCache mdnsCache : mdnsCacheList) {
            if (mdnsCache.getServiceInfo().getServiceName().equalsIgnoreCase(service.getServiceName())) {
                //Service already exist in the cache - just update it with new service info
                Log.d(TAG, "addFoundServiceToMdnsCacheList: Service already in mdnsCacheList: " + service.getServiceName());
                mdnsCache.setServiceInfo(service);
                serviceFoundInMdnsCacheList = true;
                break;
            }
        }

        if(!serviceFoundInMdnsCacheList) {
            //If here, it was not found in the list - just add a new one
            Log.d(TAG, "addFoundServiceToMdnsCacheList: Service added to mdnsCacheList: " + service.getServiceName());
            mdnsCacheList.add(new MdnsCache(service));
        }

        //Also kick-off database sync
        //initiateDatabaseSync();
    }

    private void addFoundServiceToDatabase(NsdServiceInfo service){

        if(dbNodeList == null || mdnsCacheList == null){
            return;
        }

        Boolean serviceFoundInDatabase = false;
        int index = indexOfServiceInMdnsCacheList(service);

        MdnsCache mdnsCache = new MdnsCache(service);

        /* service.getServiceName().equalsIgnoreCase(node.getServiceName()) */
        for (Node node : dbNodeList) {
            //First, check database
            if (node.getNodeId() == mdnsCache.getNodeIdFromServiceInfo()){

                serviceFoundInDatabase = true;
                mdnsCacheList.get(index).setServiceInfo(service);
                Log.d(TAG, "Service " + service.getServiceName() + " found in database. Updating");
                updateExistingServiceInfoInDatabase(mdnsCacheList.get(index));
                break;
            }

            //Second, make sure, the request to addToDatabase has not been made
            if(index != -1){
                if(mdnsCacheList.get(index).getDatabaseAddInitiated()){
                    serviceFoundInDatabase = true;
                    // If it has, then we need to wait for it.
                    Log.d(TAG, mdnsCacheList.get(index).getServiceInfo().getServiceName() + " is marked as addInitiated in database. Will not add again");
                    break;
                }

            }
        }

        //If here, the service wasn't found - add NEW
        if(!serviceFoundInDatabase){
            addNewServiceInfoToDatabase(mdnsCacheList.get(index));
        }
    }

    private void addNewServiceInfoToDatabase(MdnsCache mdnsCache) {

        if (dbNodeList == null || mdnsCache == null) {
            return;
        }

        String ipAddress = mdnsCache.getIpFromServiceInfo();
        String serviceName = mdnsCache.getServiceInfo().getServiceName();
        int nodeId = mdnsCache.getNodeIdFromServiceInfo();

        //Mark it as add being initiated.
        mdnsCache.setDatabaseAddInitiated(true);

        Node node = new Node(serviceName, nodeId, ipAddress);

        Log.d(TAG, "addNewServiceInfoToDatabase for node:  " + node.getNodeId() + " reg:" + node.getRegistered());

        nodeViewModel.insert(node);
    }

    private void updateExistingServiceInfoInDatabase(MdnsCache mdnsCache){
        if (dbNodeList == null || mdnsCache == null) {
            return;
        }

        String ipAddress = mdnsCache.getIpFromServiceInfo();
        String serviceName = mdnsCache.getServiceInfo().getServiceName();

        Node node = getNodeForServiceName(serviceName);

        if(node == null){
            return;
        }

        //Update only if Ip address is valid
        if(hd.validIp(ipAddress)){
            node.setIp(ipAddress);

            Log.d(TAG, "updateExistingServiceInfoInDatabase: service:" + node.getServiceName() + " ip:" + node.getIp() + " node:" + node.getNodeId());
            nodeViewModel.update(node);
        }
    }

    private Node getNodeForServiceName(String serviceName){

        if(dbNodeList == null){
            return null;
        }

        Node node = null;

        for(Node n : dbNodeList){
            if(n.getServiceName().equalsIgnoreCase(serviceName)){
                node = n;
                Log.d(TAG, "getNodeForServiceName: node:" + node.getNodeId() + " reg:" + node.getRegistered());
                break;
            }
        }

        return node;
    }


    /*
    RESOLVE LISTENER
     */
    private class MyResolveListener implements NsdManager.ResolveListener {
        @Override
        public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            //your code
            Log.d(TAG, "onResolveFailed: errorCode:" + errorCode);
            switch (errorCode) {
                case FAILURE_INTERNAL_ERROR:
                    Log.d(TAG, "FAILURE_INTERNAL_ERROR: " + errorCode);
                    break;

                case FAILURE_ALREADY_ACTIVE:
                    Log.d(TAG, "FAILURE_ALREADY_ACTIVE: " + errorCode);
                    break;

                case FAILURE_MAX_LIMIT:
                    Log.d(TAG, "FAILURE_MAX_LIMIT: " + errorCode);
                    break;

                default:
                    Log.d(TAG, "unknown error code: " + errorCode);
                    break;
            }
            //vtvMockup.append("\n\nonResolveFailed: errorCode:" + errorCode);

            //Retry
            //Log.d(TAG, "Retrying resolving  " + serviceInfo.getServiceName());
            //nsdManager.resolveService(serviceInfo, new MyResolveListener());

            NsdServiceInfo nsdServiceInfo = getServiceToResolve(indexOfServiceInMdnsCacheList(serviceInfo));

            if (nsdServiceInfo != null) {
                nsdManager.resolveService(nsdServiceInfo, new MyResolveListener());
            } else {
                Log.d(TAG, "onResolveFailed: Error: nsdServiceInfo is NULL. Don't know what happens next.");
            }
        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {

            nsdServiceInfo = serviceInfo;

            // Port is being returned as 9. Not needed.
            //int port = mServiceInfo.getPort();

            InetAddress host = nsdServiceInfo.getHost();
            String ipAddress = host.getHostAddress();
            Log.d(TAG, "Resolved address = " + ipAddress);
            //vtvMockup.append("\nResolved address = " + ipAddress + " for " + serviceInfo.getServiceName());

            //Mark it as resolved
            //markServiceAsResolved(serviceInfo, ipAddress);

            //Update in mdnsCacheList
            for(MdnsCache mdnsCache : mdnsCacheList){
                if(mdnsCache.getServiceInfo().getServiceName().equalsIgnoreCase(serviceInfo.getServiceName())){
                    mdnsCache.setServiceInfo(serviceInfo);
                    mdnsCache.setResolved(true);
                }
            }

            //Add to database
            addFoundServiceToDatabase(serviceInfo);

            if (!firstServiceResolved) {
                firstServiceResolved = true;
                //Also update Database
                //syncDatabaseWithServiceInfo(serviceInfo.getServiceName(), ipAddress);
            }

            //Resolve new one
            //NsdServiceInfo newService = nowResolveThisService();
            NsdServiceInfo newService = getServiceToResolve(indexOfServiceInMdnsCacheList(serviceInfo));
            if (newService != null) {
                Log.d(TAG, "\nNow requesting resolution of: " + newService.getServiceName());
                nsdManager.resolveService(newService, new MyResolveListener());
            } else {
                Log.d(TAG, "ALL SERVICES RESOLVED !!!!");
                //vtvMockup.append("\nALL SERVICES RESOLVED!!!!");
                noPendingServicesLeftToResolve = true;
                firstServiceSubmitted = false;
            }
        }
    }

    /*private NsdServiceInfo nowResolveThisService() {
        for (MdnsCache c : mdnsCacheList) {
            if (!c.getResolved()) {
                Log.d(TAG, "Now resolving " + c.getServiceInfo().getServiceName());
                //vtvMockup.append("\n\nNow resolving " + c.getServiceInfo().getServiceName());
                return c.getServiceInfo();
            }
        }
        return null;
    }*/

    private NsdServiceInfo getServiceToResolve(int lastProcessedIndex) {
        NsdServiceInfo candidateService = null;

        for (MdnsCache c : mdnsCacheList) {
            if (mdnsCacheList.indexOf(c) <= lastProcessedIndex) {
                continue;
            }
            if (!c.getResolved()) {
                candidateService = c.getServiceInfo();
                break;
            }
        }

        if (candidateService != null) {
            return candidateService;
        }

        for (MdnsCache c : mdnsCacheList) {
            if (!c.getResolved()) {
                candidateService = c.getServiceInfo();
                break;
            }
        }

        return candidateService;
    }

    private void markServiceAsResolved(NsdServiceInfo serviceInfo, String ipAddress) {
        for (MdnsCache c : mdnsCacheList) {
            if (c.getServiceInfo().getServiceName().equalsIgnoreCase(serviceInfo.getServiceName())) {
                c.setServiceInfo(serviceInfo);
                c.setResolved(true);
                Log.d(TAG, "Marked " + ipAddress + " as resolved");
                //vtvMockup.append("\nMarked " + ipAddress + " as resolved for " + serviceInfo.getServiceName());
                //Raise snackbar
                checkIfUnregisteredNucleusServicesFound();
            }
        }
    }

    /*
    private void syncDatabaseWithServiceInfo(String serviceName, String ipAddress) {

        if (dbNodeList == null) {
            Log.d(TAG, "Data from database hasn't yet arrived");
            return;
        }

        int nodeId = Integer.parseInt(
                serviceName.substring(serviceName.lastIndexOf("-") + 1, serviceName.length())
        );
        Log.d(TAG, "findDataForNodeId for node:  " + nodeId);

        for (Node dbNode : dbNodeList) {
            if (dbNode.getNodeId() == nodeId) {
                // it already exists in database. just update time stamp
                nodeViewModel.updateIpForNodeId(new Node(nodeId,
                        ipAddress, false, "", new Date(0), "", new Date(0)));
                markServiceAsFoundInDatabase(serviceName);
                return;
            }
        }

        //IF here, it is not in db.
        addEntryToDatabase(nodeId, ipAddress);
    }

    private void markServiceAsFoundInDatabase(String serviceName) {
        for (MdnsCache mdnsCache : mdnsCacheList) {
            if (mdnsCache.getServiceInfo().getServiceName().equalsIgnoreCase(serviceName)) {
                mdnsCache.setFoundInDatabase(true);
                break;
            }
        }
    }

    private void addEntryToDatabase(int nodeId, String ipAddress) {
        //Add in database
        Log.d(TAG, "addEntryToDatabase: nodeId: " + nodeId);
        Node node = new Node(
                nodeId,
                ipAddress,
                false,
                "Not Registered",
                new Date(0),
                "unknown",
                new Date(0)
        );
        nodeViewModel.insert(node);
    }
    */



    private int indexOfServiceInMdnsCacheList(NsdServiceInfo service) {
        for (MdnsCache c : mdnsCacheList) {
            if (c.getServiceInfo().getServiceName().equalsIgnoreCase(service.getServiceName())) {
                return mdnsCacheList.indexOf(c);
            }
        }
        Log.d(TAG, "Error! Service not found in MDNS Cache");
        return -1;
    }



    /*
    DATABASE OBSERVER SETUP
     */
    private void databaseDataObserverSetup(Activity activity) {

        nodeViewModel = ViewModelProviders.of((FragmentActivity) activity).get(NodeViewModel.class);

        nodeViewModel.getAllNodes().observe((LifecycleOwner) activity, new Observer<List<Node>>() {
            @Override
            public void onChanged(@Nullable List<Node> nodes) {
                Log.d(TAG, "dbNodeList has a change. Size=" + nodes.size());
                dbNodeList = nodes;

                //Mark services as added
                for(Node node : dbNodeList){
                    markServiceAsAdded(node.getServiceName());
                }

                //Look for next service in mdnsCacheList
                initiateAddToDatabaseFromMdnsCacheList();
            }
        });
    }

    private void markServiceAsAdded(String serviceName){
        for(MdnsCache mdnsCache : mdnsCacheList){
            if(mdnsCache.getServiceInfo().getServiceName().equalsIgnoreCase(serviceName)){
                mdnsCache.setDatabaseAddInitiated(false);
                break;
            }
        }
    }

    private void initiateAddToDatabaseFromMdnsCacheList(){

        for(MdnsCache mdnsCache : mdnsCacheList){
            if(!serviceExistInDatabase(mdnsCache.getServiceInfo().getServiceName()) &&
                    !mdnsCache.getDatabaseAddInitiated()){
                addFoundServiceToDatabase(mdnsCache.getServiceInfo());
                break;
            }
        }
    }

    private boolean serviceExistInDatabase(String serviceName){
        for(Node node : dbNodeList){
            if(serviceName.equalsIgnoreCase(node.getServiceName())){
                return true;
            }
        }
        return false;
    }






    private void checkIfUnregisteredNucleusServicesFound() {
        /*
            Check the mdnsCacheList against database here.
            If any new nodes are found, raise the snackbar
            For now, since we don't have database instance to compare to, we will always notify the user
         */

        if (mdnsCacheList == null) {
            Log.d(TAG, "mdnsCacheList is null. Weird");
            return;
        }

        if (mdnsCacheList.size() == 0 && !searchRequestedByUser) {
            Log.d(TAG, "No services found. Are you connected to WiFi?");
            return;
        }

        //Return if services found - but are unresolved
        if (numberOfServicesResolved(mdnsCacheList) == 0 && !searchRequestedByUser) {
            Log.d(TAG, "Services found but are still unresolved");
            return;
        }

        if (mdnsCacheList.size() == 0 && searchRequestedByUser) {
            searchRequestedByUser = false;
            /*CoordinatorLayout coordinatorLayout = findViewById(R.id.mainActivityCoordinatorLayout);
            Snackbar snackbar = Snackbar.make(coordinatorLayout, "Searching local network for Nucleus nodes", Snackbar.LENGTH_SHORT);
            snackbar.show();*/
            return;
        }
    }

    private int numberOfServicesResolved(List<MdnsCache> serviceList) {
        int count = 0;
        for (MdnsCache c : serviceList) {
            if (c.getResolved()) {
                count++;
            }
        }
        return count;
    }
}
