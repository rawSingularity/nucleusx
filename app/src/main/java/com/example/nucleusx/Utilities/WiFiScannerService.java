package com.example.nucleusx.Utilities;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.nucleusx.DatabaseEntities.Node;
import com.example.nucleusx.ViewModels.NodeViewModel;

import java.util.Date;
import java.util.List;

public class WiFiScannerService extends BroadcastReceiver {

    private static final String TAG = "WiFiScannerService";
    public static final int WIFI_PERMISSION_IDENTIFIER = 0x28F75; //random

    private WifiManager mWiFiManager;
    private Activity mActivity;
    private Context mContext;

    //Database
    private NodeViewModel nodeViewModel;
    private List<Node> dbNodeList;

    //public WiFiScannerService(WifiManager wifiManager){
    public WiFiScannerService(Activity activity) {
        mActivity = activity;
        mContext = activity.getApplicationContext();

        mWiFiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        databaseDataObserverSetup(activity);
    }

    public boolean hasSelfPermission() {
        if (mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    public void initiateScan() {
        //First, delete all the existing unregistered entries from database
        //It's ok to do so since we are going to do the search again
        nodeViewModel.deleteUnregisteredNodes();

        //Second, initiate scan
        mWiFiManager.startScan();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (intent.getAction()) {
            case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
                processScanResults(mWiFiManager.getScanResults());
                break;

            default:
                break;
        }
    }

    private void processScanResults(List<ScanResult> scanResults) {

        if (scanResults == null) {
            mWiFiManager.startScan();
            return;
        }

        Log.d(TAG, "Wifi Scanner results size:" + scanResults.size());

        for (ScanResult scanResult : scanResults) {
            //Log.d(TAG, scanResult.toString());
            Log.d(TAG, "SSID:" + scanResult.SSID + " BSSID:" + scanResult.BSSID);

            if(scanResult.SSID.startsWith("Nucleus")){
                int idx = scanResult.SSID.trim().lastIndexOf(' ');
                int nodeId = Integer.parseInt(scanResult.SSID.substring(idx+1));
                Log.d(TAG, "Parsed nodeId:" + nodeId);

                Node node = new Node(scanResult.SSID.toString(), nodeId);
                prepareToAddNode(node);
            }
        }

        //Rerun the scan
        //mWiFiManager.startScan();
    }

    private void prepareToAddNode(Node node) {

        if(dbNodeList == null || dbNodeList.size()==0){
            addNode(node);
        }
        else if(nodeExistsInDb(node)) {
            //Check if it already exists
            Node candidate = getNodeForNodeId(node.getNodeId());
            candidate.setServiceName(node.getServiceName());
            nodeViewModel.update(candidate);
            Log.d(TAG, "prepareToAddNode: node already exists:" + candidate.getNodeId() + " last updated: " + candidate.getLastUpdated());
        }
        else {
            //Does not exist in Database - add
            addNode(node);
        }
    }

    private Node getNodeForNodeId(int nodeId){
        for(Node n : dbNodeList){
            if (n.getNodeId() == nodeId){
                return n;
            }
        }
        return null;
    }

    private boolean nodeExistsInDb(Node node){
        for(Node n : dbNodeList){
            if (n.getNodeId() == node.getNodeId()){
                return true;
            }
        }
        return false;
    }

    private void addNode(Node node){
        nodeViewModel.insert(node);
    }

    /*
    DATABASE OBSERVER SETUP
     */
    private void databaseDataObserverSetup(Activity activity) {

        nodeViewModel = ViewModelProviders.of((FragmentActivity) activity).get(NodeViewModel.class);

        nodeViewModel.getAllNodes().observe((LifecycleOwner) activity, new Observer<List<Node>>() {
            @Override
            public void onChanged(@Nullable List<Node> nodes) {
                Log.d(TAG, "dbNodeList has a change. Size=" + nodes.size());
                dbNodeList = nodes;

//                //Mark services as added
//                for(Node node : dbNodeList){
//                    markServiceAsAdded(node.getServiceName());
//                }
//
//                //Look for next service in mdnsCacheList
//                initiateAddToDatabaseFromMdnsCacheList();
            }
        });
    }
}
