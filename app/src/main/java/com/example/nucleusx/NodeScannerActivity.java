package com.example.nucleusx;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.ScannedNodeItem;
import com.example.nucleusx.Adapters.ScannedNodeItemAdapter;
import com.example.nucleusx.NodeApis.IdApi;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjId;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.net.NetworkInfo.DetailedState.CONNECTED;

public class NodeScannerActivity extends AppCompatActivity {

    private static final String TAG = "NodeScannerActivity";
    private TextView textView;
    private Button w_button_node_scan;
    private Button w_button_stop_scan;
    private boolean terminateScan = false;

    private TextView vtext_view_connectivity_status;
    private TextView vtext_view_my_ip;
    private ProgressBar vpbScanProgress;
    private TextView vtvProgressValue;
    private TextView vip1;
    private TextView vip2;
    private TextView vip3;
    private TextView vip4;
    private TextView vtext_view_task_window;

    private String ipStringRaw;

    private NodeScanner nodeScanner;
    //For RecyclerView
    ArrayList<ScannedNodeItem> scannedNodeItems = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ScannedNodeItemAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_scanner);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Node Scanning Tool");

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView = findViewById(R.id.text_view_node_scanner_result);
        w_button_node_scan = findViewById(R.id.button_node_scan);
        w_button_stop_scan = findViewById(R.id.button_stop_scan);
        vtext_view_connectivity_status = findViewById(R.id.text_view_connectivity_status);
        vtext_view_my_ip = findViewById(R.id.text_view_my_ip);
        vip1 = findViewById(R.id.ip1);
        vip2 = findViewById(R.id.ip2);
        vip3 = findViewById(R.id.ip3);
        vip4 = findViewById(R.id.ip4);
        vpbScanProgress = findViewById(R.id.pbScanProgress);
        vtvProgressValue = findViewById(R.id.tvProgressValue);
        vtext_view_task_window = findViewById(R.id.text_view_task_window);
        setProgressElements(new UpdateObject("Analyzer window",0));

        Context context = getApplicationContext();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        Log.d(TAG, "getDetailedState: " + activeNetwork.getDetailedState());

        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        Log.d(TAG, "WiFi State: " + wm.getWifiState());

        WifiInfo connectionInfo = wm.getConnectionInfo();
        int ipAddress = connectionInfo.getIpAddress();

        Log.d(TAG, "Int IP: " + ipAddress);

        ipStringRaw = Formatter.formatIpAddress(ipAddress);

        Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
        Log.d(TAG, "ipString: " + String.valueOf(ipStringRaw));

        if (activeNetwork == null) {
            Snackbar.make(this.textView, "Scanner service disabled since device is not connected to any network", Snackbar.LENGTH_LONG).show();
            vtext_view_connectivity_status.setText("DISCONNECTED");
            return;
        }

        if (activeNetwork.getDetailedState() != CONNECTED) {
            //Snackbar.make(NodeScannerActivity.class, "Replace with your own action", Snackbar.LENGTH_LONG).show();
            Snackbar.make(this.textView, "Scanner service disabled since device is not connected to network", Snackbar.LENGTH_LONG).show();
            return;
        }


        Snackbar.make(this.textView, "Scanner service activated", Snackbar.LENGTH_LONG).show();

        // Connectivity status
        String cs = "Connected - ";
        if (activeNetwork.getType() == 0) {
            cs += "Cellular";
        } else if (activeNetwork.getType() == 1) {
            cs += "WiFi";
        } else {
            cs += "Other";
        }
        setTextView(vtext_view_connectivity_status, cs);

        // My IP
        setTextView(vtext_view_my_ip, ipStringRaw);

        //Compute IP Seed
        int firstDot = ipStringRaw.indexOf(".");
        Log.d(TAG, "secondDot: IndexOf: " + ipStringRaw.indexOf(".",firstDot+1));
        int secondDot = ipStringRaw.indexOf(".",firstDot+1);
        int thirdDot = ipStringRaw.lastIndexOf('.');
        Log.d(TAG, "Dot1:" + firstDot + " Dot2:" + secondDot + " Dot3:" + thirdDot + "\n");

        int ipOct1 = Integer.parseInt(ipStringRaw.substring(0, firstDot));
        int ipOct2 = Integer.parseInt(ipStringRaw.substring(firstDot + 1, secondDot));
        int ipOct3 = Integer.parseInt(ipStringRaw.substring(secondDot + 1, thirdDot));
        int ipOct4 = Integer.parseInt(ipStringRaw.substring(ipStringRaw.lastIndexOf(".") + 1));

        if (ipOct1 == 0 && ipOct2 == 0 && ipOct3 == 0 && ipOct4 == 0) {
            ipOct1 = 192;
            ipOct2 = 168;
            ipOct3 = 1;
            //ipOct4 = 1;
            Snackbar.make(this.textView, "IP Seed has been set to default", Snackbar.LENGTH_LONG).show();
        }

        // SET IP SEED
        vip1.setText(String.valueOf(ipOct1));
        vip2.setText(String.valueOf(ipOct2));
        vip3.setText(String.valueOf(ipOct3));
        vip4.setText(String.valueOf(1));

        // Enable button
        w_button_node_scan.setEnabled(true);
        //w_button_stop_scan.setEnabled(true);

        //RecyclerView test elements
        //createExampleList();
        createRecyclerView();
    }

    public void createExampleList() {
        scannedNodeItems.add(new ScannedNodeItem(389233, "192.168.1.201", "unregistered"));
        scannedNodeItems.add(new ScannedNodeItem(311625, "192.168.1.203", "registered to home-group"));
        scannedNodeItems.add(new ScannedNodeItem(316237, "192.168.1.205", "registered to Pratik's home-group"));
    }

    public void createRecyclerView(){
        mRecyclerView = findViewById(R.id.rvNodeScannerResults);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ScannedNodeItemAdapter(scannedNodeItems);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ScannedNodeItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // Flow into next Activity
                Intent intent = new Intent(NodeScannerActivity.this, NodeQueryActivity.class);
                intent.putExtra("Node Item", scannedNodeItems.get(position));
                startActivity(intent);
            }

            @Override
            public void onLocateClick(int position) {
                Toast.makeText(NodeScannerActivity.this, "TODO: Initiate turn-on-beacon request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onConfigureClick(int position) {
                //Toast.makeText(NodeScannerActivity.this, "TODO: Initiate flow to device configuration", Toast.LENGTH_SHORT).show();
                ScannedNodeItem parcel = scannedNodeItems.get(position);

                Intent intent = new Intent(NodeScannerActivity.this, NodeControlActivity.class);
                intent.putExtra("Node Item", parcel);
                startActivity(intent);
            }

            @Override
            public void onMoreInfoClick(int position) {

            }

            @Override
            public void onDeleteClick(int position) {

            }
        });
    }

    public void addItemToRecyclerView(Integer nodeId, String nodeIp, String status){
        scannedNodeItems.add(0,new ScannedNodeItem(nodeId, nodeIp, status));
        //Insert at the position
        mAdapter.notifyItemInserted(0);
        //Scroll to the newly added position
        mRecyclerView.smoothScrollToPosition(0);
    }

    public void onScanRunner(View view) {

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        w_button_stop_scan.setEnabled(true);
        w_button_node_scan.setEnabled(false);
        textView.setText("");

        vpbScanProgress.setVisibility(View.VISIBLE);
        setProgressElements(new UpdateObject("Analyzer window",0));

        Snackbar.make(view, "Node scan initiated!", Snackbar.LENGTH_LONG).show();
        //String prefix = "192.168.1.";
        String prefix = String.valueOf(vip1.getText()) + "." +
                String.valueOf(vip2.getText()) + "." +
                String.valueOf(vip3.getText()) + ".";

        /*
        for (int i = 1; i < 255; i++) {

            if(this.terminateScan){
                w_button_stop_scan.setEnabled(false);
                w_button_node_scan.setEnabled(true);
                this.terminateScan = false;
                break;
            }

            String testIp = prefix + String.valueOf(i);

            IpProbe ipProbe = (IpProbe) new IpProbe(new IpProbe.IpProbeResponse() {
                @Override
                public void processIpProbeResponse(String result) {
                    textView.append(result);
                }
            }).execute(testIp);

        }*/

        //VPN
        // Still need to figure out how to use this
        List<String> networkList = new ArrayList<>();
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                Log.d(TAG + " nl", "\ndisplay name: " + networkInterface.getDisplayName().toString());
                Log.d(TAG + " nl", "\nhardwareAddress: " +networkInterface.getHardwareAddress().toString());
                Log.d(TAG + " nl", "\nindex: " + networkInterface.getIndex());
                Log.d(TAG + " nl", "\ninetAddress: " + networkInterface.getInetAddresses().toString());
                Log.d(TAG + " nl", "\nname: " +networkInterface.getName());
                Log.d(TAG + " nl", "\nup?: " + networkInterface.isUp());
                Log.d(TAG + " nl", "\ndisplay name: " + networkInterface.getDisplayName());
                Log.d(TAG + " nl", "\nvirtual?" + networkInterface.isVirtual());

                if (networkInterface.isUp()) {
                    networkList.add(networkInterface.getName());
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Exception in networkInterface");
            ex.printStackTrace();
        }



        nodeScanner = new NodeScanner();
        nodeScanner.NetworkSniffTask(this);
        nodeScanner.execute(prefix);
    }

    public void onScanTerminateRunner(View view) {
        Snackbar.make(view, "Node scan terminated!", Snackbar.LENGTH_LONG).show();
        //this.terminateScan = true;
        nodeScanner.cancel(true);
        w_button_stop_scan.setEnabled(false);
        w_button_node_scan.setEnabled(true);
        //vpbScanProgress.setVisibility(View.GONE);
        //setProgressElements(new UpdateObject("Node scan terminated!",0));
        vtext_view_task_window.setText("Node scan terminated!");
    }

    private void setTextView(TextView tv, String s) {
        tv.setText(s);
        tv.setTextColor(Color.BLACK);
        tv.setTypeface(null, Typeface.BOLD);
    }

    private void setProgressElements(UpdateObject object){
        vpbScanProgress.setProgress(Math.round(object.updatePercent));
        vtvProgressValue.setText(String.format("%,.1f", object.updatePercent) + "%");
        vtext_view_task_window.setText(object.updateString);
    }

    class UpdateObject {
        private String updateString;
        private float updatePercent;

        public String getUpdateString() {
            return updateString;
        }

        public float getUpdatePercent() {
            return updatePercent;
        }

        public UpdateObject(String updateString, float updatePercent) {
            this.updateString = updateString;
            this.updatePercent = updatePercent;
        }
    }

    class NodeScanner extends AsyncTask<String, UpdateObject, Void> {

        private static final String TAG = "nodeScanner";

        private boolean blMyAsyncTask;
        private boolean cancelTask;

        public void NetworkSniffTask(Context context) {
            Log.d(TAG, "Method invoked: NetworkSniffTask");
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.d(TAG, "Let's sniff the network");
            String prefix = params[0];

            try {
                Context context = getApplicationContext();

                if (context != null) {

                    for (int i = 1; i < 255; i++) {

                        // CHECK IF THE USER HAS CANCELLED THE TASK
                        if (isCancelled()) {
                            cancelTask = true; // (OPTIONAL) THIS IS AN ADDITIONAL CHECK I HAVE USED. THINK OF IT AS A FAIL SAFE.
                            break; // REMOVE IF NOT USED IN A FOR LOOP
                        }

                        String testIp = prefix + String.valueOf(i);
                        Log.d(TAG, "Probing: " + testIp);

                        InetAddress address = InetAddress.getByName(testIp);
                        boolean reachable = address.isReachable(250);
                        String hostName = address.getCanonicalHostName();

                        if (reachable) {
                            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");

                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl("http://" + testIp + "/")
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();

                            IdApi idApi = retrofit.create(IdApi.class);
                            Call<NodeResponseObjId> call = idApi.getNodeResponseObjId();

                            call.enqueue(new Callback<NodeResponseObjId>() {
                                @Override
                                public void onResponse(Call<NodeResponseObjId> call, Response<NodeResponseObjId> response) {
                                    if (!response.isSuccessful()) {
                                        Log.d("Response Failure", "response failure. code: " + response.code());
                                        //Toast.makeText(NodeScannerActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                                        Snackbar.make(textView, "Response unsuccessful: " + response.code(), Snackbar.LENGTH_LONG).show();
                                        return;
                                    }

                                    // NODE FOUND!!!
                                    NodeResponseObjId nodeResponseObjId = response.body();

                                    Log.d("NODE FOUND!: ","NodeId: " + nodeResponseObjId.getNode()+" IP: " + nodeResponseObjId.getIp());
                                    //textView.append("Node " + nodeResponseObjId.getNode() + " found @ " + nodeResponseObjId.getIp() + "\n");

                                    //ADD TO RECYLERVIEW
                                    addItemToRecyclerView(nodeResponseObjId.getNode(),
                                            nodeResponseObjId.getIp(),
                                            nodeResponseObjId.getTime());
                                }

                                @Override
                                public void onFailure(Call<NodeResponseObjId> call, Throwable t) {
                                    Log.d("Call Failure", t.getMessage());
                                    //Toast.makeText(NodeScannerActivity.this, "Failure " + t.getMessage(), Toast.LENGTH_LONG).show();
                                    Snackbar.make(textView, "Request failure: " + t.getMessage(), Snackbar.LENGTH_LONG).show();
                                }
                            });
                        }

                        publishProgress(new UpdateObject("Analyzing IP: " + testIp, i*100/254));
                    }
                }
            } catch (Throwable t) {
                Log.e(TAG, "Well that's not good. - maybe cleanup the cache???", t);
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            blMyAsyncTask = true;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            blMyAsyncTask = false;

            if (cancelTask == false) {
                // THE NORMAL CODE YOU HAVE IN YOUR onPostExecute()
                setProgressElements(new UpdateObject("Scan complete!", 100));
            }
            else {
                setProgressElements(new UpdateObject("Scan aborted.", 0));
            }
        }

        @Override
        protected void onProgressUpdate(UpdateObject... values) {
            Log.d(TAG, "onProgressUpdate update: " + values[0].updateString + ", " + values[0].updatePercent);
            setProgressElements(values[0]);
        }

        public String integerToStringIP(int ip) {
//        return ((ip >> 24 ) & 0xFF) + "." +
//
//                ((ip >> 16 ) & 0xFF) + "." +
//
//                ((ip >>  8 ) & 0xFF) + "." +
//
//                ( ip        & 0xFF);

            return (ip & 0xFF) + "." + ((ip >> 8) & 0xFF) + "." + ((ip >> 16) & 0xFF) + "." + ((ip >> 24) & 0xFF);
        }
    }
}
