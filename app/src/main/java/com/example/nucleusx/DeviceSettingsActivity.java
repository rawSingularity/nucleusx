package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.DeviceLocation;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.NodeApis.SetDeviceStateApi;
import com.example.nucleusx.NodeResponseObjects.NodeResponseObjDeviceInfo;
import com.example.nucleusx.ViewModels.DeviceLocationViewModel;
import com.example.nucleusx.ViewModels.DeviceViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceSettingsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "DeviceSettingsActivity";
    HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();

    Spinner modeSpinner;
    TextView secondaryTitle;
    Spinner secondaryModeSpinner;
    TextView tvDeviceName;
    TextView tvHardwarePin;
    private boolean thisIsNotFirstRun = false;
    ControlItem incomingParcel;
    RelativeLayout advanceSettingsContainer;
    Button mEditRoomSettings;
    RadioGroup mRoomSettingsRadioGroup;
    TextView mdsManageRoomLink;
    CheckBox mdsQuickAccessCheckBox;
    TextView mdsAddToRoomTitle;

    private List<RequestListElement> requestListElements = new ArrayList<>();
    private DeviceViewModel deviceViewModel;
    private List<Device> mAllDevices;
    private List<DeviceLocation> mDeviceLocations;
    private DeviceLocationViewModel deviceLocationViewModel;

    @Override
    public <T extends View> T findViewById(int id) {
        return super.findViewById(id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_settings);
        setTitle("Device Settings");

        //Setup view components
        tvHardwarePin = findViewById(R.id.dsHardwarePinValue);
        advanceSettingsContainer = findViewById(R.id.dsAdvanceSettingsContainer);
        mdsAddToRoomTitle = findViewById(R.id.dsAddToRoomTitle);

        //Setup advanced settings button
        Button saveButton = findViewById(R.id.dsSaveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateSave();
            }
        });

        modeSpinner = findViewById(R.id.dsOperatingModeValue);
        //Now setup spinner adapter & listener
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.deviceOperatingModes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modeSpinner.setAdapter(adapter);

        tvDeviceName = findViewById(R.id.dsDeviceNameTitle);
        secondaryTitle = findViewById(R.id.dsSecondaryTitle);
        secondaryModeSpinner = findViewById(R.id.dsSecondaryValue);

        Menu m = findViewById(R.id.menuActionAdvanceSettings);

        // To get data from Node Control Activity
        Intent intent = getIntent();
        incomingParcel = intent.getParcelableExtra("Device Info Parcel");
        if (incomingParcel != null) {
            populateAdvanceDeviceSettingsWithExisting(incomingParcel);
        }

        //Now setup listener
        modeSpinner.setOnItemSelectedListener(this);

        //Floating Action Button for setting up calendar events
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.dsScheduleEventButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSchedulerActivity(incomingParcel);
            }
        });*/

        mdsManageRoomLink = findViewById(R.id.dsManageRoomLink);
        mdsManageRoomLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DeviceSettingsActivity.this, RoomsActivity.class));
            }
        });


        mRoomSettingsRadioGroup = findViewById(R.id.dsRoomRadioGroup);

        mdsManageRoomLink.setVisibility(View.GONE);
        mRoomSettingsRadioGroup.setVisibility(View.GONE);

        mEditRoomSettings = findViewById(R.id.dsEditRoomSettings);
        mEditRoomSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRoomSettingsRadioGroup.getVisibility() == View.GONE) {
                    mRoomSettingsRadioGroup.setVisibility(View.VISIBLE);
                    mdsManageRoomLink.setVisibility(View.VISIBLE);
                } else {
                    mRoomSettingsRadioGroup.setVisibility(View.GONE);
                    mdsManageRoomLink.setVisibility(View.GONE);
                }
            }
        });


        //Snackbar to let user know that the device name is modifiable
        CoordinatorLayout coordinatorLayout = findViewById(R.id.deviceSettingsScreenContainer);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Tap on device name to change it", Snackbar.LENGTH_LONG);
        snackbar.show();


        deviceViewModel = ViewModelProviders.of(this).get(DeviceViewModel.class);
        deviceViewModel.getAllDevices().observe(this, new Observer<List<Device>>() {
            @Override
            public void onChanged(@Nullable List<Device> devices) {
                //Toast.makeText(MainActivity.this, "Got all quick access devices", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "deviceViewModel: getAllDevices: onChanged");
                mAllDevices = devices;
                refreshUserView();
            }
        });

        deviceLocationViewModel = ViewModelProviders.of(this).get(DeviceLocationViewModel.class);
        deviceLocationViewModel.getAllDeviceLocations().observe(this, new Observer<List<DeviceLocation>>() {
            @Override
            public void onChanged(@Nullable List<DeviceLocation> deviceLocations) {
                //Update radioGroup
                mDeviceLocations = deviceLocations;
                updateRoomsRadioButtonGroup(deviceLocations);

                refreshUserView();
            }
        });


        mdsQuickAccessCheckBox = findViewById(R.id.dsQuickAccessCheckBox);

        // Room / DeviceLocation selection
        mRoomSettingsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                updateDeviceLocation(checkedId);
            }
        });


        // Device QuickAccess Preference
        mdsQuickAccessCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateQuickAccessPreference(isChecked);
            }
        });
    }


    private void updateRoomsRadioButtonGroup(List<DeviceLocation> deviceLocations) {
        //Defining buttons quantity!
        int buttons = deviceLocations.size();
        int counter = 0;

        //First remove existing views
        mRoomSettingsRadioGroup.removeAllViews();

        //Create buttons!
        for (DeviceLocation deviceLocation : deviceLocations) {
            RadioButton rbn = new RadioButton(this);
            rbn.setId(++counter + 1000);
            rbn.setText(deviceLocation.getName());
            //Attach button to RadioGroup
            mRoomSettingsRadioGroup.addView(rbn);
        }
    }

    private void refreshUserView() {
        if (mAllDevices == null || mDeviceLocations == null) {
            return;
        }

        Log.d(TAG, "refreshUserView");

        Device deviceInDb = null;
        for (Device d : mAllDevices) {
            if (incomingParcel.getDeviceInfo().getD() == d.getDeviceId() && incomingParcel.getDeviceInfo().getNode() == d.getNodeId()) {
                //Device found
                deviceInDb = d;
                break;
            }
        }

        if (deviceInDb == null) {
            //Device does not exist in database. Keep everything as it is & return
            return;
        }

        Log.d(TAG, deviceInDb.toString());

        //If here, we have the device from database - start setting up the views
        //1. Set quickAccess Checkbox
        mdsQuickAccessCheckBox.setChecked(deviceInDb.getQuickAccessMember());

        //2. TODO: Set Rooms
        if (deviceInDb.getLocation() == null) {
            mdsAddToRoomTitle.setText("Does not belong to any room");
        } else if (deviceInDb.getLocation().equalsIgnoreCase("") || deviceInDb.getLocation().equalsIgnoreCase("Does not belong to any room")) {
            mdsAddToRoomTitle.setText("Does not belong to any room");
        } else if (!deviceLocationExist(deviceInDb.getLocation())) {
            //updateDeviceLocation(-1);
        } else {
            mdsAddToRoomTitle.setText("Located in " + deviceInDb.getLocation());
        }

        //Hide the radiogroup
        mRoomSettingsRadioGroup.setVisibility(View.GONE);
        mdsManageRoomLink.setVisibility(View.GONE);


        //3. TODO: Set Groups
    }

    private boolean deviceLocationExist(String deviceLocation) {

        if (mDeviceLocations == null) {
            return false;
        }

        for (DeviceLocation d : mDeviceLocations) {
            if (d.getName().trim().equalsIgnoreCase(deviceLocation.trim())) {
                return true;
            }
        }
        return false;
    }

    private void updateQuickAccessPreference(Boolean quickAccessMember) {
        //First, check if the device itself exist
        //  If it does, just update the 'quickAccess' flag
        //  If it does not, add new device

        if (mAllDevices != null) {
            for (Device d : mAllDevices) {
                if (incomingParcel.getDeviceInfo().getD() == d.getDeviceId() && incomingParcel.getDeviceInfo().getNode() == d.getNodeId()) {

                    //Already exist & same value - pass
                    if (d.getQuickAccessMember() == quickAccessMember) {
                        //already the same - pass
                        return;
                    }

                    //Already exist - update
                    d.setQuickAccessMember(quickAccessMember);
                    Log.d(TAG, "updateQuickAccessPreference: ");
                    Log.d(TAG, d.toString());

                    //deviceViewModel.update(d);
                    deviceViewModel.setDeviceQuickAccessPreference(d);
                    return;
                }
            }
        }

        // if here does not exist in database - add
        Device newDevice = new Device(
                incomingParcel.getDeviceName(),
                incomingParcel.getDeviceInfo().getD(),
                incomingParcel.getDeviceInfo().getC(),
                incomingParcel.getDeviceInfo().getS(),
                incomingParcel.getDeviceInfo().getNode(),
                incomingParcel.getDeviceInfo().getUTS(),
                "Does not belong to any room",
                quickAccessMember
        );

        Log.d(TAG, "Adding to database: " + newDevice.toString());

        deviceViewModel.insert(newDevice);
    }

    private void updateDeviceLocation(int checkedId) {

        String location = "Does not belong to any room";

        if (checkedId == -1) {
            checkedId = mRoomSettingsRadioGroup.getCheckedRadioButtonId();
        }

        View radioButton = mRoomSettingsRadioGroup.findViewById(checkedId);
        int idx = mRoomSettingsRadioGroup.indexOfChild(radioButton);
        RadioButton r = (RadioButton) mRoomSettingsRadioGroup.getChildAt(idx);
        location = r.getText().toString();

        //First, check if the device itself exist
        //  If it does, just update the 'quickAccess' flag
        //  If it does not, add new device

        if (mAllDevices != null) {
            for (Device d : mAllDevices) {
                if (incomingParcel.getDeviceInfo().getD() == d.getDeviceId() && incomingParcel.getDeviceInfo().getNode() == d.getNodeId()) {

                    //Already exist & same value - pass
                    if (d.getLocation().equalsIgnoreCase(location)) {
                        //already the same - pass
                        return;
                    }

                    //Already exist & different- update
                    d.setLocation(location);

                    Log.d(TAG, "updateDeviceLocation: deviceViewMode.setDeviceLocation: " + d.toString());
                    //deviceViewModel.update(d);
                    deviceViewModel.setDeviceLocation(d);
                    return;
                }
            }
        }

        // if here does not exist in database - add
        Device newDevice = new Device(
                incomingParcel.getDeviceName(),
                incomingParcel.getDeviceInfo().getD(),
                incomingParcel.getDeviceInfo().getC(),
                incomingParcel.getDeviceInfo().getS(),
                incomingParcel.getDeviceInfo().getNode(),
                incomingParcel.getDeviceInfo().getUTS(),
                location,
                false
        );

        Log.d(TAG, "Adding to database: " + newDevice.toString());

        deviceViewModel.insert(newDevice);

        //Update timestamp of the device location so that the information is updated on RoomsActivity screen
        updateLocationTimeStamp();
    }

    private void updateLocationTimeStamp() {
        for (DeviceLocation deviceLocation : mDeviceLocations) {
            deviceLocationViewModel.update(deviceLocation);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (incomingParcel != null) {
            //fetchAndPopulateEventsWithLatest(incomingParcel.getDeviceInfo().getD());
        }
    }

    /*public void populateEventsWithExisting(List<CalendarEvent> incomingCalendarEvents){

        if(incomingCalendarEvents == null){
            Log.d(TAG, "incomingCalendarEvents is null");
            return;
        }

        for(CalendarEvent c : incomingCalendarEvents){
            mCalendarEventsList.add(c);
        }

        if(mCalendarEventsList == null){
            Log.d(TAG, "mCalendarEventsList is null");
            return;
        }
        //mCalendarEventsList.sort(Comparator.comparing(CalendarEvent::getT()));
    }*/

    public void showWarningSnackbar() {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.deviceSettingsScreenContainer);
        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Warning! Modifications can lead to failure. Are you sure?", Snackbar.LENGTH_LONG)
                .setAction("Yes! Take\nme there!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        advanceSettingsContainer.setVisibility(View.VISIBLE);
                    }
                });
        snackbar.show();
    }

    private void toggleAdvanceSettingsView() {
        if (advanceSettingsContainer.getVisibility() == View.VISIBLE) {
            advanceSettingsContainer.setVisibility(View.GONE);
        } else {
            showWarningSnackbar();
        }
    }

    private void populateAdvanceDeviceSettingsWithExisting(ControlItem incomingParcel) {
        int index;

        tvDeviceName.setText(incomingParcel.getDeviceName());
        tvHardwarePin.setText("D" + incomingParcel.getDeviceInfo().getD() + ", Outlet: " + incomingParcel.getDeviceInfo().getD());
        if (incomingParcel.getDeviceInfo() == null) {
            Log.d(TAG, "WEIRD: why is getDeviceInfo null?");
        }

        HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();
        int modeStringId = hardwareDescriptors.getDeviceModeStringResourceId(incomingParcel.getDeviceInfo().getC());

        String deviceModeString = getResources().getString(modeStringId);
        index = Arrays.asList(getResources().getStringArray(R.array.deviceOperatingModes)).indexOf(deviceModeString);
        modeSpinner.setSelection(index);

        String secModeString = getResources().getString(hardwareDescriptors.getDeviceSecondaryModeString(incomingParcel.getDeviceInfo().getC()));
        Log.d(TAG, "secModeString:" + secModeString);

        ArrayAdapter<CharSequence> adapter;
        if (modeStringId == R.string.device_mode_input_external_pullup ||
                modeStringId == R.string.device_mode_input_internal_pullup) {
            //Set title
            secondaryTitle.setText("Interrupt mode");
            //Set adapter
            adapter = ArrayAdapter.createFromResource(this, R.array.deviceInterruptModes, android.R.layout.simple_spinner_item);
            //Get index
            index = Arrays.asList(getResources().getStringArray(R.array.deviceInterruptModes)).indexOf(secModeString);

        } else {
            //Set title
            secondaryTitle.setText("Active Level");
            //Set adapter
            adapter = ArrayAdapter.createFromResource(this, R.array.deviceActiveLevel, android.R.layout.simple_spinner_item);
            //Get index
            index = Arrays.asList(getResources().getStringArray(R.array.deviceActiveLevel)).indexOf(secModeString);
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        secondaryModeSpinner.setAdapter(adapter);
        secondaryModeSpinner.setSelection(index);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.advance_settings_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuActionAdvanceSettings:
                toggleAdvanceSettingsView();
                return true;

            default:
                Log.d(TAG, "Unknown menu item selected");
                return super.onOptionsItemSelected(item);
        }
    }

    public void initiateSave() {
        // First build c-value
        List<Integer> configs = new ArrayList<Integer>();
        int i;
        String s = modeSpinner.getSelectedItem().toString();

        i = getLimitedResId(s);
        Log.d(TAG, "modeSpinner text: " + s + " resId:" + i);
        configs.add(i);

        s = secondaryModeSpinner.getSelectedItem().toString();

        i = getLimitedResId(s);
        Log.d(TAG, "secModeSpinner text: " + s + " resId:" + i);
        configs.add(i);

        HardwareDescriptors hardwareDescriptors = new HardwareDescriptors();
        i = hardwareDescriptors.getConfigurationValueForConfigurationText(configs);

        Log.d(TAG, "Final config value: " + i);

        //Perform Device configuration transaction
        initiateDeviceConfigurationRequestIfRequired(i);

        //Perform Device name change transaction
        String newName = tvDeviceName.getText().toString();
        Log.d(TAG, "newName length is " + newName.length());

        //Validation
        if (newName == null || newName.length() == 0) {
            Log.d(TAG, "newName is null");
            newName = "Device D" + incomingParcel.getDeviceInfo().getD();
            Log.d(TAG, "modified to " + newName);
        } else if (newName.length() > 31) {
            Log.d(TAG, "length is " + newName.length());
            Toast.makeText(this, "Long device names are unsupported.\nProvide a shorter one", Toast.LENGTH_LONG).show();
            return;
        } else {
            Log.d(TAG, "validation pass");
            initiateDeviceNameChangeRequestIfRequired(newName);
        }

        if (requestListElements.size() == 0) {
            //Nothing to wait upon - revert immediately
            Toast.makeText(this, "Nothing to save", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void initiateDeviceConfigurationRequestIfRequired(int c) {
        //First, figure out if we need to configure device
        if (incomingParcel.getDeviceInfo().getC() == c) {
            //c-value has not changed - no configuration change required
            return;
        }

        requestListElements.add(new RequestListElement("deviceConfiguration", false));

        Log.d(TAG, "Preparing to send to: " + incomingParcel.getDeviceInfo().getIp());
        Log.d(TAG, "d=" + incomingParcel.getDeviceInfo().getD() +
                " c=" + c + " i=" + incomingParcel.getDeviceInfo().getNode());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + incomingParcel.getDeviceInfo().getIp() + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjPostConfiguration(incomingParcel.getDeviceInfo().getD(),
                c, incomingParcel.getDeviceInfo().getNode());
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(DeviceSettingsActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                NodeResponseObjDeviceInfo nodeResponseObjDeviceInfo = response.body();
                Toast.makeText(getApplicationContext(), "Device configuration updated", Toast.LENGTH_LONG).show();
                //
                markRequestAsProcessed("deviceConfiguration");
                checkOnPendingRequests();
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    public void initiateDeviceNameChangeRequestIfRequired(String newName) {
        if (incomingParcel.getDeviceName() != null) {
            if (incomingParcel.getDeviceName().equalsIgnoreCase(newName)) {
                //Device name hasn't changed - no name chage request required
                return;
            }
        }

        requestListElements.add(new RequestListElement("deviceNameChange", false));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + incomingParcel.getDeviceInfo().getIp() + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SetDeviceStateApi setDeviceStateApi = retrofit.create(SetDeviceStateApi.class);
        Call<NodeResponseObjDeviceInfo> call = setDeviceStateApi.getNodeResponseObjPostDeviceNameChange(incomingParcel.getDeviceInfo().getD(),
                newName, incomingParcel.getDeviceInfo().getNode());
        call.enqueue(new Callback<NodeResponseObjDeviceInfo>() {
            @Override
            public void onResponse(Call<NodeResponseObjDeviceInfo> call, Response<NodeResponseObjDeviceInfo> response) {
                if (!response.isSuccessful()) {
                    Log.d("sysCall Failure", "response failure. code: " + response.code());
                    Toast.makeText(DeviceSettingsActivity.this, "Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                NodeResponseObjDeviceInfo nodeResponseObjDeviceInfo = response.body();
                Toast.makeText(getApplicationContext(), "Device name updated", Toast.LENGTH_LONG).show();
                //Mark request as processed
                markRequestAsProcessed("deviceNameChange");
                checkOnPendingRequests();
            }

            @Override
            public void onFailure(Call<NodeResponseObjDeviceInfo> call, Throwable t) {

            }
        });
    }

    private void markRequestAsProcessed(String type) {
        for (RequestListElement r : requestListElements) {
            if (r.getRequestType().equalsIgnoreCase(type)) {
                Log.d(TAG, "Marked Request: " + type + " as Processed!");
                r.setRequestProcessed(true);
                return;
            }
        }
    }

    public void checkOnPendingRequests() {
        for (RequestListElement r : requestListElements) {
            if (r.getRequestProcessed() == false) {
                Log.d(TAG, "Request: " + r.getRequestType() + " is still being processed!!");
                return;
            }
        }
        // If here, all requests have finished processing
        Log.d(TAG, "All REQUESTS PROCESSED!!!!");
        //Hide keyboard
        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(NodeControlActivity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        //Complete activity
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // this flag is required to avoid the onItemSelected run when the fields are initialized
        // programatically based on incoming parcel.
        if (!thisIsNotFirstRun) {
            Log.d(TAG, "This is first run of onItemSelected. Discarding...");
            thisIsNotFirstRun = true;
            return;
        }

        String selectedItem = parent.getItemAtPosition(position).toString();
        Log.d(TAG, "item selected: " + selectedItem);

        switch (parent.getId()) {

            case R.id.dsOperatingModeValue:
                Log.d(TAG, "primary spinner: " + selectedItem);
                setAppropriationsOnSecondarySettingsMenu(selectedItem);
                break;

            case R.id.dsSecondaryValue:
                Log.d(TAG, "secondary spinner: " + selectedItem);

            default:
                if (parent == null) {
                    Log.d(TAG, "Error: onItemSelected: Null parent");
                } else {
                    Log.d(TAG, "Error: onItemSelected: Unknown parent. Id=" + parent.getId());
                }
                break;
        }

    }

    private void setAppropriationsOnSecondarySettingsMenu(String selectedItem) {

        ArrayAdapter<CharSequence> adapter = null;

        if (selectedItem.startsWith("Output")) {
            //Set title
            secondaryTitle.setText("Active Level");
            //Set adapter
            adapter = ArrayAdapter.createFromResource(this, R.array.deviceActiveLevel, android.R.layout.simple_spinner_item);
        } else {
            //Set title
            secondaryTitle.setText("Interrupt mode");
            //Set adapter
            adapter = ArrayAdapter.createFromResource(this, R.array.deviceInterruptModes, android.R.layout.simple_spinner_item);
        }


        if (adapter == null) {
            Log.d(TAG, "Error: Unable to resolve selected item: " + selectedItem);
        }

        //Setup secondary spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        secondaryModeSpinner.setAdapter(adapter);
        secondaryModeSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public int getLimitedResId(String s) {
        if (s.equalsIgnoreCase(getResources().getString(R.string.device_mode_input_external_pullup))) {
            return R.string.device_mode_input_external_pullup;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.device_mode_input_internal_pullup))) {
            return R.string.device_mode_input_internal_pullup;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.device_mode_output_digital))) {
            return R.string.device_mode_output_digital;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.device_mode_output_pwm))) {
            return R.string.device_mode_output_pwm;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.active_level_high))) {
            return R.string.active_level_high;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.active_level_low))) {
            return R.string.active_level_low;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_no_interrupt))) {
            return R.string.interrupt_mode_no_interrupt;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_change))) {
            return R.string.interrupt_mode_change;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_high_to_low))) {
            return R.string.interrupt_mode_high_to_low;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_low_to_high))) {
            return R.string.interrupt_mode_low_to_high;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_level_low))) {
            return R.string.interrupt_mode_level_low;
        } else if (s.equalsIgnoreCase(getResources().getString(R.string.interrupt_mode_level_high))) {
            return R.string.interrupt_mode_level_high;
        }

        return -1;
    }
}
