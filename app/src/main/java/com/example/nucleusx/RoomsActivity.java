package com.example.nucleusx;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nucleusx.ActivityParcels.DeviceListParcel;
import com.example.nucleusx.Adapters.DeviceLocationAdapter;
import com.example.nucleusx.DatabaseEntities.Device;
import com.example.nucleusx.DatabaseEntities.DeviceLocation;
import com.example.nucleusx.Utilities.HardwareDescriptors;
import com.example.nucleusx.ViewModels.DeviceLocationViewModel;

import java.util.ArrayList;
import java.util.List;

public class RoomsActivity extends AppCompatActivity {

    private static final String TAG = "RoomsActivity";

    HardwareDescriptors hd;

    private DeviceLocationViewModel deviceLocationViewModel;
    private RecyclerView recyclerView;
    private FloatingActionButton mAddRoomButton;
    private List<DeviceLocation> mDeviceLocations;
    private DeviceLocationAdapter deviceLocationAdapter;
    private Boolean addRoomInProgress;

    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);
        setTitle("My Rooms");

        hd = new HardwareDescriptors(this);

        recyclerView = findViewById(R.id.roomsViewRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        deviceLocationAdapter = new DeviceLocationAdapter(this);
        recyclerView.setAdapter(deviceLocationAdapter);

        deviceLocationViewModel = ViewModelProviders.of(this).get(DeviceLocationViewModel.class);
        deviceLocationViewModel.getAllDeviceLocations().observe(this, new Observer<List<DeviceLocation>>() {
            @Override
            public void onChanged(@Nullable List<DeviceLocation> deviceLocations) {
                //update RecyclerView
                //deviceLocationAdapter.setDeviceLocationList(deviceLocations);
                deviceLocationAdapter.submitList(deviceLocations);
                mDeviceLocations = deviceLocations;
                addRoomInProgress = false;

                logDeviceLocations(deviceLocations);

                scrollToTopDelayed();

                if(mDeviceLocations.size() != 0){
                    findViewById(R.id.roomDefaultBanner).setVisibility(View.GONE);
                }
                else {
                    findViewById(R.id.roomDefaultBanner).setVisibility(View.VISIBLE);
                }
            }
        });

        mAddRoomButton = findViewById(R.id.addRoomButton);
        mAddRoomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addRoomInProgress) {
                    Toast.makeText(RoomsActivity.this, "New room addition already in progress - please wait", Toast.LENGTH_LONG).show();
                }
                else{
                    addRoomInProgress = true;
                    AddRoomAsyncTask addRoomAsyncTask = new AddRoomAsyncTask();
                    addRoomAsyncTask.execute();
                }
            }
        });

        deviceLocationAdapter.setOnItemClickListener(new DeviceLocationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                //List<Integer> parcel = hd.getListOfCombinedNodeIdAndDeviceIdForRoom(mDeviceLocations.get(position).getName());

                DeviceListParcel deviceListParcel = new DeviceListParcel(deviceLocationAdapter.getDeviceLocationAtPosition(position).getName());
                deviceListParcel.setDeviceList(hd.getListOfDevicesForRoom(deviceLocationAdapter.getDeviceLocationAtPosition(position).getName()));

                if(deviceListParcel.getDeviceList().size() == 0){
                    Toast.makeText(RoomsActivity.this, mDeviceLocations.get(position).getName() + " does not have any devices", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent intent = new Intent(RoomsActivity.this, ManageDeviceActivity.class);
                intent.putExtra("Device List", deviceListParcel);
                startActivity(intent);
            }

            @Override
            public void onDeleteClick(int position) {
                showRoomDeletionSnackbarForPosition(position);
            }

            @Override
            public void onNameClick(int position, String newName) {
                updateRoomName(position, newName.trim());
            }

            @Override
            public void onNameEdit() {
                hideEditRoomNameSnackbar();
            }
        });
    }

    private void logDeviceLocations(List<DeviceLocation> deviceLocations){
        for(DeviceLocation d : deviceLocations){
            Log.d(TAG, d.toString());
        }
    }

    private void scrollToTopDelayed(){
        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                //Scroll to top
                recyclerView.getLayoutManager().scrollToPosition(0);
            }
        }.start();
    }

    private void updateRoomName(int position, String newName){
        if(mDeviceLocations == null){
            return;
        }

        if(newName == null){
            return;
        }

        if(newName.length() == 0){
            Toast.makeText(RoomsActivity.this, "Name cannot be empty", Toast.LENGTH_LONG).show();
            return;
        }

        if(newName.length() < 3){
            Toast.makeText(RoomsActivity.this, "Name length is too short", Toast.LENGTH_LONG).show();
            return;
        }

        if(newName.length() > 30){
            Toast.makeText(RoomsActivity.this, "Name length is too long", Toast.LENGTH_LONG).show();
            return;
        }

        if(nameAlreadyExist(newName)){
            Toast.makeText(RoomsActivity.this, newName + "already exist", Toast.LENGTH_LONG).show();
            return;
        }

        if(newName.trim().equals(mDeviceLocations.get(position).getName().trim())){
            Toast.makeText(RoomsActivity.this, "No change to update", Toast.LENGTH_LONG).show();
            return;
        }

        newName = hd.sanitizeRoomName(newName);

        DeviceLocation newDeviceLocation = new DeviceLocation(newName);
        newDeviceLocation.setId(mDeviceLocations.get(position).getId());

        //First, update all devices with the old room name
        hd.updateAllDeviceLocationIn(mDeviceLocations.get(position).getName(), newName);

        //Now update the room name itself
        deviceLocationViewModel.update(newDeviceLocation);


        Toast.makeText(RoomsActivity.this, newName + " updated!", Toast.LENGTH_LONG).show();


        //Hide keyboard
        /*
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);*/
    }

    private void showRoomDeletionSnackbarForPosition(final int position){
        CoordinatorLayout coordinatorLayout = findViewById(R.id.roomCoordinatorLayout);

        StringBuilder sb = new StringBuilder("This will delete ");
        sb.append(mDeviceLocations.get(position).getName());
        sb.append(" This action cannot be undone.");

        Snackbar snackbar = Snackbar.make(coordinatorLayout, sb.toString(), Snackbar.LENGTH_LONG)
                .setAction("DELETE!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        performRoomPurge(position);
                    }
                });
        snackbar.show();
    }

    private void performRoomPurge(int position){
        if(mDeviceLocations == null){
            return;
        }

        //First, remove all devices from this room
        hd.removeDevicesFromRoom(mDeviceLocations.get(position).getName());

        //And then, delete the room
        deviceLocationViewModel.delete(mDeviceLocations.get(position));
    }

    private class AddRoomAsyncTask extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            StringBuilder sb = new StringBuilder("New Room ");

            Boolean candidateNameFound = false;
            int counter = 0;

            while(!candidateNameFound){

                if(nameAlreadyExist(sb.toString())){
                    candidateNameFound = false;
                    sb.delete(8, sb.length());

                    counter++;
                    sb.append(" ");
                    sb.append(counter);
                    Log.d(TAG, "Now testing: " + sb.toString());
                }
                else{
                    candidateNameFound = true;
                }
            }
            return sb.toString().trim();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            DeviceLocation newRoom = new DeviceLocation(s);
            deviceLocationViewModel.insert(newRoom);
            showEditRoomNameSnackbar();
        }
    }

    private boolean nameAlreadyExist(String name){
        for(DeviceLocation deviceLocation : mDeviceLocations){
            if(deviceLocation.getName().trim().equalsIgnoreCase(name.trim())){
                Log.d(TAG, name + " already exist");
                return true;
            }
        }
        Log.d(TAG, name + " does not exist");
        return false;
    }

    public void showEditRoomNameSnackbar() {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.roomCoordinatorLayout);

        String message = "Tap New Room's name to edit";

        snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE)
                .setAction("", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
            }

            @Override
            public void onShown(Snackbar sb) {
                super.onShown(sb);

            }
        });
        snackbar.show();
    }

    private void hideEditRoomNameSnackbar(){
        if(snackbar == null){
            return;
        }

        snackbar.dismiss();
    }
}
